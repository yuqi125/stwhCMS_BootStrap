﻿using System;
using System.Data;
using System.Collections.Generic;
using stwh_Common;
using stwh_Model;
using stwh_IDAL;
using stwh_DALFactory;

namespace stwh_BLL
{
    public partial class stwh_menu_role_functionBLL
    {
        private readonly Istwh_menu_role_functionDAL dal = DataAccess.CreateIDAL("stwh_menu_role_functionDAL") as Istwh_menu_role_functionDAL;

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<stwh_Model.stwh_menu_role_function> DataTableToList(DataTable dt)
        {
            List<stwh_Model.stwh_menu_role_function> modelList = new List<stwh_Model.stwh_menu_role_function>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                stwh_Model.stwh_menu_role_function model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = dal.DataRowToModel(dt.Rows[n]) as stwh_menu_role_function;
                    if (model != null)
                    {
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<stwh_Model.stwh_menu_role_function> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
    }
}
