﻿using System;
using System.Data;
using System.Collections.Generic;
using stwh_Common;
using stwh_Model;
using stwh_IDAL;
using stwh_DALFactory;

namespace stwh_BLL
{
	/// <summary>
	/// stwh_static
	/// </summary>
	public partial class stwh_staticBLL
	{
        private readonly Istwh_staticDAL dal = DataAccess.CreateIDAL("stwh_staticDAL") as Istwh_staticDAL;
		public stwh_staticBLL()
		{}
		#region  BasicMethod
        /// <summary>
        /// 分页获取用户消息
        /// </summary>
        /// <typeparam name="T">指代类型</typeparam>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public List<T> GetListByPage<T>(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount) where T : class
        {
            DataSet ds = dal.GetListByPage(FieldColumn, FieldOrder, If, pageSize, pageNumber, ref selectCount, ref d_peopleCount);
            return IListDataSet.DataSetToIList<T>(ds, 0) as List<T>;
        }

        /// <summary>
        /// 分页获取用户消息
        /// </summary>
        /// <typeparam name="T">指代类型</typeparam>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public List<T> GetListByPage<T>(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag) where T : class
        {
            DataSet ds = dal.GetListByPage(FieldColumn, FieldOrder, If, pageSize, pageNumber, ref selectCount, ref d_peopleCount, flag);
            return IListDataSet.DataSetToIList<T>(ds, 0) as List<T>;
        }

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int stwh_stid)
		{
			return dal.Exists(stwh_stid);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(stwh_Model.stwh_static model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(stwh_Model.stwh_static model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int stwh_stid)
		{
			
			return dal.Delete(stwh_stid);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string stwh_stidlist )
		{
			return dal.DeleteList(stwh_stidlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public stwh_Model.stwh_static GetModel(int stwh_stid)
		{
            return dal.GetModel(stwh_stid) as stwh_static;
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public stwh_Model.stwh_static GetModelByCache(int stwh_stid)
		{
			
			string CacheKey = "stwh_staticModel-" + stwh_stid;
			object objModel = stwh_Common.DataCache.GetMCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(stwh_stid);
                    if (objModel != null) stwh_Common.DataCache.AddMCache(CacheKey, objModel);
				}
				catch{}
			}
			return (stwh_Model.stwh_static)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<stwh_Model.stwh_static> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<stwh_Model.stwh_static> DataTableToList(DataTable dt)
		{
			List<stwh_Model.stwh_static> modelList = new List<stwh_Model.stwh_static>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				stwh_Model.stwh_static model;
				for (int n = 0; n < rowsCount; n++)
				{
                    model = dal.DataRowToModel(dt.Rows[n]) as stwh_static;
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		#endregion  BasicMethod
	}
}

