﻿using System;
using System.Data;
using System.Collections.Generic;
using stwh_Common;
using stwh_Model;
using stwh_IDAL;
using stwh_DALFactory;

namespace stwh_BLL
{
	/// <summary>
	/// stwh_roleinfo
	/// </summary>
	public partial class stwh_roleinfoBLL
	{
        private readonly Istwh_roleinfoDAL dal = DataAccess.CreateIDAL("stwh_roleinfoDAL") as Istwh_roleinfoDAL;
		public stwh_roleinfoBLL()
		{}
		#region  BasicMethod
        /// <summary>
        /// 分页获取用户消息
        /// </summary>
        /// <typeparam name="T">指代类型</typeparam>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public List<T> GetListByPage<T>(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount) where T : class
        {
            DataSet ds = dal.GetListByPage(FieldColumn, FieldOrder, If, pageSize, pageNumber, ref selectCount, ref d_peopleCount);
            return IListDataSet.DataSetToIList<T>(ds, 0) as List<T>;
        }

        /// <summary>
        /// 分页获取用户消息
        /// </summary>
        /// <typeparam name="T">指代类型</typeparam>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public List<T> GetListByPage<T>(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount,int flag) where T : class
        {
            DataSet ds = dal.GetListByPage(FieldColumn, FieldOrder, If, pageSize, pageNumber, ref selectCount, ref d_peopleCount,flag);
            return IListDataSet.DataSetToIList<T>(ds, 0) as List<T>;
        }

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string stwh_rname,int stwh_rid)
		{
			return dal.Exists(stwh_rname,stwh_rid);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(stwh_Model.stwh_roleinfo model)
		{
			return dal.Add(model);
		}

        /// <summary>
        /// 为角色设置菜单和功能权限
        /// </summary>
        /// <param name="stwh_rid">角色id</param>
        /// <param name="mids">菜单集合</param>
        /// <param name="fids">功能集合</param>
        /// <returns></returns>
        public bool Add(int stwh_rid, string mids, string fids)
        {
            return dal.Add(stwh_rid, mids, fids);
        }

        /// <summary>
        /// 添加一个角色信息（并且设置当前角色的所属菜单及功能）
        /// </summary>
        /// <param name="stwh_rname">角色名称</param>
        /// <param name="stwh_rdescription">描述</param>
        /// <param name="stwh_rstate">角色状态</param>
        /// <param name="stwh_menuid">所属菜单id（多个菜单id以','分割）</param>
        /// <returns></returns>
        public int Add(string stwh_rname, string stwh_rdescription, int stwh_rstate, string stwh_menuid)
        {
            return dal.Add(stwh_rname, stwh_rdescription, stwh_rstate, stwh_menuid);
        }

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(stwh_Model.stwh_roleinfo model)
		{
			return dal.Update(model);
		}

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="stwh_rid">编号</param>
        /// <param name="stwh_rstate">状态</param>
        /// <returns></returns>
        public bool Update(string stwh_rid, int stwh_rstate)
        {
            return dal.Update(stwh_rid, stwh_rstate);
        }

        /// <summary>
        /// 恢复被删除的角色
        /// </summary>
        /// <param name="stwh_rids">角色id</param>
        /// <returns></returns>
        public bool Update(string stwh_rids)
        {
            return dal.Update(stwh_rids);
        }

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int stwh_rid)
		{
			return dal.Delete(stwh_rid);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string stwh_rname,int stwh_rid)
		{
			return dal.Delete(stwh_rname,stwh_rid);
		}

        /// <summary>
        /// 批量删除数据（逻辑删除，物理删除）
        /// </summary>
        /// <param name="stwh_ridlist">角色id</param>
        /// <param name="flag">标识，0逻辑删除，1物理删除</param>
        /// <returns></returns>
        public bool DeleteList(string stwh_ridlist, int flag)
		{
			return dal.DeleteList(stwh_ridlist,flag);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public stwh_Model.stwh_roleinfo GetModel(int stwh_rid)
		{
            return dal.GetModel(stwh_rid) as stwh_roleinfo;
		}

         /// <summary>
        /// 判断角色是否有权限查看某一个页面
        /// </summary>
        /// <param name="stwh_rid">角色id</param>
        /// <param name="pString">父菜单名称（例如：sys_users、sys_setting等）</param>
        /// <param name="fString">页面（功能）名称（例如：index.aspx、add.aspx、update.aspx、add.ashx）</param>
        /// <returns></returns>
        public bool CheckRoleFunction(int stwh_rid, string pString, string fString)
        {
            return dal.CheckRoleFunction(stwh_rid, pString, fString);
        }

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public stwh_Model.stwh_roleinfo GetModelByCache(int stwh_rid)
		{
			
			string CacheKey = "stwh_roleinfoModel-" + stwh_rid;
			object objModel = stwh_Common.DataCache.GetMCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(stwh_rid);
                    if (objModel != null) stwh_Common.DataCache.AddMCache(CacheKey, objModel);
				}
				catch{}
			}
			return (stwh_Model.stwh_roleinfo)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<stwh_Model.stwh_roleinfo> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<stwh_Model.stwh_roleinfo> DataTableToList(DataTable dt)
		{
			List<stwh_Model.stwh_roleinfo> modelList = new List<stwh_Model.stwh_roleinfo>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				stwh_Model.stwh_roleinfo model;
				for (int n = 0; n < rowsCount; n++)
				{
                    model = dal.DataRowToModel(dt.Rows[n]) as stwh_roleinfo;
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		#endregion  BasicMethod
	}
}

