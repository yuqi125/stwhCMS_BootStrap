﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using stwh_Model;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_menu_role接口层
    /// </summary>
    public interface Istwh_menu_roleDAL : IBaseDAL
    {
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        stwh_menu_role GetModel();

        /// <summary>
        /// 增加一条数据
        /// </summary>
        bool Add(stwh_menu_role jbmodel);

        /// <summary>
        /// 添加角色菜单信息，去除重复
        /// </summary>
        /// <param name="stwh_rid">角色id</param>
        /// <param name="stwh_menuid">所属菜单id（多个菜单id以','分割）</param>
        /// <returns></returns>
        int Add(int stwh_rid, string stwh_menuid);

        /// <summary>
        /// 删除一条数据
        /// </summary>
        bool Delete(int stwh_menuid, int stwh_rid);

        /// <summary>
        /// 获得数据列表
        /// </summary>
        DataSet GetList(string strWhere, int flag);
    }
}
