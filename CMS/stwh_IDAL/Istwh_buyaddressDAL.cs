﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using stwh_Model;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_buyaddress接口层
    /// </summary>
    public interface Istwh_buyaddressDAL : IBaseDAL
    {
        /// <summary>
        /// 更新会员地址默认状态
        /// </summary>
        /// <param name="stwh_buid">会员id</param>
        /// <param name="stwh_baid">地址id</param>
        /// <returns></returns>
        bool Update(int stwh_buid, int stwh_baid);

        /// <summary>
        /// 删除会员地址
        /// </summary>
        /// <param name="stwh_baid">地址编号</param>
        /// <param name="stwh_buid">会员id</param>
        /// <returns></returns>
        bool Delete(int stwh_baid, int stwh_buid);

        /// <summary>
        /// 获取会员最新订单商品总价
        /// </summary>
        /// <param name="stwh_buid">会员id</param>
        /// <returns></returns>
        decimal GetOrderTotalPrice(int stwh_buid, ref string stwh_orddid);

        /// <summary>
        /// 获取会员默认地址
        /// </summary>
        /// <param name="stwh_buid">会员id</param>
        /// <param name="stwh_baismr">是否默认地址（0不默认，1默认）</param>
        /// <returns></returns>
        stwh_buyaddress GetModel(int stwh_buid, int stwh_baismr = 1);
    }
}
