﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_Resources接口层
    /// </summary>
    public interface Istwh_ResourcesDAL : IBaseDAL
    {
        /// <summary>
        /// 清空数据
        /// </summary>
        bool DeleteListALL();
    }
}
