﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_roleinfo接口层
    /// </summary>
    public interface Istwh_roleinfoDAL : IBaseDAL
    {
         /// <summary>
        /// 为角色设置菜单和功能权限
        /// </summary>
        /// <param name="stwh_rid">角色id</param>
        /// <param name="mids">菜单集合</param>
        /// <param name="fids">功能集合</param>
        /// <returns></returns>
        bool Add(int stwh_rid, string mids, string fids);

        /// <summary>
        /// 添加一个角色信息（并且设置当前角色的所属菜单及功能）
        /// </summary>
        /// <param name="stwh_rname">角色名称</param>
        /// <param name="stwh_rdescription">描述</param>
        /// <param name="stwh_rstate">角色状态</param>
        /// <param name="stwh_menuid">所属菜单id（多个菜单id以','分割）</param>
        /// <returns></returns>
        int Add(string stwh_rname, string stwh_rdescription, int stwh_rstate, string stwh_menuid);

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="stwh_rid">编号</param>
        /// <param name="stwh_rstate">状态</param>
        /// <returns></returns>
        bool Update(string stwh_rid, int stwh_rstate);

        /// <summary>
        /// 恢复被删除的角色
        /// </summary>
        /// <param name="stwh_rids">角色id</param>
        /// <returns></returns>
        bool Update(string stwh_rids);

        /// <summary>
        /// 删除一条数据
        /// </summary>
        bool Delete(string stwh_rname, int stwh_rid);

         /// <summary>
        /// 批量删除数据（逻辑删除，物理删除）
        /// </summary>
        /// <param name="stwh_ridlist">角色id</param>
        /// <param name="flag">标识，0逻辑删除，1物理删除</param>
        /// <returns></returns>
        bool DeleteList(string stwh_ridlist, int flag);

        /// <summary>
        /// 判断角色是否有权限查看某一个页面
        /// </summary>
        /// <param name="stwh_rid">角色id</param>
        /// <param name="pString">父菜单名称（例如：sys_users、sys_setting等）</param>
        /// <param name="fString">页面（功能）名称（例如：index.aspx、add.aspx、update.aspx、add.ashx）</param>
        /// <returns></returns>
        bool CheckRoleFunction(int stwh_rid, string pString, string fString);

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        bool Exists(string stwh_rname, int stwh_rid);
    }
}
