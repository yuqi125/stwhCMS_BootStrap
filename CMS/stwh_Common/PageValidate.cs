using System;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace stwh_Common
{
	/// <summary>
	/// 页面数据校验类
	/// </summary>
	public class PageValidate
	{
        private static Regex RegPhone = new Regex(@"^(([\d]{7,8})|([\d]{3,4}[\s-]?[\d]{7,8})|([\d]{3,4}[\s-]?[\d]{7,8}[\s-]?[\d]{1,4})|(((\+)|([\d]{2,4}))[\s-]?[\d]{1,4}[\s-]?[\d]{3,4}[\s-]?[\d]{7,8})|([\d]{11})|([\d]{3}[\s]?[\d]{4}[\s]?[\d]{4})|((([+])|([\d]{2,4}))[\s]?[\d]{1,4}[\s]?[\d]{3}[\s]?[\d]{4}[\s]?[\d]{4})|((([\d]{7,8})|([\d]{3,4}[\s-]?[\d]{7,8})|([\d]{3,4}[\s-]?[\d]{7,8}[\s-]?[\d]{1,4})|(((\+)|([\d]{2,4}))[\s-]?[\d]{1,4}[\s-]?[\d]{3,4}[\s-]?[\d]{7,8}))[\/](([\d]{11})|([\d]{3}[\s]?[\d]{4}[\s]?[\d]{4})|((([+])|([\d]{2,4}))[\s]?[\d]{1,4}[\s]?[\d]{3}[\s]?[\d]{4}[\s]?[\d]{4})))|((([\d]{11})|([\d]{3}[\s]?[\d]{4}[\s]?[\d]{4})|((([+])|([\d]{2,4}))[\s]?[\d]{1,4}[\s]?[\d]{3}[\s]?[\d]{4}[\s]?[\d]{4}))[\/](([\d]{7,8})|([\d]{3,4}[\s-]?[\d]{7,8})|([\d]{3,4}[\s-]?[\d]{7,8}[\s-]?[\d]{1,4})|(((\+)|([\d]{2,4}))[\s-]?[\d]{1,4}[\s-]?[\d]{3,4}[\s-]?[\d]{7,8}))))$");
        private static Regex RegMobile = new Regex(@"^((13)|(14)|(15)|(17)|(18)){1}\d{9}$");
		private static Regex RegNumber = new Regex(@"^[0-9]+$");//数字
        private static Regex RegNumberDH = new Regex(@"^[0-9,]+$");//数字逗号
        private static Regex RegNumberLetter = new Regex(@"^[-0-9a-zA-Z]+$");//数字字母
        private static Regex RegAlphanumeric = new Regex(@"^[0-9a-zA-Z_]+$");//字母数字下划线
		private static Regex RegNumberSign = new Regex(@"^[+-]?[0-9]+$");//带正负数字
		private static Regex RegDecimal = new Regex(@"^[0-9]+[.]?[0-9]+$");//浮点数
		private static Regex RegDecimalSign = new Regex(@"^[+-]?[0-9]+[.]?[0-9]+$"); //等价于^[+-]?\d+[.]?\d+$
		private static Regex RegEmail = new Regex(@"^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$");//email
		private static Regex RegCHZN = new Regex(@"^[\u4e00-\u9fa5]{1,}$");
        private static Regex RegCHZNNum = new Regex(@"^[0-9a-zA-Z\u4e00-\u9fa5_,.!?'""\+\-\*\/\[\]:;、；：—《》【】，。！？‘’“”]{1,}$");//汉字数字字母标点符号
        private static Regex RegURL = new Regex(@"(https?|ftp|rtsp|mms):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?");//url
        private static Regex RegIP = new Regex(@"^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$");
        private static Regex RegPwd = new Regex(@"(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[+=_!@#&])[a-zA-Z0-9+=_!@#&]{8,12}");//密码复杂度

		public PageValidate()
		{
		}

        /// <summary>
        /// 检查密码复杂度
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        public static bool IsPwdComplex(string inputData)
        {
            Match m = RegPwd.Match(inputData);
            return m.Success;
        }

        /// <summary>
        /// 检查inputData字符是否是ip地址
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        public static bool IsIP(string inputData)
        {
            Match m = RegIP.Match(inputData);
            return m.Success;
        }

        /// <summary>
        /// 自定义正则表达式验证输入字符串
        /// </summary>
        /// <param name="regex">正则表达式</param>
        /// <param name="inputData">验证字符串</param>
        /// <returns></returns>
        public static bool IsCustom(string regex, string inputData)
        {
            Match m = new Regex(regex).Match(inputData);
            return m.Success;
        }

        /// <summary>
        /// 检查inputdata字符是否是URL
        /// </summary>
        /// <param name="inputData">输入字符串</param>
        /// <returns></returns>
        public static bool IsURLLetter(string inputData)
        {
            Match m = RegURL.Match(inputData);
            return m.Success;
        }

		#region 数字字符串检查
        /// <summary>
        /// 检查inputdata字符是否是字母数字
        /// </summary>
        /// <param name="inputData">输入字符串</param>
        /// <returns></returns>
        public static bool IsNumberLetter(string inputData)
        {
            Match m = RegNumberLetter.Match(inputData);
            return m.Success;
        }

        /// <summary>
        /// 检查inputdata字符是否是字母数字下划线
        /// </summary>
        /// <param name="inputData">输入字符串</param>
        /// <returns></returns>
        public static bool IsAlphanumeric(string inputData)
        {
            Match m = RegAlphanumeric.Match(inputData);
            return m.Success;
        }

        /// <summary>
        /// 检查inputData字符是电话号码
        /// </summary>
        /// <param name="inputData">输入字符串</param>
        /// <returns></returns>
        public static bool IsPhone(string inputData)
        {
            Match m = RegPhone.Match(inputData);
            return m.Success;
        }

        /// <summary>
        /// 检查inputData字符是否是手机号码
        /// </summary>
        /// <param name="inputData">输入字符串</param>
        /// <returns></returns>
        public static bool IsMobileNumber(string inputData)
        {
            Match m = RegMobile.Match(inputData);
            return m.Success;
        }

		/// <summary>
		/// 检查Request查询字符串的键值，是否是数字，最大长度限制
		/// </summary>
		/// <param name="req">Request</param>
		/// <param name="inputKey">Request的键值</param>
		/// <param name="maxLen">最大长度</param>
		/// <returns>返回Request查询字符串</returns>
		public static string FetchInputDigit(HttpRequest req, string inputKey, int maxLen)
		{
			string retVal = string.Empty;
			if(inputKey != null && inputKey != string.Empty)
			{
				retVal = req.QueryString[inputKey];
				if(null == retVal)
					retVal = req.Form[inputKey];
				if(null != retVal)
				{
					retVal = SqlText(retVal, maxLen);
					if(!IsNumber(retVal))
						retVal = string.Empty;
				}
			}
			if(retVal == null)
				retVal = string.Empty;
			return retVal;
		}		

		/// <summary>
		/// 是否数字字符串
		/// </summary>
		/// <param name="inputData">输入字符串</param>
		/// <returns></returns>
		public static bool IsNumber(string inputData)
		{
			Match m = RegNumber.Match(inputData);
			return m.Success;
		}

        /// <summary>
        /// 是否数字字符串
        /// </summary>
        /// <param name="inputData">输入字符串</param>
        /// <returns></returns>
        public static bool IsNumber(object inputData)
        {
            int tmpInt;
            if (inputData == null) return false;
            if (inputData.ToString().Trim().Length == 0) return false;
            if (!int.TryParse(inputData.ToString(), out tmpInt)) return false;
            else return true;
        }

        /// <summary>
        /// 是否数字+英文逗号字符串
        /// </summary>
        /// <param name="inputData">输入字符串</param>
        /// <returns></returns>
        public static bool IsNumberDH(string inputData)
        {
            Match m = RegNumberDH.Match(inputData);
            return m.Success;
        }

		/// <summary>
		/// 是否数字字符串 可带正负号
		/// </summary>
		/// <param name="inputData">输入字符串</param>
		/// <returns></returns>
		public static bool IsNumberSign(string inputData)
		{
			Match m = RegNumberSign.Match(inputData);
			return m.Success;
		}		

		/// <summary>
		/// 是否是浮点数
		/// </summary>
		/// <param name="inputData">输入字符串</param>
		/// <returns></returns>
		public static bool IsDecimal(string inputData)
		{
			Match m = RegDecimal.Match(inputData);
			return m.Success;
		}		

		/// <summary>
		/// 是否是浮点数 可带正负号
		/// </summary>
		/// <param name="inputData">输入字符串</param>
		/// <returns></returns>
		public static bool IsDecimalSign(string inputData)
		{
			Match m = RegDecimalSign.Match(inputData);
			return m.Success;
		}		

		#endregion

		#region 中文检测

		/// <summary>
		/// 检测是否有中文字符
		/// </summary>
		/// <param name="inputData"></param>
		/// <returns></returns>
		public static bool IsHasCHZN(string inputData)
		{
			Match m = RegCHZN.Match(inputData);
			return m.Success;
		}	

		#endregion

        #region 中文检测数字字母下划线

        /// <summary>
        /// 检测是否有中文字符数字字母下划线
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        public static bool IsHasCHZNNum(string inputData)
        {
            Match m = RegCHZNNum.Match(inputData);
            return m.Success;
        }

        #endregion

		#region 邮件地址
		/// <summary>
		/// 是否是email
		/// </summary>
		/// <param name="inputData">输入字符串</param>
		/// <returns></returns>
		public static bool IsEmail(string inputData)
		{
			Match m = RegEmail.Match(inputData);
			return m.Success;
		}		

		#endregion

        #region 日期格式判断
        /// <summary>
        /// 日期格式字符串判断
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsDateTime(string str)
        {
            try
            {
                if (!string.IsNullOrEmpty(str))
                {
                    DateTime.Parse(str);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        } 
        #endregion

        #region 其他

        /// <summary>
		/// 检查字符串最大长度，返回指定长度的串
		/// </summary>
		/// <param name="sqlInput">输入字符串</param>
		/// <param name="maxLength">最大长度</param>
		/// <returns></returns>			
		public static string SqlText(string sqlInput, int maxLength)
		{			
			if(sqlInput != null && sqlInput != string.Empty)
			{
				sqlInput = sqlInput.Trim();							
				if(sqlInput.Length > maxLength)//按最大长度截取字符串
					sqlInput = sqlInput.Substring(0, maxLength);
			}
			return sqlInput;
		}		

		/// <summary>
		/// 字符串编码
		/// </summary>
		/// <param name="inputData"></param>
		/// <returns></returns>
		public static string HtmlEncode(string inputData)
		{
			return HttpUtility.HtmlEncode(inputData);
		}
		/// <summary>
		/// 设置Label显示Encode的字符串
		/// </summary>
		/// <param name="lbl"></param>
		/// <param name="txtInput"></param>
		public static void SetLabel(Label lbl, string txtInput)
		{
			lbl.Text = HtmlEncode(txtInput);
		}
		public static void SetLabel(Label lbl, object inputObj)
		{
			SetLabel(lbl, inputObj.ToString());
		}		
		//字符串清理
		public static string InputText(string inputString, int maxLength) 
		{			
			StringBuilder retVal = new StringBuilder();

			// 检查是否为空
			if ((inputString != null) && (inputString != String.Empty)) 
			{
				inputString = inputString.Trim();
				
				//检查长度
				if (inputString.Length > maxLength)
					inputString = inputString.Substring(0, maxLength);
				
				//替换危险字符
				for (int i = 0; i < inputString.Length; i++) 
				{
					switch (inputString[i]) 
					{
						case '"':
							retVal.Append("&quot;");
							break;
						case '<':
							retVal.Append("&lt;");
							break;
						case '>':
							retVal.Append("&gt;");
							break;
						default:
							retVal.Append(inputString[i]);
							break;
					}
				}				
				retVal.Replace("'", " ");// 替换单引号
			}
			return retVal.ToString();
			
		}
		/// <summary>
		/// 转换成 HTML code
		/// </summary>
		/// <param name="str">string</param>
		/// <returns>string</returns>
		public static string Encode(string str)
		{			
			str = str.Replace("&","&amp;");
			str = str.Replace("'","''");
			str = str.Replace("\"","&quot;");
			str = str.Replace(" ","&nbsp;");
			str = str.Replace("<","&lt;");
			str = str.Replace(">","&gt;");
			str = str.Replace("\n","<br>");
			return str;
		}
		/// <summary>
		///解析html成 普通文本
		/// </summary>
		/// <param name="str">string</param>
		/// <returns>string</returns>
		public static string Decode(string str)
		{			
			str = str.Replace("<br>","\n");
			str = str.Replace("&gt;",">");
			str = str.Replace("&lt;","<");
			str = str.Replace("&nbsp;"," ");
			str = str.Replace("&quot;","\"");
			return str;
		}

        /// <summary>
        /// 去除sql语句中的危险字符
        /// </summary>
        /// <param name="sqlText">sql语句</param>
        /// <returns></returns>
        public static string SqlTextClear(string sqlText)
        {
            if (sqlText == null)
            {
                return null;
            }
            if (sqlText == "")
            {
                return "";
            }
            sqlText = sqlText.Replace(",", "");//去除,
            sqlText = sqlText.Replace("<", "");//去除<
            sqlText = sqlText.Replace(">", "");//去除>
            sqlText = sqlText.Replace("--", "");//去除--
            sqlText = sqlText.Replace("'", "");//去除'
            sqlText = sqlText.Replace("\"", "");//去除"
            sqlText = sqlText.Replace("=", "");//去除=
            sqlText = sqlText.Replace("%", "");//去除%
            sqlText = sqlText.Replace(" ", "");//去除空格
            return sqlText;
        }
		#endregion

        #region 是否由特定字符组成
        /// <summary>
        /// 是否由特定字符组成（默认相对对象为空）
        /// </summary>
        /// <param name="strInput">校验字符串</param>
        /// <returns></returns>
        public static bool isContainSameChar(string strInput)
        {
            string charInput = string.Empty;
            if (!string.IsNullOrEmpty(strInput))
            {
                charInput = strInput.Substring(0, 1);
            }
            return isContainSameChar(strInput, charInput, strInput.Length);
        }
        /// <summary>
        /// 是否由特定字符组成（相对对象为charInput）
        /// </summary>
        /// <param name="strInput">校验字符串</param>
        /// <param name="charInput">比较字符串</param>
        /// <param name="lenInput">长度限制</param>
        /// <returns></returns>
        public static bool isContainSameChar(string strInput, string charInput, int lenInput)
        {
            if (string.IsNullOrEmpty(charInput))
            {
                return false;
            }
            else
            {
                //Regex RegNumber = new Regex(string.Format("^([{0}])+$", charInput));
                Regex RegNumber = new Regex(string.Format("^([{0}]{{1}})+$", charInput,lenInput));
                Match m = RegNumber.Match(strInput);
                return m.Success;
            }
        }
        #endregion

        #region 检查输入的参数是不是某些定义好的特殊字符：这个方法目前用于密码输入的安全检查
        /// <summary>
        /// 检查输入的参数是不是某些定义好的特殊字符：这个方法目前用于密码输入的安全检查
        /// </summary>
        /// <param name="strInput">校验字符串</param>
        /// <returns></returns>
        public static bool isContainSpecChar(string strInput)
        {
            string[] list = new string[] { "123456", "654321" };
            bool result = new bool();
            for (int i = 0; i < list.Length; i++)
            {
                if (strInput == list[i])
                {
                    result = true;
                    break;
                }
            }
            return result;
        }
        #endregion
    }
}
