﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stwh_Common
{
    /// <summary>
    /// 消息类型
    /// </summary>
    public enum MessageType
    {
        /// <summary>
        /// 成功
        /// </summary>
        success,
        /// <summary>
        /// 信息
        /// </summary>
        info,
        /// <summary>
        /// 警告
        /// </summary>
        warning,
        /// <summary>
        /// 错误
        /// </summary>
        danger
    }
}
