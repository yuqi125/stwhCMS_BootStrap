﻿using System;
using System.Web;
using System.Web.Caching;
using System.Collections;
//引入命名空间
using MemcachedProviders.Cache;
using System.Collections.Generic;

namespace stwh_Common
{
    /// <summary>
    /// 缓存相关的操作类
    /// </summary>
    public class DataCache
    {
        private static readonly Cache objCache;
        
        static DataCache()
        {
            HttpContext context = HttpContext.Current;
            if (context != null) objCache = (Cache)context.Cache;
            else objCache = HttpRuntime.Cache;
        }
        #region System.Web.Caching
        /// <summary>
        /// 获取储存在缓存中的项数
        /// </summary>
        /// <returns></returns>
        public static int GetCacheCount()
        {
            return objCache.Count;
        }
        /// <summary>
        /// 清空当前应用程序所有缓存
        /// </summary>
        public static void ClearCache()
        {
            IDictionaryEnumerator CacheEnum = objCache.GetEnumerator();
            ArrayList al = new ArrayList();
            //检索所有key，并存入ArrayList
            while (CacheEnum.MoveNext())
            {
                al.Add(CacheEnum.Key);
            }
            foreach (string key in al)
            {
                objCache.Remove(key);
            }
        }
        /// <summary>
        /// 移除当前应用程序指定CacheKey的Cache值
        /// </summary>
        /// <param name="CacheKey">缓存数据项的键值</param>
        public static object RemoveCache(string CacheKey)
        {
            return objCache.Remove(CacheKey);
        }
        /// <summary>
        /// 获取当前应用程序指定CacheKey的Cache值
        /// </summary>
        /// <param name="CacheKey">缓存数据项的键值</param>
        /// <returns></returns>
        public static object GetCache(string CacheKey)
        {
            return objCache[CacheKey];
        }
        /// <summary>
        /// 向当前应用程序指定CacheKey的Cache值
        /// </summary>
        /// <param name="CacheKey">参数“CacheKey”代表缓存数据项的键值，必须是唯一的</param>
        /// <param name="objObject">参数“objObject”代表缓存数据的内容，可以是任意类型。</param>
        public static void AddCache(string CacheKey, object objObject)
        {
            objCache.Add(CacheKey, objCache, null, DateTime.Now.AddMinutes(stwh_Common.ConfigHelper.GetConfigDouble("ModelCache")), TimeSpan.Zero, CacheItemPriority.Normal, null);
        }
        /// <summary>
        /// 向当前应用程序指定CacheKey的Cache值
        /// </summary>
        /// <param name="CacheKey">参数“CacheKey”代表缓存数据项的键值，必须是唯一的</param>
        /// <param name="objObject">参数“objObject”代表缓存数据的内容，可以是任意类型。</param>
        /// <param name="absoluteExpiration">表示缓存过期的时间</param>
        /// <param name="slidingExpiration">表示一段时间间隔，表示缓存参数将在多长时间以后被删除，此参数与absoluteExpiration参数相关联。</param>
        public static void AddCache(string CacheKey, object objObject, DateTime absoluteExpiration, TimeSpan slidingExpiration)
        {
            objCache.Add(CacheKey, objCache, null, absoluteExpiration, slidingExpiration, CacheItemPriority.Normal, null);
        }
        /// <summary>
        /// 修改当前应用程序指定CacheKey的Cache值
        /// </summary>
        /// <param name="CacheKey">参数“CacheKey”代表缓存数据项的键值，必须是唯一的</param>
        /// <param name="objObject">参数“objObject”代表缓存数据的内容，可以是任意类型。</param>
        public static void SetCache(string CacheKey, object objObject)
        {
            objCache.Insert(CacheKey, objObject, null, DateTime.Now.AddMinutes(stwh_Common.ConfigHelper.GetConfigDouble("ModelCache")), TimeSpan.Zero);
        }
        /// <summary>
        /// 修改当前应用程序指定CacheKey的Cache值
        /// </summary>
        /// <param name="CacheKey">参数“CacheKey”代表缓存数据项的键值，必须是唯一的</param>
        /// <param name="objObject">参数“objObject”代表缓存数据的内容，可以是任意类型。</param>
        /// <param name="filename">被依赖的文件路径（当被依赖的文件更改时，缓存会立即被清空）</param>
        public static void SetCache(string CacheKey, object objObject, string filename)
        {
            objCache.Insert(CacheKey, objCache, new CacheDependency(@"" + filename), DateTime.Now.AddMinutes(stwh_Common.ConfigHelper.GetConfigDouble("ModelCache")), TimeSpan.Zero);
        }
        /// <summary>
        /// 修改当前应用程序指定CacheKey的Cache值
        /// </summary>
        /// <param name="CacheKey">参数“CacheKey”代表缓存数据项的键值，必须是唯一的</param>
        /// <param name="objObject">参数“objObject”代表缓存数据的内容，可以是任意类型。</param>
        /// <param name="absoluteExpiration">表示缓存过期的时间</param>
        /// <param name="slidingExpiration">表示一段时间间隔，表示缓存参数将在多长时间以后被删除，此参数与absoluteExpiration参数相关联。</param>
        public static void SetCache(string CacheKey, object objObject, DateTime absoluteExpiration, TimeSpan slidingExpiration)
        {
            objCache.Insert(CacheKey, objObject, null, absoluteExpiration, slidingExpiration);
        }
        #endregion
        #region Memcached
        /// <summary>
        /// 获取指定key的Cache值
        /// </summary>
        /// <param name="key">缓存数据项的键值</param>
        /// <returns></returns>
        public static object GetMCache(string key)
        {
            return DistCache.Get(key);
        }
        /// <summary>
        /// 获取指定keys的Cache值
        /// </summary>
        /// <param name="keys">缓存数据项的键值数组</param>
        /// <returns></returns>
        public static IDictionary<string, object> GetMCache(string[] keys)
        {
            return DistCache.Get(keys);
        }
        /// <summary>
        /// 获取指定keys的Cache值
        /// </summary>
        /// <typeparam name="T">泛型对象</typeparam>
        /// <param name="key">缓存数据项的键值</param>
        /// <returns></returns>
        public static T GetMCache<T>(string key)
        {
            return DistCache.Get<T>(key);
        }
        /// <summary>
        /// 添加（更新）指定key的Cache值
        /// </summary>
        /// <param name="key">缓存数据项的键值</param>
        /// <param name="value">待存入缓存的数据</param>
        /// <returns></returns>
        public static bool AddMCache(string key,object value)
        {
            return DistCache.Add(key, value);
        }
        /// <summary>
        /// 添加（更新）指定key的Cache值，使用默认过期时间
        /// </summary>
        /// <param name="key">缓存数据项的键值</param>
        /// <param name="value">待存入缓存的数据</param>
        /// <param name="DefaultExpire">是否使用默认过期（true 使用，false 不使用）</param>
        /// <returns></returns>
        public static bool AddMCache(string key, object value,bool DefaultExpire)
        {
            return DistCache.Add(key, value, DefaultExpire);
        }
        /// <summary>
        /// 添加（更新）指定key的Cache值，并指定过期时间
        /// </summary>
        /// <param name="key">缓存数据项的键值</param>
        /// <param name="value">待存入缓存的数据</param>
        /// <param name="lNumOfMilliSeconds">cache过期时间，单位毫秒</param>
        /// <returns></returns>
        public static bool AddMCache(string key, object value, long lNumOfMilliSeconds)
        {
            return DistCache.Add(key, value, lNumOfMilliSeconds);
        }
        /// <summary>
        /// 添加（更新）指定key的Cache值，并指定过期后时间间隔
        /// </summary>
        /// <param name="key">缓存数据项的键值</param>
        /// <param name="value">待存入缓存的数据</param>
        /// <param name="timeSpan">过期后时间间隔</param>
        /// <returns></returns>
        public static bool AddMCache(string key, object value, TimeSpan timeSpan)
        {
            return DistCache.Add(key, value, timeSpan);
        }
        /// <summary>
        /// 将指定key的Cache值（以string类型存放int数字），按照Amount进行减
        /// </summary>
        /// <param name="key">缓存数据项的键值</param>
        /// <param name="Amount">要减的数值</param>
        /// <returns>如果key不存在、值小于0、值非整数 则返回-1。</returns>
        public static long Decrement(string key, long Amount)
        {
            return DistCache.Decrement(key, Amount);
        }
        /// <summary>
        /// 将指定key的Cache值（以string类型存放int数字），按照Amount进行增
        /// </summary>
        /// <param name="key">缓存数据项的键值</param>
        /// <param name="Amount">要增的数值</param>
        /// <returns>如果key不存在、值小于0、值非整数 则返回-1。</returns>
        public static long Increment(string key, long Amount)
        {
            return DistCache.Increment(key, Amount);
        }
        /// <summary>
        /// 移除指定key的Cache值
        /// </summary>
        /// <param name="key">缓存数据项的键值</param>
        /// <returns></returns>
        public static bool RemoveMCache(string key)
        {
            return (bool)DistCache.Remove(key);
        }
        /// <summary>
        /// 移除所有key的Cache值
        /// </summary>
        public static void ClearMCache()
        {
            DistCache.RemoveAll();
        }
        #endregion
    }
}
