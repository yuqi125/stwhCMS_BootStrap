//字符串扩展方法
/*
去除字符串左右空格
*/
String.prototype.Trim = function () {
    return this.replace(/(^\s*)|(\s*$)/g, "");
}
/*
去除字符串左边空格
*/
String.prototype.LTrim = function () {
    return this.replace(/(^\s*)/g, "");
}
/*
去除字符串右边空格
*/
String.prototype.RTrim = function () {
    return this.replace(/(\s*$)/g, "");
}
//Array数组扩展方法
/*
将数组按照传递分隔（separator ）符，转换为字符串
separator           必须。分隔符

其他方法说明：
定义和用法
join() 方法用于把数组中的所有元素放入一个字符串。
元素是通过指定的分隔符进行分隔的。
语法
arrayObject.join(separator)
参数	            描述
separator	    可选。指定要使用的分隔符。如果省略该参数，则使用逗号作为分隔符。
*/
Array.prototype.toStringArray = function (separator) {
    if (!separator) separator = ",";
    return this.join(separator);
};
/*
移除数组中指定下标元素，并返回数组
index       必须。索引

其他方法说明：
定义和用法
splice() 方法向/从数组中添加/删除项目，然后返回被删除的项目。
注释：该方法会改变原始数组。

语法
arrayObject.splice(index,howmany,item1,.....,itemX)

参数	                    描述
index	                必需。整数，规定添加/删除项目的位置，使用负数可从数组结尾处规定位置。
howmany	            必需。要删除的项目数量。如果设置为 0，则不会删除项目。
item1, ..., itemX	    可选。向数组添加的新项目。
*/
Array.prototype.removeIndexArray = function (index) {
    if (isNaN(index) || index > this.length || index < 0) return [];
    this.splice(index, 1);
    return this;
};
/*
将新元素插入到数组指定索引位置，插入位置的元素自动后移，并返回数组
content         必须。新元素
index            可选。索引位置
*/
Array.prototype.InsertArray = function (content, index) {
    if (isNaN(index)) return [];
    this.splice(index, 0, content);
    return this;
};
/*
将数组指定索引位置的值修改为新元素值，并返回数组
content         必须。新元素
index            可选。索引位置
*/
Array.prototype.UpdateArray = function (content, index) {
    if (isNaN(index)) return [];
    this.splice(index, 1, content);
    return this;
};
/*
将新元素添加到数组中，并返回数组
content         必须。新元素
isFirst            可选。是否添加到开始位置（默认 false, true 添加到数组开始位置，false 添加到数组结尾处位置）
*/
Array.prototype.AddArray = function (content, isFirst) {
    if (typeof (isFirst) != "boolean" && isFirst) return [];
    if (isFirst) this.unshift(content);
    else this.push(content);
    return this;
};
/*
查询元素在数组中的索引位置，返回索引
searchText          必须。搜索内容
*/
Array.prototype.IndexOfArray = function (searchText) {
    var index = -1;
    for (var i = 0; i < this.length; i++) {
        if (this[i] == searchText) {
            index = i;
            break;
        }
    }
    return index;
};
//--------------------------------------------------- 
// 判断闰年 
//--------------------------------------------------- 
Date.prototype.isLeapYear = function () {
    return (0 == this.getYear() % 4 && ((this.getYear() % 100 != 0) || (this.getYear() % 400 == 0)));
}

//--------------------------------------------------- 
// 日期格式化 
// 格式 YYYY/yyyy/YY/yy 表示年份 
// MM/M 月份 
// W/w 星期 
// dd/DD/d/D 日期 
// hh/HH/h/H 时间 
// mm/m 分钟 
// ss/SS/s/S 秒 
//示例： 
//alert(new Date().Format("yyyy年MM月dd日")); 
//alert(new Date().Format("MM/dd/yyyy")); 
//alert(new Date().Format("yyyyMMdd")); 
//alert(new Date().Format("yyyy-MM-dd hh:mm:ss"));
//--------------------------------------------------- 
Date.prototype.Format = function (formatStr) {
    var str = formatStr;
    var Week = ['日', '一', '二', '三', '四', '五', '六'];
    str = str.replace(/yyyy|YYYY/, this.getFullYear());
    str = str.replace(/yy|YY/, (this.getYear() % 100) > 9 ? (this.getYear() % 100).toString() : '0' + (this.getYear() % 100));
    str = str.replace(/MM/, (this.getMonth() + 1) > 9 ? (this.getMonth() + 1).toString() : '0' + (this.getMonth() + 1));
    str = str.replace(/M/g, (this.getMonth() + 1));
    str = str.replace(/w|W/g, Week[this.getDay()]);
    str = str.replace(/dd|DD/, this.getDate() > 9 ? this.getDate().toString() : '0' + this.getDate());
    str = str.replace(/d|D/g, this.getDate());
    str = str.replace(/hh|HH/, this.getHours() > 9 ? this.getHours().toString() : '0' + this.getHours());
    str = str.replace(/h|H/g, this.getHours());
    str = str.replace(/mm/, this.getMinutes() > 9 ? this.getMinutes().toString() : '0' + this.getMinutes());
    str = str.replace(/m/g, this.getMinutes());
    str = str.replace(/ss|SS/, this.getSeconds() > 9 ? this.getSeconds().toString() : '0' + this.getSeconds());
    str = str.replace(/s|S/g, this.getSeconds());
    return str;
}

// 在时间对象上添加指定的天数
// 例如 要在 var time = new Date('2013/07/15 15:12:10')上添加15天
// 可以使用time.addDays(15)即可
Date.prototype.addDays = Date.prototype.addDays || function (days) {
    this.setDate(this.getDate() + days);
    return this;
}
Date.prototype.addMonth = Date.prototype.addMonth || function (month) {
    this.setMonth(this.getMonth() + month);
    return this;
}
Date.prototype.addYear = Date.prototype.addYear || function (year) {
    this.setFullYear(this.getFullYear() + year);
    return this;
}

//+--------------------------------------------------- 
//| 求两个时间的天数差 日期格式为 YYYY-MM-dd 
//+--------------------------------------------------- 
function daysBetween(DateOne, DateTwo) {
    var OneMonth = DateOne.substring(5, DateOne.lastIndexOf('-'));
    var OneDay = DateOne.substring(DateOne.length, DateOne.lastIndexOf('-') + 1);
    var OneYear = DateOne.substring(0, DateOne.indexOf('-'));
    var TwoMonth = DateTwo.substring(5, DateTwo.lastIndexOf('-'));
    var TwoDay = DateTwo.substring(DateTwo.length, DateTwo.lastIndexOf('-') + 1);
    var TwoYear = DateTwo.substring(0, DateTwo.indexOf('-'));
    var cha = ((Date.parse(OneMonth + '/' + OneDay + '/' + OneYear) - Date.parse(TwoMonth + '/' + TwoDay + '/' + TwoYear)) / 86400000);
    return Math.abs(cha);
}

//+--------------------------------------------------- 
//| 日期计算 
//+--------------------------------------------------- 
Date.prototype.DateAdd = function (strInterval, Number) {
    var dtTmp = this;
    switch (strInterval) {
        case 's': return new Date(Date.parse(dtTmp) + (1000 * Number));
        case 'n': return new Date(Date.parse(dtTmp) + (60000 * Number));
        case 'h': return new Date(Date.parse(dtTmp) + (3600000 * Number));
        case 'd': return new Date(Date.parse(dtTmp) + (86400000 * Number));
        case 'w': return new Date(Date.parse(dtTmp) + ((86400000 * 7) * Number));
        case 'q': return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number * 3, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
        case 'm': return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
        case 'y': return new Date((dtTmp.getFullYear() + Number), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
    }
}

//+--------------------------------------------------- 
//| 比较日期差 dtEnd 格式为日期型或者 有效日期格式字符串 
//+--------------------------------------------------- 
Date.prototype.DateDiff = function (strInterval, dtEnd) {
    var dtStart = this;
    if (typeof dtEnd == 'string')//如果是字符串转换为日期型 
    {
        dtEnd = StringToDate(dtEnd);
    }
    switch (strInterval) {
        case 's': return parseInt((dtEnd - dtStart) / 1000);
        case 'n': return parseInt((dtEnd - dtStart) / 60000);
        case 'h': return parseInt((dtEnd - dtStart) / 3600000);
        case 'd': return parseInt((dtEnd - dtStart) / 86400000);
        case 'w': return parseInt((dtEnd - dtStart) / (86400000 * 7));
        case 'm': return (dtEnd.getMonth() + 1) + ((dtEnd.getFullYear() - dtStart.getFullYear()) * 12) - (dtStart.getMonth() + 1);
        case 'y': return dtEnd.getFullYear() - dtStart.getFullYear();
    }
}

//+--------------------------------------------------- 
//| 日期输出字符串，重载了系统的toString方法 
//+--------------------------------------------------- 
Date.prototype.toString = function (showWeek) {
    var myDate = this;
    var str = myDate.toLocaleDateString();
    if (showWeek) {
        var Week = ['日', '一', '二', '三', '四', '五', '六'];
        str += ' 星期' + Week[myDate.getDay()];
    }
    return str;
}

//+--------------------------------------------------- 
//| 日期合法性验证 
//| 格式为：YYYY-MM-DD或YYYY/MM/DD 
//+--------------------------------------------------- 
function IsValidDate(DateStr) {
    var sDate = DateStr.replace(/(^\s+|\s+$)/g, ''); //去两边空格; 
    if (sDate == '') return true;
    //如果格式满足YYYY-(/)MM-(/)DD或YYYY-(/)M-(/)DD或YYYY-(/)M-(/)D或YYYY-(/)MM-(/)D就替换为'' 
    //数据库中，合法日期可以是:YYYY-MM/DD(2003-3/21),数据库会自动转换为YYYY-MM-DD格式 
    var s = sDate.replace(/[\d]{4,4}[\-\/]{1}[\d]{1,2}[\-\/]{1}[\d]{1,2}/g, '');
    if (s == '') //说明格式满足YYYY-MM-DD或YYYY-M-DD或YYYY-M-D或YYYY-MM-D 
    {
        var t = new Date(sDate.replace(/\-/g, '/'));
        var ar = sDate.split(/[-\/:]/);
        if (ar[0] != t.getYear() || ar[1] != t.getMonth() + 1 || ar[2] != t.getDate()) {
            //alert('错误的日期格式！格式为：YYYY-MM-DD或YYYY/MM/DD。注意闰年。'); 
            return false;
        }
    }
    else {
        //alert('错误的日期格式！格式为：YYYY-MM-DD或YYYY/MM/DD。注意闰年。'); 
        return false;
    }
    return true;
}

//+--------------------------------------------------- 
//| 日期时间检查 
//| 格式为：YYYY-MM-DD HH:MM:SS 
//+--------------------------------------------------- 
function CheckDateTime(str) {
    var reg = /^(\d+)-(\d{1,2})-(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/;
    var r = str.match(reg);
    if (r == null) return false;
    r[2] = r[2] - 1;
    var d = new Date(r[1], r[2], r[3], r[4], r[5], r[6]);
    if (d.getFullYear() != r[1]) return false;
    if (d.getMonth() != r[2]) return false;
    if (d.getDate() != r[3]) return false;
    if (d.getHours() != r[4]) return false;
    if (d.getMinutes() != r[5]) return false;
    if (d.getSeconds() != r[6]) return false;
    return true;
}

//+--------------------------------------------------- 
//| 把日期分割成数组 
//+--------------------------------------------------- 
Date.prototype.toArray = function () {
    var myDate = this;
    var myArray = Array();
    myArray[0] = myDate.getFullYear();
    myArray[1] = myDate.getMonth();
    myArray[2] = myDate.getDate();
    myArray[3] = myDate.getHours();
    myArray[4] = myDate.getMinutes();
    myArray[5] = myDate.getSeconds();
    return myArray;
}

//+--------------------------------------------------- 
//| 取得日期数据信息 
//| 参数 interval 表示数据类型 
//| y 年 m月 d日 w星期 ww周 h时 n分 s秒 
//+--------------------------------------------------- 
Date.prototype.DatePart = function (interval) {
    var myDate = this;
    var partStr = '';
    var Week = ['日', '一', '二', '三', '四', '五', '六'];
    switch (interval) {
        case 'y': partStr = myDate.getFullYear(); break;
        case 'm': partStr = myDate.getMonth() + 1; break;
        case 'd': partStr = myDate.getDate(); break;
        case 'w': partStr = Week[myDate.getDay()]; break;
        case 'ww': partStr = myDate.WeekNumOfYear(); break;
        case 'h': partStr = myDate.getHours(); break;
        case 'n': partStr = myDate.getMinutes(); break;
        case 's': partStr = myDate.getSeconds(); break;
    }
    return partStr;
}

//+--------------------------------------------------- 
//| 取得当前日期所在月的最大天数 
//+--------------------------------------------------- 
Date.prototype.MaxDayOfDate = function () {
    var myDate = this;
    var ary = myDate.toArray();
    var date1 = (new Date(ary[0], ary[1] + 1, 1));
    var date2 = date1.dateAdd(1, 'm', 1);
    var result = dateDiff(date1.Format('yyyy-MM-dd'), date2.Format('yyyy-MM-dd'));
    return result;
}

//+--------------------------------------------------- 
//| 取得当前日期所在周是一年中的第几周 
//+--------------------------------------------------- 
Date.prototype.WeekNumOfYear = function () {
    var myDate = this;
    var ary = myDate.toArray();
    var year = ary[0];
    var month = ary[1] + 1;
    var day = ary[2];
    myDate = DateValue('' + month + '-' + day + '-' + year + '');
    result = DatePart('ww', myDate);
    return result;
}

//+--------------------------------------------------- 
//| 字符串转成日期类型 
//| 格式 MM/dd/YYYY MM-dd-YYYY YYYY/MM/dd YYYY-MM-dd 
//+--------------------------------------------------- 
function StringToDate(DateStr) {
    var converted = Date.parse(DateStr);
    var myDate = new Date(converted);
    if (isNaN(myDate)) {
        //var delimCahar = DateStr.indexOf('/')!=-1?'/':'-'; 
        var arys = DateStr.split('-');
        myDate = new Date(arys[0], --arys[1], arys[2]);
    }
    return myDate;
}

///html编码
function HtmlEncode(text) {
    text = text.replace(/&/g, '&amp;');
    text = text.replace(/"/g, '&quot;');
    text = text.replace(/</g, '&lt;');
    text = text.replace(/>/g, '&gt;');
    text = text.replace(/ /g, '&nbsp;');

    return text;
}

///html解码
function HtmlDecode(text) {
    text = text.replace(/&quot;/g, '"');
    text = text.replace(/&lt;/g, '<');
    text = text.replace(/&gt;/g, '>');
    text = text.replace(/&nbsp;/g, ' ');
    text = text.replace(/&amp;/g, '&');

    return text;
}

//在光标位置插入内容
function insertHtmlAtCaret(html) {
    var sel, range;
    if (window.getSelection) {
        // IE9 and non-IE
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();
            var el = document.createElement("div");
            el.innerHTML = html;
            var frag = document.createDocumentFragment(), node, lastNode;
            while ((node = el.firstChild)) {
                lastNode = frag.appendChild(node);
            }
            range.insertNode(frag);
            // Preserve the selection
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }
        }
    }
    else if (document.selection && document.selection.type != "Control") {
        // IE < 9
        document.selection.createRange().pasteHTML(html);
    }
}


/*  
将Date/String类型,解析为String类型.  
传入String类型,则先解析为Date类型  
不正确的Date,返回 ''  
如果时间部分为0,则忽略,只返回日期部分.  
*/
function formatDate(v) {
    if (v instanceof Date) {
        var y = v.getFullYear();
        var m = v.getMonth() + 1;
        if (m < 10) {
            m = "0" + m;
        }
        var d = v.getDate();
        if (d < 10) {
            d = "0" + d;
        }
        var h = v.getHours();
        var i = v.getMinutes();
        var s = v.getSeconds();
        if (s < 10) {
            s = "0" + s;
        }
        var ms = v.getMilliseconds();
        if (ms > 0)
            return y + '-' + m + '-' + d + ' ' + h + ':' + i + ':' + s;
        if (h > 0 || i > 0 || s > 0)
            return y + '-' + m + '-' + d + ' ' + h + ':' + i + ':' + s;
        return y + '-' + m + '-' + d;
    }
    return v;
}

/*
只允许输入数字
示例：
让文本框只能输入数字
<input type="text" onkeydown="return checkNumber(event);"  value="1" />
*/
function checkNumber(e) {
    if (isFirefox = navigator.userAgent.indexOf("Firefox") > 0) {  //FF 
        if (!((e.which >= 48 && e.which <= 57) || (e.which >= 96 && e.which <= 105) || (e.which == 8) || (e.which == 46)))
            return false;
    } else {
        if (!((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || (event.keyCode == 8) || (event.keyCode == 46))) {
            event.returnValue = false;
        }
    }
}
/*校验密码复杂度*/
function IsPwdComplex(strData) {
    return (/(?=[a-zA-Z0-9+=_!@#&]*[0-9])(?=[a-zA-Z0-9+=_!@#&]*[a-zA-Z])(?=[a-zA-Z0-9+=_!@#&]*[_=+@])[a-zA-Z0-9+=_!@#&]{8,12}/).test(strData);
}
/*校验汉字、数字、字母、标点符号长度*/
function IsHanZF(length1, length2, strData) {
    return (/^[a-zA-Z0-9\u4e00-\u9fff\s\[\]\\;\'\/,.!*()_\-\+Ω【】、；：‘’“”《》，。？！—（）]*$/).test(strData) && (strData.length >= length1 && strData.length <= length2);
}
/*校验汉字长度*/
function IsHan(length1, length2, strData) {
    return (/^[\u4e00-\u9fff]*$/).test(strData) && (strData.length >= length1 && strData.length <= length2);
}
/*校验数字长度*/
function IsNumber(length1, length2, strData) {
    return (/^[0-9]*$/).test(strData) && (strData.length >= length1 && strData.length <= length2);
}
/*校验数字*/
function IsNumber(strData) {
    return (/^[0-9]*$/).test(strData);
}
/*校验字母长度*/
function IsZM(length1, length2, strData) {
    return (/^[a-zA-Z]*$/).test(strData) && (strData.length >= length1 && strData.length <= length2);
}
/*校验字母*/
function IsZM(strData) {
    return (/^[a-zA-Z]*$/).test(strData);
}
/*校验字母数字长度*/
function IsZMNum(length1, length2, strData) {
    return (/^[a-zA-Z0-9]*$/).test(strData) && (strData.length >= length1 && strData.length <= length2);
}
/*校验电话号码*/
function IsTel(strData) {
    return (/^((\d{3,4})|\d{3,4}-)?\d{7,8}(-\d{3})*$/).test(strData);
}
/*校验手机号码*/
function IsMobileTel(strData) {
    return (/^((13)|(14)|(15)|(17)|(18)){1}\d{9}$/).test(strData);
}
/*校验email*/
function IsEmail(strData) {
    return (/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/).test(strData);
}
/*校验IP地址*/
function IsIP(strData) {
    return (/^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/).test(strData);
}
/*校验身份证*/
function IsCard(strData)
{
    return (/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/).test(strData);
}
/*获取地址栏参数*/
function GetUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); 
    else return null;
}
//在min-max之间生成随机数
function GetRandomNum(Min, Max) {
    var Range = Max - Min;
    var Rand = Math.random();
    return (Min + Math.round(Rand * Range));
}