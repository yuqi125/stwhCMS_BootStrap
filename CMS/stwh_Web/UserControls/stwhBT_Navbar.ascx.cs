﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.UserControls
{
    public partial class stwhBT_Navbar : System.Web.UI.UserControl
    {
        /// <summary>
        /// 网站配置信息
        /// </summary>
        public stwh_website WebSite
        {
            get { return stwh_Web.stwh_admin.Common.WebSite.LoadWebSite(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            stwh_userinfo model = Session["htuser"] as stwh_userinfo;
            List<stwh_menuinfo> allmenu = new List<stwh_menuinfo>();
            //是超级管理员，获取全部菜单,包含属性隐藏的
            if (model.stwh_uiid == 1) allmenu = new stwh_Web.stwh_admin.Common.PageBase().GetMenuList();
            else allmenu = new stwh_menuinfoBLL().GetModelList(model.stwh_rid);

            //获取一级菜单数据
            List<stwh_menuinfo> menuparentlist = allmenu.Where(a => a.stwh_menuparentID == 0).OrderByDescending(a => a.stwh_menuorder).ToList<stwh_menuinfo>();
            string menustr = "";
            for (int i = 0; i < menuparentlist.Count; i++)
            {
                if (i == 0)
                {
                    menustr = "<div data-id=\"" + menuparentlist[i].stwh_menuid + "\" class=\"pmenu pmenu_cur\"><span class=\"" + menuparentlist[i].stwh_menuICO_url + "\"></span> " + menuparentlist[i].stwh_menuname + "</div>";
                }
                else menustr += "<div data-id=\"" + menuparentlist[i].stwh_menuid + "\" class=\"pmenu\"><span class=\"" + menuparentlist[i].stwh_menuICO_url + "\"></span> " + menuparentlist[i].stwh_menuname + "</div>";
            }
            this.pmenu.InnerHtml = menustr;
        }
    }
}