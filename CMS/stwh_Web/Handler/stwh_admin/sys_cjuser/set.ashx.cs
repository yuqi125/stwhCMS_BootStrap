﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.Web.SessionState;

namespace stwh_Web.Handler.stwh_admin.sys_cjuser
{
    /// <summary>
    /// set 的摘要说明
    /// </summary>
    public class set : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    string stoptime = context.Request["stoptime"];
                    if (string.IsNullOrEmpty(stoptime))
                    {
                        BaseHandler.SendResponseMsgs(context, "-1", "请设置活动注册截止时间！", 0);
                        return;
                    }
                    INIFile inifile = new INIFile(System.Web.HttpRuntime.AppDomainAppPath + "config/activity.ini");
                    inifile.IniWriteValue("activity", "key", stoptime);

                    stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 设置活动注册截止时间成功！");
                    BaseHandler.SendResponseMsgs(context, "0", "操作成功！", 0);
                }
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息：" + ex.StackTrace);
                BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}