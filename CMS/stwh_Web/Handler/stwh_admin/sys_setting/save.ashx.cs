﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using System.IO;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using stwh_Common.DEncrypt;
using Newtonsoft.Json;
using System.Web.SessionState;

namespace stwh_Web.Handler.stwh_admin.sys_setting
{
    /// <summary>
    /// save 的摘要说明
    /// </summary>
    public class save : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    string data = context.Request["data"];
                    if (string.IsNullOrEmpty(data))
                    {
                        BaseHandler.SendResponseMsgs(context, "-1", "请上传数据！", 0);
                        return;
                    }
                    List<stwh_FormModel> dataList = JsonConvert.DeserializeObject<List<stwh_FormModel>>(data);
                    if (dataList.Count == 0) BaseHandler.SendResponseMsgs(context, "-1", "没有数据", 0);
                    else
                    {
                        try
                        {
                            stwh_config model = FormModel.ToModel<stwh_config>(dataList);
                            model.stwh_cfversion = "V" + System.Reflection.Assembly.GetAssembly(this.GetType()).GetName().Version.ToString(3);
                            #region 校验
                            if (string.IsNullOrEmpty(model.stwh_cfico))
                            {
                                BaseHandler.SendResponseMsgs(context, "-1", "请上传网站图标！", 0);
                                return;
                            }
                            if (string.IsNullOrEmpty(model.stwh_cfwebname))
                            {
                                BaseHandler.SendResponseMsgs(context, "-1", "请设置网站名称！", 0);
                                return;
                            }
                            if (string.IsNullOrEmpty(model.stwh_cfuploadtype))
                            {
                                BaseHandler.SendResponseMsgs(context, "-1", "请设置上传文件类型！", 0);
                                return;
                            }
                            if (string.IsNullOrEmpty(model.stwh_cfuploadsize + ""))
                            {
                                BaseHandler.SendResponseMsgs(context, "-1", "请设置上传文件大小！", 0);
                                return;
                            }
                            if (model.stwh_cfuploadsize > 30720)
                            {
                                BaseHandler.SendResponseMsgs(context, "-1", "上传文件大小不能超过30720KB！", 0);
                                return;
                            }
                            if (model.stwh_cfuploadsize < 1)
                            {
                                BaseHandler.SendResponseMsgs(context, "-1", "上传文件大小不能小于1KB！", 0);
                                return;
                            }
                            if (model.stwh_cfthumbnail > 31457280)
                            {
                                BaseHandler.SendResponseMsgs(context, "-1", "缩略图上传文件大小不能超过307200KB！", 0);
                                return;
                            }
                            if (model.stwh_cfthumbnail < 1)
                            {
                                BaseHandler.SendResponseMsgs(context, "-1", "缩略图上传文件大小不能小于1字节！", 0);
                                return;
                            }
                            if (string.IsNullOrEmpty(model.stwh_cfcopyright))
                            {
                                BaseHandler.SendResponseMsgs(context, "-1", "请设置网站版权信息！", 0);
                                return;
                            }
                            if (!PageValidate.IsEmail(model.stwh_cfsendname))
                            {
                                BaseHandler.SendResponseMsgs(context, "-1", "发件人邮箱格式错误！", 0);
                                return;
                            }
                            if (!PageValidate.IsEmail(model.stwh_cfuser))
                            {
                                BaseHandler.SendResponseMsgs(context, "-1", "邮箱登陆名格式错误！", 0);
                                return;
                            }
                            if (string.IsNullOrEmpty(model.stwh_cfpwd))
                            {
                                BaseHandler.SendResponseMsgs(context, "-1", "请输入邮箱登陆密码！", 0);
                                return;
                            }
                            #endregion
                            model.stwh_cfpwd = DESEncrypt.Encrypt(model.stwh_cfpwd);
                            if (stwh_Web.stwh_admin.Common.WebSite.UpdateWebSite(model))
                            {
                                stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 保存网站配置信息成功！");
                                BaseHandler.SendResponseMsgs(context, "0", "操作成功！", 0);
                            }
                            else
                            {
                                stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 保存网站配置信息失败！");
                                BaseHandler.SendResponseMsgs(context, "-1", "保存失败，请仔细检查数据！", 0);
                            }
                        }
                        catch (Exception ex)
                        {
                            BaseHandler.SendResponseMsgs(context, "-1", "保存网站配置信息失败，"+ex.Message + "\r\n详细信息为：" + ex.StackTrace, 0);
                        }
                    }
                }
                else BaseHandler.SendResponseMsgs(context, "-1", "数据传输方法错误，请采用post方式！", 0);
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息："+ex.StackTrace); 
                BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}