﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.Web.SessionState;

namespace stwh_Web.Handler.stwh_admin.sys_notices
{
    /// <summary>
    /// service 的摘要说明
    /// </summary>
    public class service : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    #region 调用远程服务器系统公告内容
                    //StwhServiceSoapClient clientservice = new StwhServiceSoapClient();
                    //string result = clientservice.GetNotice(stwh_Web.stwh_admin.Common.WebSite.LoadWebSite().WebKey);
                    //if (!string.IsNullOrEmpty(result))
                    //{
                    //    stwh_result resultobj = new stwh_result();
                    //    resultobj = JsonConvert.DeserializeObject<stwh_result>(result);
                    //    if (resultobj.msgcode == (0 + "")) BaseHandler.SendResponseMsgs(context, "0", resultobj.msg.ToString(), resultobj.sumcount);
                    //}
                    BaseHandler.SendResponseMsgs(context, "0", "", 0);
                    #endregion
                }
                else BaseHandler.SendResponseMsgs(context, "-1", "数据传输方法错误，请采用post方式！", 0);
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息：" + ex.StackTrace);
                BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}