﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using stwh_Common;

namespace stwh_Web.Handler.stwh_admin
{
    public class ForbiddenHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html lang=\"zh-CN\">");
            sb.Append("<head>");
            sb.Append("<title></title>");
            sb.Append("</head>");
            sb.Append("<body>");
            sb.Append("<div style=\"width:100%; height:50px; line-height:50px; text-align:center; font-size:25px; font-weight:bold; border-bottom:1px solid #333;\">");
            sb.Append("403 Forbidden");
            sb.Append("</div>");
            sb.Append("</body>");
            sb.Append("</html>");
            context.Response.Write(sb.ToString());
        }
    }
}