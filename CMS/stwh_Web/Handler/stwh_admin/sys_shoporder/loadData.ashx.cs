﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.Web.SessionState;

namespace stwh_Web.Handler.stwh_admin.sys_shoporder
{
    /// <summary>
    /// loadData 的摘要说明
    /// </summary>
    public class loadData : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    string pageCount = context.Request["pageCount"];
                    string pageIndex = context.Request["pageIndex"];
                    string whereStr = context.Request["whereStr"];
                    if (PageValidate.IsNumber(pageCount) && PageValidate.IsNumber(pageIndex))
                    {
                        #region 分析查询条件
                        if (string.IsNullOrEmpty(whereStr)) whereStr = "1 = 1 and ";
                        else
                        {
                            List<stwh_FormModel> dataList = JsonConvert.DeserializeObject<List<stwh_FormModel>>(whereStr);
                            if (dataList.Count != 0)
                            {
                                whereStr = "1 = 1 and ";
                                foreach (stwh_FormModel item in dataList)
                                {
                                    if (item.Name.ToLower().Trim() == "stwh_bumobile")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value)) whereStr += "stwh_bumobile = '" + item.Value + "' and ";
                                    }
                                    else if (item.Name.ToLower().Trim() == "stwh_ortime")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value) && item.Value.Trim() != ",")
                                        {
                                            string[] times = item.Value.Split(',');
                                            whereStr += "stwh_ortime between '" + times[0] + "' and '" + times[1] + "' and ";
                                        }
                                    }
                                    else if (item.Name.ToLower().Trim() == "stwh_orstatus")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value) && item.Value != "-1") whereStr += "stwh_orstatus= " + item.Value + " and ";
                                    }
                                    else if (item.Name.ToLower().Trim() == "stwh_orpaystyle")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value) && item.Value != "-1") whereStr += "stwh_orpaystyle= " + item.Value + " and ";
                                    }
                                    else if (item.Name.ToLower().Trim() == "stwh_orddid")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value)) whereStr += "stwh_orddid = " + item.Value + " and ";
                                    }
                                }
                            }
                        }
                        whereStr += "1 = 1";
                        #endregion
                        if (loginModel.stwh_uiid != 1) whereStr = whereStr + " and stwh_uiid = " + loginModel.stwh_uiid + " and stwh_wxuid = " + context.Session["wxuid"].ToString();
                        else whereStr = whereStr + " and stwh_wxuid = " + context.Session["wxuid"].ToString();
                        int ss = 0, cc = 0;
                        List<stwh_order> ListData = new stwh_orderBLL().GetListByPage<stwh_order>("stwh_orid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);
                        BaseHandler.SendResponseMsgs(context, "0", ListData, cc);
                    }
                    else BaseHandler.SendResponseMsgs(context, "-1", "数据格式错误！", 0);
                }
                else BaseHandler.SendResponseMsgs(context, "-1", "数据传输方法错误，请采用post方式！", 0);
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息：" + ex.StackTrace);
                BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}