﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.Web.SessionState;
using System.Xml;

namespace stwh_Web.Handler.stwh_admin
{
    /// <summary>
    /// updateCache 的摘要说明
    /// </summary>
    public class updateCache : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    //更新菜单管理中菜单缓存
                    DataCache.AddMCache("cacheMenu", new stwh_menuinfoBLL().GetModelList(""));
                    //更新菜单功能缓存
                    DataCache.AddMCache("cacheFunction", new stwh_functionBLL().GetModelList(""));
                    //更新网站配置缓存
                    stwh_Web.stwh_admin.Common.WebSite.LoadWebSite(true);
                    //更新网站请求路径白名单
                    stwh_Web.stwh_admin.Common.WebSite.LoadChkURL(true);

                    stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 更新缓存数据成功！");
                    BaseHandler.SendResponseMsgs(context, "0", "操作成功！", 0);
                }
                else
                {
                    BaseHandler.SendResponseMsgs(context, "-1", "数据传输方法错误，请采用post方式！", 0);
                }
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息："+ex.StackTrace); 
                BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}