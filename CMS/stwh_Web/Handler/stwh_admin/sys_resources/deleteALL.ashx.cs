﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.Web.SessionState;

namespace stwh_Web.Handler.stwh_admin.sys_resources
{
    /// <summary>
    /// deleteALL 的摘要说明
    /// </summary>
    public class deleteALL : IHttpHandler, IRequiresSessionState
    {
        /// <summary>
        /// 删除目录下的所有文件
        /// </summary>
        /// <param name="path">目录绝对路径</param>
        private void DeleteFiles(string path)
        {
            //获取目录下的所有
            //string[] files = Directory.GetFiles(Server.MapPath(path));
            string[] fileslist = System.IO.Directory.GetFileSystemEntries(path);
            foreach (string ff in fileslist)
            {
                //判断是否是目录
                if (System.IO.Directory.Exists(ff)) DeleteFiles(ff);
                // 否则直接Delete文件
                else System.IO.File.Delete(ff);
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    if (new stwh_ResourcesBLL().DeleteListALL())
                    {
                        DeleteFiles(context.Server.MapPath(stwh_Web.stwh_admin.Common.WebSite.LoadWebSite().WebUploadDire));
                        stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 删除所有资源文件成功！");
                        BaseHandler.SendResponseMsgs(context, "0", "操作成功！", 0);
                        
                    }
                    else
                    {
                        stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 删除所有资源文件失败！");
                        BaseHandler.SendResponseMsgs(context, "-1", "删除失败！", 0);
                        
                    }
                }
                else
                {
                    BaseHandler.SendResponseMsgs(context, "-1", "数据传输方法错误，请采用post方式！", 0);
                    
                }
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息："+ex.StackTrace); BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
                
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}