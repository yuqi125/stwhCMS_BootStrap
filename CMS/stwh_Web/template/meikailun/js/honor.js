var Gallery = function(id,thumbMargin,auto,speed){
  var a = auto?auto:1;//自动播放，默认自动播放
  this.s = speed?speed:3500;//自动播放速度，默认2000
  var ct = this.ct = document.getElementById(id),
      sUl = this.sUl = ct.getElementsByTagName("ul")[0],
      tUl = this.tUl = ct.getElementsByTagName("ul")[1],
      sLiW = this.sLiW = sUl.getElementsByTagName("li")[0].offsetWidth;
  this.tLi = tUl.getElementsByTagName("li");
  var tLiW = this.tLiw = this.tLi[0].offsetWidth + thumbMargin,
         l = this.l = this.tLi.length;
  this.thumbMargin = thumbMargin;
  this.cur = 0;
  this.prev = ct.getElementsByTagName("a")[0];
  this.next = ct.getElementsByTagName("a")[1];
  sUl.style.width = l*sLiW + "px";   
  tUl.style.width = l*tLiW + "px"; 
  sUl.style.left = "0";
  tUl.style.left = "0";
  var that = this;
  for(var i=0;i<l;i++){
    (function(arg){
      tUl.getElementsByTagName("li")[i].onclick = function(){
        that.move(arg*sLiW);
          var j = 0;
          while(j<l){
            if(j==arg){ this.className = "cur";that.cur = j;}
            else { that.tLi[j].className = ""};
            j++;
          }
        };
      })(i);
    };
    this.prev.onclick = function(){that.prevMove()};
    this.next.onclick = function(){that.nextMove()};
    if(a){this.auto()};
  }
  Gallery.prototype = {
    auto : function(){
        var that = this;
        var timer = setInterval(function(){
          if(that.cur == that.l-1){
            that.tLi[that.cur].className = "";
            that.tUl.style.left = 0;
            that.cur = 0;
            that.tLi[that.cur].className = "cur";
            that.move(0);
          } 
          else {that.nextMove()}
        },this.s)
        this.ct.onmouseover = function(){ clearInterval(timer)};
        this.ct.onmouseout = function(){ that.auto()};
      },
    move : function(f_pos){
        var that = this;
        clearInterval(that.sUl.loop);
        this.sUl.loop = setInterval(function(){
          var left = -parseFloat(that.sUl.style.left);
          if(left == f_pos){
            clearInterval(that.sUl.loop)
          } else {
            var step = (left - f_pos)/10;
            step = step>0?Math.ceil(step):Math.floor(step);
            that.sUl.style.left = -left + step + "px";
          }
        },10)
      },
    prevMove : function(){
        if(this.cur == 0){
          this.cur = 0
        } else {
          this.tLi[this.cur].className = "";
          this.cur--;
          if(this.tLi[this.cur].offsetLeft+parseInt(this.tUl.style.left)<this.thumbMargin){
            this.button(1);
          }
		  var liw = this.cur.offsetWidth;
          this.tLi[this.cur].className = "cur";
          this.move(this.cur*this.sLiW);
        }
      },
    nextMove : function(){
        if(this.cur == this.l-1){
          this.cur = this.l-1
        } else {
          this.tLi[this.cur].className = "";
          this.cur++;
          if(this.tLi[this.cur].offsetLeft+parseInt(this.tUl.style.left)+this.thumbMargin>810){
            this.button(-1);
          }
		  var liw = this.cur.offsetWidth;
          this.tLi[this.cur].className = "cur";
          this.move(this.cur*this.sLiW);
        }
      },
    button : function(p){
		this.LiW = this.tLi[this.cur].offsetWidth + 7;
      this.tUl.style.left = parseInt(this.tUl.style.left) + p*this.LiW + "px";
  }
}
