if (typeof dwr == 'undefined' || dwr.engine == undefined) throw new Error('You must include DWR engine before including this file');

(function() {
  if (dwr.engine._getObject("dwr_sitenews") == undefined) {
    var p;
    
    p = {};
    p._path = '/dwr';

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setContent = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setContent', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setId = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setId', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.del = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'del', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.edit = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'edit', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getInfoType = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getInfoType', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getParentId = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getParentId', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getCategory = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getCategory', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.selectlist = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'selectlist', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setParentId = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setParentId', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getSimple = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getSimple', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setSimple = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setSimple', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setCategory = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setCategory', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setInfoType = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setInfoType', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getFname = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getFname', arguments);
    };

    /**
     * @param {class java.io.File} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setFname = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setFname', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setMenuId = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setMenuId', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getMenuId = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getMenuId', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getSectionId = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getSectionId', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setSectionId = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setSectionId', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getPublishtime = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getPublishtime', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setPublishtime = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setPublishtime', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getSmalltitle = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getSmalltitle', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setSmalltitle = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setSmalltitle', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getLargetitle = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getLargetitle', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setLargetitle = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setLargetitle', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getPublishstatus = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getPublishstatus', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setPublishstatus = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setPublishstatus', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.queryList = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'queryList', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getImageContentType = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getImageContentType', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setImageContentType = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setImageContentType', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.queryShareList = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'queryShareList', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.queryList_fzlc = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'queryList_fzlc', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getFnameFileName = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getFnameFileName', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setFnameFileName = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setFnameFileName', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getFnameContentType = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getFnameContentType', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setFnameContentType = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setFnameContentType', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getChildMenuId = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getChildMenuId', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setChildMenuId = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setChildMenuId', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.buildInfo_wlkg = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'buildInfo_wlkg', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setInfo = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setInfo', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.infoPublish = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'infoPublish', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.download = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'download', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getMenuIndex = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getMenuIndex', arguments);
    };

    /**
     * @param {class java.lang.Integer} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setMenuIndex = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setMenuIndex', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.personInfo = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'personInfo', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.saveShare = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'saveShare', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.tecResInfo = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'tecResInfo', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.irInfo = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'irInfo', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.add = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'add', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getId = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getId', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.list = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'list', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.save = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'save', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getContent = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getContent', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getTitle = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getTitle', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setTitle = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setTitle', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getSource = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getSource', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.info = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'info', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.publish = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'publish', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getImage = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getImage', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setSource = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setSource', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getInfo = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getInfo', arguments);
    };

    /**
     * @param {class java.io.File} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setImage = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setImage', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setImageFileName = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setImageFileName', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getImageFileName = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getImageFileName', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setPath = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setPath', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getImageUrl = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getImageUrl', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getWlkgTitle = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getWlkgTitle', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getFileUrl = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getFileUrl', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {class java.lang.Object} p1 a param
     * @param {function|Object} callback callback function or options object
     */
    p.addMessage = function(p0, p1, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'addMessage', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getWlkgDescription = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getWlkgDescription', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getWlkgKeywords = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getWlkgKeywords', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getPath = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getPath', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getParameter = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getParameter', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getRequest = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getRequest', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getResponse = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getResponse', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.setServletRequest = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setServletRequest', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.setServletResponse = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setServletResponse', arguments);
    };

    /**
     * @param {interface com.opensymphony.xwork2.inject.Container} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setContainer = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setContainer', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.pause = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'pause', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getErrors = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getErrors', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.hasErrors = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'hasErrors', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getTexts = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getTexts', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.getTexts = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getTexts', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.hasKey = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'hasKey', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.hasFieldErrors = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'hasFieldErrors', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getFieldErrors = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getFieldErrors', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.hasActionErrors = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'hasActionErrors', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getActionErrors = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getActionErrors', arguments);
    };

    /**
     * @param {interface java.util.Collection} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setActionErrors = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setActionErrors', arguments);
    };

    /**
     * @param {interface java.util.Collection} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setActionMessages = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setActionMessages', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getActionMessages = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getActionMessages', arguments);
    };

    /**
     * @param {interface java.util.Map} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.setFieldErrors = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'setFieldErrors', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.addActionError = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'addActionError', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.addActionMessage = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'addActionMessage', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {class java.lang.String} p1 a param
     * @param {function|Object} callback callback function or options object
     */
    p.addFieldError = function(p0, p1, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'addFieldError', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.hasActionMessages = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'hasActionMessages', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.clearErrorsAndMessages = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'clearErrorsAndMessages', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getErrorMessages = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getErrorMessages', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.clearFieldErrors = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'clearFieldErrors', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.clearActionErrors = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'clearActionErrors', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.clearMessages = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'clearMessages', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.clearErrors = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'clearErrors', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.doDefault = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'doDefault', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {class java.lang.String} p1 a param
     * @param {function|Object} callback callback function or options object
     */
    p.getFormatted = function(p0, p1, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getFormatted', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.clone = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'clone', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.execute = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'execute', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.getLocale = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getLocale', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.validate = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'validate', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {class java.lang.String} p1 a param
     * @param {interface java.util.List} p2 a param
     * @param {interface com.opensymphony.xwork2.util.ValueStack} p3 a param
     * @param {function|Object} callback callback function or options object
     */
    p.getText = function(p0, p1, p2, p3, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getText', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {class java.lang.String} p1 a param
     * @param {function|Object} callback callback function or options object
     */
    p.getText = function(p0, p1, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getText', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {function|Object} callback callback function or options object
     */
    p.getText = function(p0, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getText', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {interface java.util.List} p1 a param
     * @param {function|Object} callback callback function or options object
     */
    p.getText = function(p0, p1, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getText', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {class java.lang.String} p1 a param
     * @param {class [Ljava.lang.String;} p2 a param
     * @param {function|Object} callback callback function or options object
     */
    p.getText = function(p0, p1, p2, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getText', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {class java.lang.String} p1 a param
     * @param {interface java.util.List} p2 a param
     * @param {function|Object} callback callback function or options object
     */
    p.getText = function(p0, p1, p2, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getText', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {class [Ljava.lang.String;} p1 a param
     * @param {function|Object} callback callback function or options object
     */
    p.getText = function(p0, p1, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getText', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {class java.lang.String} p1 a param
     * @param {class [Ljava.lang.String;} p2 a param
     * @param {interface com.opensymphony.xwork2.util.ValueStack} p3 a param
     * @param {function|Object} callback callback function or options object
     */
    p.getText = function(p0, p1, p2, p3, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getText', arguments);
    };

    /**
     * @param {class java.lang.String} p0 a param
     * @param {class java.lang.String} p1 a param
     * @param {class java.lang.String} p2 a param
     * @param {function|Object} callback callback function or options object
     */
    p.getText = function(p0, p1, p2, callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'getText', arguments);
    };

    /**
     * @param {function|Object} callback callback function or options object
     */
    p.input = function(callback) {
      return dwr.engine._execute(p._path, 'dwr_sitenews', 'input', arguments);
    };
    
    dwr.engine._setObject("dwr_sitenews", p);
  }
})();

