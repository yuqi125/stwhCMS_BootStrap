﻿// JavaScript Document
$(document).ready(function () {
    $(".show_detail_1").click(function () {
        shopP();
    });
    $(".show_detail_class").click(function () {
        shopM();
    });

    $(".show_detail_review").click(function () {
        shopR();
    });

    $(".md").click(function () {
        $(window).scrollTop("500");
    })
    $(".ml").click(function () {
        $(".memu_link").find("a").removeClass("now");
        $(this).addClass("now");
    })

    //搜索点击
    $("#jobSearchBtn").click(function () {
        var text = $("#jobSearchText").val();
        if (text == "" || text == "搜索岗位关键字") {
            layer.msg("请输入搜索岗位关键字", 1, -1);
            $("#jobSearchText").focus();
            return false;
        }
        window.location = "/search/" + text;
    });

    //星星
    $("#ulCourseRating").rating({ allowRate: false }).append("<li style='float: left;height: 19px;'>&nbsp;&nbsp;(<a href='#divTabs'>" + setScore($("#ulCourseRating").attr("data-score")) + "分</a>)</li>");
    $("#ulCommentRating").rating();

    //评论页码
    var pageIndex = 1;

    //分页栏
    var PagingBar = onyx.ui.PagingBar;

    var pagingBar = new PagingBar({
        box: "divPagingBar",
        pageGoto: function (pageNo) {
            pageIndex = pageNo;
            comment.list();
            location.hash = "#md";
        }
    });

    pagingBar.setButtons({
        pageNo: 1,
        pagesCount: 1
    });

    var Post = {
        createPost: function () {
            var setting = {
                type: "post",
                dataType: "json"
            };
            var post = {};
            post.url = "";
            post.data = "";
            post.error = null;
            post.success = null;
            post.action = function () {
                var optins = { url: post.url, data: post.data, error: post.errorm, success: post.success };
                optins = $.extend(setting, optins);
                $.ajax(optins);
            };
            return post;
        }
    };

    var StrUtil = onyx.util.StrUtil;

    var Comment = {
        createComment: function () {
            var getPost = function (url, data, callBack) {
                var result = { result: 0, msg: "" };
                var post = Post.createPost();
                post.url = url;
                post.data = data;
                post.error = function (d) { result.result = 0; result.msg = "提交发生错误"; if ($.isFunction(callBack)) callBack.call(d, result); };
                post.success = function (d) {
                    if ($.isFunction(callBack)) callBack.call(d, result);
                };
                return post;
            };
            var posting = false;
            var comment = {};
            comment.groupID = "";
            comment.reviewID = "";
            comment.isCommented = false;
            comment.sayText = "";
            comment.star = 0;
            comment.face = "";
            comment.userName = "";
            comment.list = function () {
                var data = {};
                data.jobID = jobID;
                data.pageNo = pageIndex;
                data.pageSize = 10;
                var post = getPost("/course-coursereview-getlistByJobID.htm", data, function (r) {
                    if (this.result == 1) {
                        $("#comment").html("");
                        var sHtml = "";
                        $.each(this.list, function () {
                            if (this.reviewId == comment.reviewID)
                                return true;
                            sHtml += "<div class='feedback_member'>";
                            sHtml += "<dl>";
                            sHtml += "<dt>";
                            sHtml += "<img src='" + this.face + "?rnd=" + Math.random() + "'  alt='" + StrUtil.toHtml(this.userName) + "'/></dt>";
                            sHtml += "<dd>";
                            sHtml += "<strong><a href='javascript:void(0);' title='" + StrUtil.toHtml(this.userName) + "'></a>" + StrUtil.toHtml(this.userName);
                            if (this.groupId && this.groupId == "4") {
                                sHtml += "<i class='i1'></i>";
                            }
                            else if (this.groupId && this.groupId == "9") {
                                sHtml += "<i class='i2'></i>";
                            }
                            sHtml += "</strong>";
                            sHtml += StrUtil.toHtml(this.sayText);
                            sHtml += "<strong>课程：" + StrUtil.toHtml(this.courseName) + "</strong>";
                            sHtml += "</dd>";
                            sHtml += "</dl>";
                            sHtml += "<ul class='ulCommentRating' data-score='" + this.isGood + "'>";
                            sHtml += "</ul>";
                            //sHtml += "（" + this.isGood + "分）";
                            sHtml += "</div>";
                        });
                        $("#comment").html(sHtml);
                        $("#comment .ulCommentRating").each(function () {
                            $(this).rating({ allowRate: false }).append("<li style='float: left;height: 19px;'>&nbsp;&nbsp;(" + setScore($(this).attr("data-score")) + "分)</li>");
                        });
                        pagingBar.setButtons({
                            pageNo: data.pageNo,
                            pagesCount: this.pageInfo.pagesCount
                        });
                    }
                });
                post.action();
            };
            comment.get = function (afterSucceed) {
                var data = {};
                data.jobID = jobID;
                //data.courseId = courseId;
                data.userId = "#UserId#";
                var post = getPost("/course-coursereview-getjson.htm", data, function (r) {
                    if (this.result === 1) {
                        console.log(this.list.sayText);
                        if (this.list.sayTime) {
                            comment.isCommented = true;
                            comment.sayText = this.list.sayText;
                            comment.star = this.list.isGood;
                            comment.face = this.list.face;
                            comment.userName = this.list.userName;
                            comment.reviewID = this.list.reviewId;
                            comment.groupID = this.list.groupId
                            if ($.isFunction(afterSucceed)) afterSucceed.call(this.list);
                        }
                    }
                    else {
                        if ($.isFunction(afterSucceed)) afterSucceed.call(null);
                    }
                    return false;
                });
                post.action();
            };
            comment.initCommentUI = function () {
                var sHtml = "";
                sHtml += "<dt><img alt='' src='" + comment.face + "?rnd=" + Math.random() + "'/></dt>";
                sHtml += "<dd style='margin:0px;'><strong><a title='" + StrUtil.toHtml(comment.userName) + "' href='javascript:void(0);'></a>" + StrUtil.toHtml(comment.userName) + StrUtil.toHtml(comment.courseName);
                if (comment.groupID && comment.groupID == "4") {
                    sHtml += "<i class='i1'></i>";
                    //sHtml += "<i style='backgroup:url(http://www.yingsheng.com/content/images/ask/renzhen.jpg) no-repeat;width:64px;height:15px;'></i>";
                }
                else if (comment.groupID && comment.groupID == "9") {
                    sHtml += "<i class='i2'></i>";
                }
                sHtml += "</strong>" + StrUtil.toHtml(comment.sayText) + "</dd>";
                $("#dlCommentContent").html(sHtml);
                $("#saytext").val(StrUtil.toHtml(comment.sayText));
                $("#ulCommentRating").attr("data-comment", StrUtil.toHtml(comment.sayText)).attr("data-score", comment.star).empty().rating();
                //$("#btnComment").val("编辑评论");
                $("#ulCommentEdit").empty().attr("data-score", comment.star).rating({ allowRate: false }).append("<li style='float: left;height: 19px;'>&nbsp;&nbsp;(" + setScore(comment.star) + "分)</li>");
            };
            return comment;
        }
    };

    var comment = Comment.createComment();
    comment.list();

    //comment.get(function () {
    //    comment.list();
    //    if (this.reviewId != null) {
    //        comment.initCommentUI();
    //    }
    //    $(".nav_fix_info ul:eq(1)").click();
    //});
})
var shopP = function () {
    $(".show_detail_review").removeClass("now");
    $(".show_detail_class").removeClass("now");
    $(".show_detail_1").addClass("now");
    $(".detail_1").show();
    $("#commentList").hide()
    $(".detail_class").hide();
    $('.memu_link').find("a").removeClass("now");
    $("#xtbjs").addClass('now');
}
var shopM = function () {
    $(".show_detail_1").removeClass("now");
    $(".show_detail_review").removeClass("now");
    $(".show_detail_class").addClass("now");
    $(".detail_1").hide();
    $("#commentList").hide();
    $(".detail_class").show();
    $('.memu_link').find("a").removeClass("now");
    $("#kcml").addClass('now');
}

var shopR = function () {
    $(".show_detail_1").removeClass("now");
    $(".show_detail_class").removeClass("now");
    $(".show_detail_review").addClass("now");
    $("#commentList").show();
    $(".detail_1").hide();
    $(".detail_class").hide();
    $('.memu_link').find("a").removeClass("now");
    $("#returnCommandList").addClass('now');
}

window.onscroll = function () {
    var a = $(this).scrollTop(),
    b = $(".tab_title").offset().top,
    c = $(".tab_title").offset().top - 80;
    //d = $(".bottom_nav").offset().top - 500;
    if (a > b) {
        $(".nav_fix").addClass("show_nav_fix");
    } else {
        $(".nav_fix").removeClass("show_nav_fix");
    }

    if (a > c) {
        $(".memu_link").addClass("memu_link_fix");
    } else {
        $(".memu_link").removeClass("memu_link_fix");
    }
}
/*
(function () {
    $(document).ready(function () {
        $(".Current .s3").click(function () {
            if ($(".category_v3").is(":hidden") == true) {
                $("#allcat_ioc").addClass("i_show");
                $(".category_v3").toggle(300);
            } else {
                $("#allcat_ioc").removeClass("i_show");
                $(".category_v3").toggle(300);
            }
        });

        $(".fenx_for").mousemove(function () {
            $(".share_tool").show();
        });
        $(".share_tool").mousemove(function () {
            $(".share_tool").show();
        });
        $(".share_tool").mouseout(function () {
            if ($(".m_video").is(":hidden") != true) {
                $(".share_tool").hide();
            }
        });
        $(".fenx_for").mouseout(function () {
            if ($(".m_video").is(":hidden") != true) {
                $(".share_tool").hide();
            }
        });

        

    });
})();*/






//添加购物车
function toBuy(str) {
    addToShoppingCart(str, "B", function (success) {
        if (success) {
            location.href = "http://order.yingsheng.com/Order?source=1";
        }
        else {
            if (this.msg.indexOf("登录") > -1) {
                OpenLoginOrRegBox('f', '1');
            }
            else {
                alert(this.msg);
            }
        }

    });
    //var ShoppingCart = ys.ShoppingCart;
    //ShoppingCart.add({
    //    itemId: str,
    //    type: "B"
    //}, true);
}

function addToShoppingCart(id, courseType, callBack) {
    try {
        _adwq.push(['_setDataType', 'cart']);
        _adwq.push(['_setCustomer',
            yId
        ]);
        _adwq.push(['_setItem',
            jobPosId,    // 09890是一个例子，请填入商品编号  - 必填项
            name,       // 电视是一个例子，请填入商品名称  - 必填项
            newPrice,    // 12.00是一个例子，请填入商品金额  - 必填项
            '1',        // 1是一个例子，请填入商品数量  - 必填项
            'B',     // A123是一个例子，请填入商品分类编号  - 必填项
            '岗位系统班'        // 家电是一个例子，请填入商品分类名称  - 必填项
        ]);
        _adwq.push(['_trackTrans']);
    }
    catch (e) { }
    var data = {};
    data.id = id;
    data.type = courseType;
    data.count = 1;
    $.ajax({
        type: "Post",
        dataType: 'json',
        url: "/shoppingcart-shoppingcart-add.htm",
        async: false,
        data: data,
        success: function (d) {
            if ($.isFunction(callBack)) callBack.call(d, d.result === 1);
        }
    });
}

function setScore(obj) {
    var score = parseFloat(obj);
    if ($.isNumeric(score)) {
        return score == 0 ? "等您来评" : (parseFloat(score)).toFixed(1);
    }
    else {
        return "等您来评";//0.0;
    }
}
