/**
 * Created by Administrator on 2015/12/31.
 */
$(function () {
    //    var hzgy = $('.main-cont .main-cont-right ul li');
    //    hzgy.click(function () {
    //        var index = $(this).index();
    //        $(this).addClass('active').siblings('li').removeClass('active');
    //        $('.main-cont-left>div').eq(index).show().siblings().hide();
    //    });
    var anext = $('.gszc .page-next a');
    anext.click(function () {
        var index = $(this).index();
        $(this).addClass('active').siblings('a').removeClass('active');
        $('.page-content>div').eq(index).show().siblings().hide();
        return false;
    })
    var feng = $('.burning .burning-up .zpMenu DT');
    feng.click(function () {
        $(this).css('width', '0px').siblings('dt').css('width', '75px');
        var text = $(this).attr('data');
        //alert(text)
        if (text == "填埋") {
            $('.tab-bar .map img').attr('src', '/template/huanjing/images/tm_03.png');
            $('.bur-content .tm').show().siblings().hide();
        } else if (text == "焚烧") {
            $('.tab-bar .map img').attr('src', '/template/huanjing/images/fs_03.png');
            $('.bur-content .fs').show().siblings().hide();
        } else {
            $('.tab-bar .map img').attr('src', '/template/huanjing/images/zz_03.png');
            $('.bur-content .zz').show().siblings().hide();
        }
    })
    $('.button-icon a').hover(function () {
        var index = $(this).index();
        if (index == '0') {
            $(this).find('img').attr('src', '/template/huanjing/images/buttons_03.jpg');
        } else if (index == '1') {
            $(this).find('img').attr('src', '/template/huanjing/images/buttons_05.jpg');
        } else {
            $(this).find('img').attr('src', '/template/huanjing/images/buttons_07.jpg');
        }
    }, function () {
        $('.button-icon a').eq(0).find('img').attr('src', '/template/huanjing/images/buttons_12.jpg');
        $('.button-icon a').eq(1).find('img').attr('src', '/template/huanjing/images/buttons_13.jpg');
        $('.button-icon a').eq(2).find('img').attr('src', '/template/huanjing/images/buttons_14.jpg');
    })

    $('.bu-ser>div').hover(function () {
        $(this).children("div").dequeue().animate({
            "opacity": "1"
        }, 500);
        $(this).children("span").dequeue().animate({
            "opacity": "0.85"
        }, 500);
    }, function () {
        $(this).children("div").dequeue().animate({
            "opacity": "0"
        }, 500);
        $(this).children("span").dequeue().animate({
            "opacity": "1"
        }, 500);
    }).click(function () {
        var index = $(this).index();
        $(this).find('span').show().closest('div').siblings().find('span').hide();
        $('.bus>div').eq(index).show().siblings().hide();
    });

    $('.gsjs-tab a').click(function () {
        var index = $(this).index();
        $(this).addClass('active').siblings().removeClass('active');
        $('.xmjs-box>div').eq(index).show().siblings().hide();
        return false;
    })
    $('.zhaobiao table tr').each(function () {
        $(this).children('td').last().css('borderRight', 'none')
    })
    $('.zhaobiao table tr:odd').css('background', '#ebeef3')

    $('.pla-tabs .tab-top span').click(function () {
        var index = $(this).index();
        $(this).addClass('active').siblings().removeClass('active');
        $('.planning .pla-tabs .boxs>div').eq(index).show().siblings().hide();
    })
    $('.pla-tabs .tab-top span').click(function () {
        var index = $(this).index();
        $(this).addClass('active').siblings().removeClass('active');
        $('.research .pla-tabs .boxs>div').eq(index).show().siblings().hide();
    })
    $('.pla-tabs .tab-top span').click(function () {
        var index = $(this).index();
        $(this).addClass('active').siblings().removeClass('active');
        $('.operating .pla-tabs .boxs>div').eq(index).show().siblings().hide();
    })
    $('.service-div .ser-page a').click(function () {
        var index = $(this).index() - 1;
        $(this).addClass('active').siblings().removeClass('active');
        $('.znashi>div').eq(index).show().siblings().hide();
        return false;
    })
    $('.zz .list li').hover(function () {
        var index = $(this).index() + 1;
        $('.zz .map-img').attr('src', '/template/huanjing/images/zz' + index + '.jpg');
        $('#wifi').removeClass().addClass('animatedemo gif' + index);
    });

    $(".SearchA").hover(function () {
        $(this).children(".btnSearch").dequeue().animate({
            "opacity": "1"
        }, 500);
    }, function () {
        $(this).children(".btnSearch").dequeue().animate({
            "opacity": "0"
        }, 500);
    });

    $('.showAnimation').hover(function () {
        $(this).find('.contentView .hiveTop').animate({
            borderWidth: "50px",
            borderBottomWidth: "88px",
            borderLeftWidth: "none",
            borderRightWidth: "50px",
            borderTopWidth: "88px"
        }, 300, 'linear', function () {
            $('.showAnimation').find('.contentView .hiveTop').animate({
                borderWidth: "47px",
                borderBottomWidth: "83px",
                borderLeftWidth: "none",
                borderRightWidth: "47px",
                borderTopWidth: "83px"
            }, 300)
        })
        $(this).find('.contentView .hiveBottom').animate({
            borderWidth: "50px",
            borderBottomWidth: "88px",
            borderLeftWidth: "50px",
            borderRightWidth: "none",
            borderTopWidth: "88px"
        }, 299, 'linear', function () {
            $('.showAnimation').find('.contentView .hiveBottom').animate({
                borderWidth: "47px",
                borderBottomWidth: "83px",
                borderLeftWidth: "47px",
                borderRightWidth: "none",
                borderTopWidth: "83px"
            }, 299)
        })
        $(this).find('.contentView .hiveCenter').animate({
            width: "120px"
        }, 298, 'linear', function () {
            $('.showAnimation').find('.contentView .hiveCenter').animate({
                width: "115px"
            }, 298)
        })
        $(this).find('.contentView').animate({
            width: "225px",
            height: "176px",
            top: 0,
            left: 0
        }, 297, 'linear', function () {
            $('.showAnimation').find('.contentView').animate({
                width: "215px",
                height: "166px",
                top: 5,
                left: 5
            }, 298)
        })
    }, function () {

    }).click(function () {
        $('.animation').hide();
        $('.animationB').show();
    });
    $('#j').click(function () {
        $('.animation').show();
        $('.animationB').hide();
    });
    $('.positionA').click(function () {
        $('.animation').hide();
        $('.animationB').show();
    });
    $('.positionB').click(function () {
        $('.animation').show();
        $('.animationB').hide();
    });
    $('.enterBG').hover(function () {
        $(this).find('span').animate({
            width: '203px',
            height: '176px',
            top: '0',
            left: '0'
        }, 300, 'linear', function () {
            $('.enterBG').find('span').animate({
                width: "193px",
                height: "166px",
                top: 5,
                left: 5
            }, 298)
        })
    }, function () { });
})
