﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using stwh_Common.HttpProc;
using VTemplate.Engine;
using System.Text;
using Newtonsoft.Json;
using System.Reflection;
using System.IO;

namespace stwh_Web.stwh_admin.Common
{
    /// <summary>
    /// 模板类型
    /// </summary>
    public enum TemplateType
    {
        /// <summary>
        /// pc网站类型
        /// </summary>
        PC,
        /// <summary>
        /// 移动网站类型
        /// </summary>
        Mobile,
        /// <summary>
        /// 商城网站类型
        /// </summary>
        Shop
    }

    /// <summary>
    /// （PC）模板基类引擎
    /// </summary>
    public class PageBaseVT
    {
        #region 构造函数
        private string webtemplate = "default";
        private string _if = "1 =1 ";

        public PageBaseVT()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PageName">页面名称（index、about、news等）</param>
        public PageBaseVT(string PageName)
        {
            TpType = TemplateType.PC;
            webtemplate = WebSite.LoadWebSite().Webtemplate;
            _Document = new TemplateDocument(WebClient.GetRootPath() + "/template/" + webtemplate + "/" + PageName + ".html", Encoding.UTF8);
        }
        #endregion

        #region 属性
        private TemplateDocument _Document;

        /// <summary>
        /// 当前页面的模板文档对象
        /// </summary>
        protected TemplateDocument Document
        {
            get { return _Document; }
            set { _Document = value; }
        }

        private TemplateType _tpType = TemplateType.PC;

        /// <summary>
        /// 模板类型
        /// </summary>
        public TemplateType TpType
        {
            get { return _tpType; }
            set { _tpType = value; }
        }
        #endregion

        #region 校验数据的seo标题、关键词、描述
        /// <summary>
        /// 判断文章对象的seo标题、关键词、描述
        /// </summary>
        /// <param name="model"></param>
        private void CheckArticle(stwh_article model)
        {
            if (string.IsNullOrEmpty(model.stwh_atseotitle)) model.stwh_atseotitle = model.stwh_attitle;
            if (string.IsNullOrEmpty(model.stwh_atseokeywords))
            {
                if (string.IsNullOrEmpty(model.stwh_atbiaoqian))
                {
                    if (TpType == TemplateType.PC) model.stwh_atseokeywords = WebSite.LoadWebSite().Seokey;
                    else if (TpType == TemplateType.Mobile) model.stwh_atseokeywords = WebSite.LoadWebSite().Seokey_m;
                    else model.stwh_atseokeywords = WebSite.LoadWebSite().Seokey_s;
                }
                else model.stwh_atseokeywords = model.stwh_atbiaoqian;
            }
            if (string.IsNullOrEmpty(model.stwh_atseodescription))
            {
                if (string.IsNullOrEmpty(model.stwh_atjianjie))
                {
                    if (TpType == TemplateType.PC) model.stwh_atseodescription = WebSite.LoadWebSite().Seodescription;
                    else if (TpType == TemplateType.Mobile) model.stwh_atseodescription = WebSite.LoadWebSite().Seodescription_m;
                    else model.stwh_atseodescription = WebSite.LoadWebSite().Seodescription_s;
                }
                else model.stwh_atseodescription = model.stwh_atjianjie;
            }
        }

        /// <summary>
        /// 判断文件对象的seo标题、关键词、描述
        /// </summary>
        /// <param name="model"></param>
        private void CheckFile(stwh_files model)
        {
            if (string.IsNullOrEmpty(model.stwh_flseotitle)) model.stwh_flseotitle = model.stwh_fltitle;
            if (string.IsNullOrEmpty(model.stwh_flsetokeywords))
            {
                if (string.IsNullOrEmpty(model.stwh_flbiaoqian))
                {
                    if (TpType == TemplateType.PC) model.stwh_flsetokeywords = WebSite.LoadWebSite().Seokey;
                    else if (TpType == TemplateType.Mobile) model.stwh_flsetokeywords = WebSite.LoadWebSite().Seokey_m;
                    else model.stwh_flsetokeywords = WebSite.LoadWebSite().Seokey_s;
                }
                else model.stwh_flsetokeywords = model.stwh_flbiaoqian;
            }
            if (string.IsNullOrEmpty(model.stwh_flsetodescription))
            {
                if (string.IsNullOrEmpty(model.stwh_fljianjie))
                {
                    if (TpType == TemplateType.PC) model.stwh_flsetodescription = WebSite.LoadWebSite().Seodescription;
                    else if (TpType == TemplateType.Mobile) model.stwh_flsetodescription = WebSite.LoadWebSite().Seodescription_m;
                    else model.stwh_flsetodescription = WebSite.LoadWebSite().Seodescription_s;
                }
                else model.stwh_flsetodescription = model.stwh_fljianjie;
            }
        }

        /// <summary>
        /// 判断商品对象的seo标题、关键词、描述
        /// </summary>
        /// <param name="model"></param>
        private void CheckProduct(stwh_product model)
        {
            if (string.IsNullOrEmpty(model.stwh_pseotitle)) model.stwh_pseotitle = model.stwh_ptitle;
            if (string.IsNullOrEmpty(model.stwh_psetokeywords))
            {
                if (string.IsNullOrEmpty(model.stwh_pbiaoqian))
                {
                    if (TpType == TemplateType.PC) model.stwh_psetokeywords = WebSite.LoadWebSite().Seokey;
                    else if (TpType == TemplateType.Mobile) model.stwh_psetokeywords = WebSite.LoadWebSite().Seokey_m;
                    else model.stwh_psetokeywords = WebSite.LoadWebSite().Seokey_s;
                }
                else model.stwh_psetokeywords = model.stwh_pbiaoqian;
            }
            if (string.IsNullOrEmpty(model.stwh_psetodescription))
            {
                if (string.IsNullOrEmpty(model.stwh_pdescription))
                {
                    if (TpType == TemplateType.PC) model.stwh_psetodescription = WebSite.LoadWebSite().Seodescription;
                    else if (TpType == TemplateType.Mobile) model.stwh_psetodescription = WebSite.LoadWebSite().Seodescription_m;
                    else model.stwh_psetodescription = WebSite.LoadWebSite().Seodescription_s;
                }
                else model.stwh_psetodescription = model.stwh_pdescription;
            }
        }
        #endregion

        #region 获取数据集合
        /// <summary>
        /// 获取指定条件商品分类数据
        /// </summary>
        /// <returns></returns>
        public object GetProductTypeData()
        {
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = _if;
            //获取标签属性值
            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            List<stwh_producttype> ListData = new List<stwh_producttype>();
            VTemplate.Engine.Attribute showall = tag.Attributes["showall"];
            if (showall != null && !string.IsNullOrEmpty(showall.Value.GetValue().ToString()) && showall.Value.GetValue().ToString().ToLower().Trim() == "true")
            {
                ListData = new stwh_producttypeBLL().GetModelList(whereStr + " order by stwh_ptorder desc,stwh_ptid desc");
            }
            else
            {
                VTemplate.Engine.Attribute showtype = tag.Attributes["showtype"];
                if (showtype != null)
                {
                    List<stwh_producttype> AllLisTypetData;
                    StringBuilder sb = new StringBuilder();
                    string id = "";
                    switch (showtype.Value.GetValue().ToString().ToLower().Trim())
                    {
                        default:
                            //递归获取其它栏目的子栏目id
                            List<stwh_producttype> idlist = new stwh_producttypeBLL().GetModelList(string.Format("stwh_ptname like '%{0}%'", showtype.Value.GetValue().ToString().ToLower().Trim()));
                            if (idlist.Count > 0)
                            {
                                AllLisTypetData = new stwh_producttypeBLL().GetModelList("");
                                //查询当前栏目下的子栏目id
                                stwh_Web.Handler.stwh_admin.BaseHandler.ProductTypeList(AllLisTypetData, sb, idlist[0].stwh_ptid, 0);
                                id = idlist[0].stwh_ptid + "," + sb.ToString() + "0";
                            }
                            break;
                    }
                    if (!string.IsNullOrEmpty(id)) whereStr = whereStr + " and stwh_ptid in (" + id + ")";
                }

                int ss = 0, cc = 0;
                ListData = new stwh_producttypeBLL().GetListByPage<stwh_producttype>("stwh_ptorder desc,stwh_ptid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

                //设置全局相关属性（使用以下属性必须先调用GetProductTypeData函数，先调用没有任何效果）
                this.Document.Variables.SetValue("sptypesumcount", cc);
                this.Document.Variables.SetValue("sptypepageindex", pageIndex);
                this.Document.Variables.SetValue("sptypepagecount", pageCount);
            }

            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取指定条件的商品数据
        /// </summary>
        /// <returns></returns>
        public object GetProductData()
        {
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = "stwh_pissh = 1";
            string tid = HttpContext.Current.Request["tid"];
            if (!string.IsNullOrEmpty(tid) && PageValidate.IsNumber(tid))
            {
                whereStr = whereStr + " and stwh_ptid = " + tid;
                List<stwh_producttype> pttlist = new stwh_producttypeBLL().GetModelList(_if + " and stwh_ptid = " + tid);
                if (pttlist.Count != 0) this.Document.Variables.SetValue("producttypemodel", pttlist[0]);
                else this.Document.Variables.SetValue("producttypemodel", null);
                //获取商品分类参数列表
                this.Document.Variables.SetValue("ptparamlist", new stwh_producttype_pBLL().GetModelList(_if + " and stwh_ptid = " + tid + " order by stwh_ptporder desc", 1));
            }
            //获取标签属性值
            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            VTemplate.Engine.Attribute showtype = tag.Attributes["showtype"];
            if (showtype != null)
            {
                List<stwh_producttype> AllLisTypetData;
                StringBuilder sb = new StringBuilder();
                string id = "";
                switch (showtype.Value.GetValue().ToString().ToLower().Trim())
                {
                    default:
                        //递归获取其它栏目的子栏目id
                        List<stwh_producttype> idlist = new stwh_producttypeBLL().GetModelList(string.Format(_if + " and stwh_ptname like '%{0}%'", showtype.Value.GetValue().ToString().ToLower().Trim()));
                        if (idlist.Count > 0)
                        {
                            AllLisTypetData = new stwh_producttypeBLL().GetModelList(_if);
                            //查询当前栏目下的子栏目id
                            stwh_Web.Handler.stwh_admin.BaseHandler.ProductTypeList(AllLisTypetData, sb, idlist[0].stwh_ptid, 0);
                            id = idlist[0].stwh_ptid + "," + sb.ToString() + "0";
                            //设置全局属性
                            this.Document.Variables.SetValue("producttypemodel", idlist[0]);
                            //获取商品分类参数列表
                            this.Document.Variables.SetValue("ptparamlist", new stwh_producttype_pBLL().GetModelList(_if + " and stwh_ptid = " + idlist[0].stwh_ptid + " order by stwh_ptporder desc", 1));
                        }
                        break;
                }
                if (!string.IsNullOrEmpty(id)) whereStr = whereStr + " and stwh_ptid in (" + id + ")";
            }

            #region 解析带有表达式函数变量
            //获取变量表达式
            VTemplate.Engine.Attribute exp = tag.Attributes["exp"];
            //获取数据库字段名
            VTemplate.Engine.Attribute expname = tag.Attributes["expname"];
            //获取是否模糊查询
            VTemplate.Engine.Attribute expvague = tag.Attributes["expvague"];
            //获取分割符号
            VTemplate.Engine.Attribute expsplit = tag.Attributes["expsplit"];
            if (exp != null && !string.IsNullOrEmpty(exp.Value.GetValue().ToString()) && expname != null && !string.IsNullOrEmpty(expname.Value.GetValue().ToString()))
            {
                string expstr = exp.Value.GetValue().ToString();
                //数据库字段名
                string expitemname = expname.Value.GetValue().ToString();
                object objValue = null;
                //遍历当前模板中的变量
                foreach (Variable item in this.Document.Variables)
                {
                    if (expstr.ToLower().IndexOf(item.Name.ToLower()) > 0)
                    {
                        objValue = item.Value;
                        break;
                    }
                }
                if (objValue != null)
                {
                    string searchValue = "";
                    //获取对象中所有公共属性
                    PropertyInfo[] propertys = objValue.GetType().GetProperties();
                    foreach (PropertyInfo pi in propertys)
                    {
                        if (expstr.ToLower().IndexOf(pi.Name.ToLower()) > 0)
                        {
                            searchValue = pi.GetValue(objValue, null) + "";
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        //判断是否模糊查询
                        if (expvague != null && !string.IsNullOrEmpty(expvague.Value.GetValue().ToString()) && expvague.Value.GetValue().ToString().ToLower() == "true")
                        {
                            char charsplit = ',';
                            if (expsplit != null && !string.IsNullOrEmpty(expsplit.Value.GetValue().ToString())) charsplit = expsplit.Value.GetValue().ToString()[0];
                            string[] parlist = searchValue.Split(charsplit);
                            string lsif = "";
                            for (int i = 0; i < parlist.Length; i++)
                            {
                                if (i == (parlist.Length - 1)) lsif = lsif + expitemname + " like '%" + parlist[i] + "%'";
                                else lsif += expitemname + " like '%" + parlist[i] + "%' or ";
                            }
                            whereStr = whereStr + " and (" + lsif + ")";
                        }
                        else
                        {
                            whereStr = whereStr + " and " + expitemname + " = '" + searchValue + "'";
                        }
                    }
                }
            }
            #endregion

            List<stwh_product> ListData = new List<stwh_product>();
            VTemplate.Engine.Attribute showall = tag.Attributes["showall"];
            string idstr = "0";
            List<stwh_producttype_p> pvaluelist = new List<stwh_producttype_p>();
            if (showall != null && !string.IsNullOrEmpty(showall.Value.GetValue().ToString()) && showall.Value.GetValue().ToString().ToLower().Trim() == "true")
            {
                ListData = new stwh_productBLL().GetModelList(whereStr + " order by stwh_piszhiding desc,stwh_porder desc,stwh_pid desc");
                foreach (stwh_product item in ListData)
                {
                    idstr = idstr + "," + item.stwh_pid;
                }
                //查询商品参数
                pvaluelist = new stwh_producttype_pBLL().GetModelList(_if + " and stwh_pid in (" + idstr + ") order by stwh_ptporder desc", 0);
            }
            else
            {

                int ss = 0, cc = 0;
                ListData = new stwh_productBLL().GetListByPage<stwh_product>("stwh_piszhiding desc,stwh_porder desc,stwh_pid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);
                foreach (stwh_product item in ListData)
                {
                    idstr = idstr + "," + item.stwh_pid;
                }
                //查询商品参数
                pvaluelist = new stwh_producttype_pBLL().GetModelList(_if + " and stwh_pid in (" + idstr + ") order by stwh_ptporder desc", 0);

                //设置全局相关属性（使用以下属性必须先调用GetProductData函数，先调用没有任何效果）
                this.Document.Variables.SetValue("spsumcount", cc);
                this.Document.Variables.SetValue("sppageindex", pageIndex);
                this.Document.Variables.SetValue("sppagecount", pageCount);
                this.Document.Variables.SetValue("spid", tid);
            }
            foreach (stwh_product item in ListData)
            {
                CheckProduct(item);
            }
            this.Document.Variables.SetValue("sppvaluelist", pvaluelist);
            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取指定条件文件分类数据
        /// </summary>
        /// <returns></returns>
        public object GetFileTypeData()
        {
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = _if;
            //获取标签属性值
            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            List<stwh_filetype> ListData = new List<stwh_filetype>();
            VTemplate.Engine.Attribute showall = tag.Attributes["showall"];
            if (showall != null && !string.IsNullOrEmpty(showall.Value.GetValue().ToString()) && showall.Value.GetValue().ToString().ToLower().Trim() == "true")
            {
                ListData = new stwh_filetypeBLL().GetModelList(whereStr + " order by stwh_ftorder desc,stwh_ftid desc");
            }
            else
            {
                int ss = 0, cc = 0;
                ListData = new stwh_filetypeBLL().GetListByPage<stwh_filetype>("stwh_ftorder desc,stwh_ftid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

                //设置全局文章相关属性（使用以下属性必须先调用GetFileTypeData函数，先调用没有任何效果）
                this.Document.Variables.SetValue("ftypesumcount", cc);
                this.Document.Variables.SetValue("ftypepageindex", pageIndex);
                this.Document.Variables.SetValue("ftypepagecount", pageCount);
            }

            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取指定条件的文件数据
        /// </summary>
        /// <returns></returns>
        public object GetFilesData()
        {
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = "stwh_flissh = 1";
            string tid = HttpContext.Current.Request["tid"];
            if (!string.IsNullOrEmpty(tid) && PageValidate.IsNumber(tid))
            {
                whereStr = whereStr + " and stwh_ftid = " + tid;
                //设置全局属性
                this.Document.Variables.SetValue("filetypemodel", new stwh_filetypeBLL().GetModel(int.Parse(tid)));
            }
            //获取标签属性值
            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            VTemplate.Engine.Attribute showtype = tag.Attributes["showtype"];
            if (showtype != null)
            {
                string id = "";
                switch (showtype.Value.GetValue().ToString().ToLower().Trim())
                {
                    default:
                        List<stwh_filetype> idlist = new stwh_filetypeBLL().GetModelList(string.Format("stwh_ftname like '%{0}%'", showtype.Value.GetValue().ToString().ToLower().Trim()));
                        if (idlist.Count > 0)
                        {
                            id = idlist[0].stwh_ftid + "";
                            //设置全局属性
                            this.Document.Variables.SetValue("filetypemodel", idlist[0]);
                        }
                        break;
                }
                if (!string.IsNullOrEmpty(id)) whereStr = whereStr + " and stwh_ftid in (" + id + ")";
            }
            List<stwh_files> ListData = new List<stwh_files>();
            VTemplate.Engine.Attribute showall = tag.Attributes["showall"];
            if (showall != null && !string.IsNullOrEmpty(showall.Value.GetValue().ToString()) && showall.Value.GetValue().ToString().ToLower().Trim() == "true")
            {
                ListData = new stwh_filesBLL().GetModelList(whereStr + " order by stwh_fliszhiding desc,stwh_florder desc,stwh_flid desc");
            }
            else
            {
                int ss = 0, cc = 0;
                ListData = new stwh_filesBLL().GetListByPage<stwh_files>("stwh_fliszhiding desc,stwh_florder desc,stwh_flid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

                //设置全局文章相关属性（使用以下属性必须先调用GetArticleData函数，先调用没有任何效果）
                this.Document.Variables.SetValue("filesumcount", cc);
                this.Document.Variables.SetValue("filepageindex", pageIndex);
                this.Document.Variables.SetValue("filepagecount", pageCount);
                this.Document.Variables.SetValue("filetid", tid);
            }
            foreach (stwh_files item in ListData)
            {
                CheckFile(item);
            }
            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取指定条件文章类型数据
        /// </summary>
        /// <returns></returns>
        public object GetArtypeData()
        {
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = _if;
            //获取标签属性值
            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            VTemplate.Engine.Attribute showtype = tag.Attributes["showtype"];
            if (showtype != null)
            {
                List<stwh_artype> AllLisTypetData;
                StringBuilder sb = new StringBuilder();
                string id = "";
                switch (showtype.Value.GetValue().ToString().ToLower().Trim())
                {
                    case "news":
                        //递归获取新闻中心的子栏目id
                        AllLisTypetData = new stwh_artypeBLL().GetModelList("stwh_artparentid not in (0,2,3)");
                        stwh_Web.Handler.stwh_admin.BaseHandler.ArticleList(AllLisTypetData, sb, 1, 0);
                        id = sb.ToString() + "0";
                        break;
                    case "product":
                        //递归获取产品中心的子栏目id
                        AllLisTypetData = new stwh_artypeBLL().GetModelList("stwh_artparentid not in (0,1,3)");
                        stwh_Web.Handler.stwh_admin.BaseHandler.ArticleList(AllLisTypetData, sb, 2, 0);
                        id = sb.ToString() + "0";
                        break;
                    case "project":
                        //递归获取案例中心的子栏目id
                        AllLisTypetData = new stwh_artypeBLL().GetModelList("stwh_artparentid not in (0,1,2)");
                        stwh_Web.Handler.stwh_admin.BaseHandler.ArticleList(AllLisTypetData, sb, 3, 0);
                        id = sb.ToString() + "0";
                        break;
                    default:
                        //递归获取其它栏目的子栏目id
                        List<stwh_artype> idlist = new stwh_artypeBLL().GetModelList(string.Format("stwh_artname like '%{0}%'", showtype.Value.GetValue().ToString().ToLower().Trim()));
                        if (idlist.Count > 0)
                        {
                            AllLisTypetData = new stwh_artypeBLL().GetModelList("");
                            //查询当前栏目下的子栏目id
                            stwh_Web.Handler.stwh_admin.BaseHandler.ArticleList(AllLisTypetData, sb, idlist[0].stwh_artid, 0);
                            id = idlist[0].stwh_artid + "," + sb.ToString() + "0";
                        }
                        break;
                }
                if (!string.IsNullOrEmpty(id)) whereStr = whereStr + " and stwh_artid in (" + id + ")";
            }

            List<stwh_artype> ListData = new List<stwh_artype>();
            VTemplate.Engine.Attribute showall = tag.Attributes["showall"];
            if (showall != null && !string.IsNullOrEmpty(showall.Value.GetValue().ToString()) && showall.Value.GetValue().ToString().ToLower().Trim() == "true")
            {
                ListData = new stwh_artypeBLL().GetModelList(whereStr + " order by stwh_artorder desc,stwh_artid desc");
            }
            else
            {
                int ss = 0, cc = 0;
                ListData = new stwh_artypeBLL().GetListByPage<stwh_artype>("stwh_artorder desc,stwh_artid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

                //设置全局文章相关属性（使用以下属性必须先调用GetArtypeData函数，先调用没有任何效果）
                this.Document.Variables.SetValue("artypesumcount", cc);
                this.Document.Variables.SetValue("artypepageindex", pageIndex);
                this.Document.Variables.SetValue("artypepagecount", pageCount);
            }

            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取指定条件的文章数据
        /// </summary>
        /// <returns></returns>
        public object GetArticleData()
        {
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = "stwh_atissh = 1";
            string tid = HttpContext.Current.Request["tid"];
            if (!string.IsNullOrEmpty(tid) && PageValidate.IsNumber(tid))
            {
                whereStr = whereStr + " and stwh_artid = " + tid;
                //设置全局属性
                this.Document.Variables.SetValue("artypemodel", new stwh_artypeBLL().GetModel(int.Parse(tid)));
            }
            //获取标签属性值
            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            //获取当前分类下子分类数据（包含当前分类），若当前分类不存在，返回审核通过的最新数据
            VTemplate.Engine.Attribute showtype = tag.Attributes["showtype"];
            if (showtype != null)
            {
                List<stwh_artype> AllLisTypetData;
                StringBuilder sb = new StringBuilder();
                string id = "";
                switch (showtype.Value.GetValue().ToString().ToLower().Trim())
                {
                    case "news":
                        //递归获取新闻中心的子栏目id
                        AllLisTypetData = new stwh_artypeBLL().GetModelList("stwh_artid not in (2,3)");
                        stwh_Web.Handler.stwh_admin.BaseHandler.ArticleList(AllLisTypetData, sb, 1, 0);
                        id = "1," + sb.ToString() + "0";
                        break;
                    case "product":
                        //递归获取产品中心的子栏目id
                        AllLisTypetData = new stwh_artypeBLL().GetModelList("stwh_artid not in (1,3)");
                        stwh_Web.Handler.stwh_admin.BaseHandler.ArticleList(AllLisTypetData, sb, 2, 0);
                        id = "2," + sb.ToString() + "0";
                        break;
                    case "project":
                        //递归获取案例中心的子栏目id
                        AllLisTypetData = new stwh_artypeBLL().GetModelList("stwh_artid not in (1,2)");
                        stwh_Web.Handler.stwh_admin.BaseHandler.ArticleList(AllLisTypetData, sb, 3, 0);
                        id = "3," + sb.ToString() + "0";
                        break;
                    default:
                        //递归获取其它栏目的子栏目id
                        List<stwh_artype> idlist = new stwh_artypeBLL().GetModelList(string.Format("stwh_artname like '%{0}%'", showtype.Value.GetValue().ToString().ToLower().Trim()));
                        if (idlist.Count > 0)
                        {
                            AllLisTypetData = new stwh_artypeBLL().GetModelList("");
                            //查询当前栏目下的子栏目id
                            stwh_Web.Handler.stwh_admin.BaseHandler.ArticleList(AllLisTypetData, sb, idlist[0].stwh_artid, 0);
                            id = idlist[0].stwh_artid + "," + sb.ToString() + "0";
                            //设置全局属性
                            this.Document.Variables.SetValue("artypemodel", idlist[0]);
                        }
                        break;
                }
                if (!string.IsNullOrEmpty(id)) whereStr = whereStr + " and stwh_artid in (" + id + ")";
            }

            #region 解析带有表达式函数变量
            //获取变量表达式
            VTemplate.Engine.Attribute exp = tag.Attributes["exp"];
            //获取数据库字段名
            VTemplate.Engine.Attribute expname = tag.Attributes["expname"];
            //获取是否模糊查询
            VTemplate.Engine.Attribute expvague = tag.Attributes["expvague"];
            //获取分割符号
            VTemplate.Engine.Attribute expsplit = tag.Attributes["expsplit"];
            if (exp != null && !string.IsNullOrEmpty(exp.Value.GetValue().ToString()) && expname != null && !string.IsNullOrEmpty(expname.Value.GetValue().ToString()))
            {
                string expstr = exp.Value.GetValue().ToString();
                //数据库字段名
                string expitemname = expname.Value.GetValue().ToString();
                object objValue = null;
                //遍历当前模板中的变量
                foreach (Variable item in this.Document.Variables)
                {
                    if (expstr.ToLower().IndexOf(item.Name.ToLower()) > 0)
                    {
                        objValue = item.Value;
                        break;
                    }
                }
                if (objValue != null)
                {
                    string searchValue = "";
                    //获取对象中所有公共属性
                    PropertyInfo[] propertys = objValue.GetType().GetProperties();
                    foreach (PropertyInfo pi in propertys)
                    {
                        if (expstr.ToLower().IndexOf(pi.Name.ToLower()) > 0)
                        {
                            searchValue = pi.GetValue(objValue, null) + "";
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        //判断是否模糊查询
                        if (expvague != null && !string.IsNullOrEmpty(expvague.Value.GetValue().ToString()) && expvague.Value.GetValue().ToString().ToLower() == "true")
                        {
                            char charsplit = ',';
                            if (expsplit != null && !string.IsNullOrEmpty(expsplit.Value.GetValue().ToString())) charsplit = expsplit.Value.GetValue().ToString()[0];
                            string[] parlist = searchValue.Split(charsplit);
                            string lsif = "";
                            for (int i = 0; i < parlist.Length; i++)
                            {
                                if (i == (parlist.Length - 1)) lsif = lsif + expitemname + " like '%" + parlist[i] + "%'";
                                else lsif += expitemname + " like '%" + parlist[i] + "%' or ";
                            }
                            whereStr = whereStr + " and (" + lsif + ")";
                        }
                        else
                        {
                            whereStr = whereStr + " and " + expitemname + " = '" + searchValue + "'";
                        }
                    }
                }
            }
            #endregion
            List<stwh_article> ListData = new List<stwh_article>();
            VTemplate.Engine.Attribute showall = tag.Attributes["showall"];
            if (showall != null && !string.IsNullOrEmpty(showall.Value.GetValue().ToString()) && showall.Value.GetValue().ToString().ToLower().Trim() == "true")
            {
                ListData = new stwh_articleBLL().GetModelList(whereStr + " order by stwh_atiszhiding desc,stwh_atorder desc,stwh_atid desc");
            }
            else
            {
                int ss = 0, cc = 0;
                ListData = new stwh_articleBLL().GetListByPage<stwh_article>("stwh_atiszhiding desc,stwh_atorder desc,stwh_atid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

                //设置全局文章相关属性（使用以下属性必须先调用GetArticleData函数，先调用没有任何效果）
                this.Document.Variables.SetValue("articlesumcount", cc);
                this.Document.Variables.SetValue("articlepageindex", pageIndex);
                this.Document.Variables.SetValue("articlepagecount", pageCount);
                this.Document.Variables.SetValue("articletid", tid);
            }
            foreach (stwh_article item in ListData)
            {
                CheckArticle(item);
            }
            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取banner图片
        /// </summary>
        /// <returns></returns>
        public object GetBanner()
        {
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "4";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = _if;
            //获取标签属性值
            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "4" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            List<stwh_Banner> ListData = new List<stwh_Banner>();
            VTemplate.Engine.Attribute showall = tag.Attributes["showall"];
            if (showall != null && !string.IsNullOrEmpty(showall.Value.GetValue().ToString()) && showall.Value.GetValue().ToString().ToLower().Trim() == "true")
            {
                ListData = new stwh_BannerBLL().GetModelList(whereStr + " order by stwh_baorder desc");
            }
            else
            {
                int ss = 0, cc = 0;
                ListData = new stwh_BannerBLL().GetListByPage<stwh_Banner>("stwh_baorder", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

                //设置全局文章相关属性（使用以下属性必须先调用GetBanner函数，先调用没有任何效果）
                this.Document.Variables.SetValue("bannersumcount", cc);
                this.Document.Variables.SetValue("bannerpageindex", pageIndex);
                this.Document.Variables.SetValue("bannerpagecount", pageCount);
            }

            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取招标信息数据
        /// </summary>
        /// <returns></returns>
        public object GetBiddingData()
        {
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = "1 = 1";
            //获取标签属性值
            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            int ss = 0, cc = 0;
            List<stwh_bidding> ListData = new stwh_biddingBLL().GetListByPage<stwh_bidding>("stwh_bdorder desc,stwh_bdid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

            //设置全局文章相关属性（使用以下属性必须先调用GetBiddingData函数，先调用没有任何效果）
            this.Document.Variables.SetValue("biddingsumcount", cc);
            this.Document.Variables.SetValue("biddingpageindex", pageIndex);
            this.Document.Variables.SetValue("biddingpagecount", pageCount);

            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取网站公告数据
        /// </summary>
        /// <returns></returns>
        public object GetNoticeData()
        {
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = _if;
            //获取标签属性值
            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            int ss = 0, cc = 0;
            List<stwh_notice> ListData = new stwh_noticeBLL().GetListByPage<stwh_notice>("stwh_noorder desc,stwh_noid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

            //设置全局文章相关属性（使用以下属性必须先调用GetNoticeData函数，先调用没有任何效果）
            this.Document.Variables.SetValue("noticesumcount", cc);
            this.Document.Variables.SetValue("noticepageindex", pageIndex);
            this.Document.Variables.SetValue("noticepagecount", pageCount);

            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取网站留言数据
        /// </summary>
        /// <returns></returns>
        public object GetMessageData()
        {
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = "stwh_lmissh = 1";
            //获取标签属性值
            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            int ss = 0, cc = 0;
            List<stwh_leaveMSG> ListData = new stwh_leaveMSGBLL().GetListByPage<stwh_leaveMSG>("stwh_lmid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

            //设置全局文章相关属性（使用以下属性必须先调用GetMessageData函数，先调用没有任何效果）
            this.Document.Variables.SetValue("msgsumcount", cc);
            this.Document.Variables.SetValue("msgpageindex", pageIndex);
            this.Document.Variables.SetValue("msgpagecount", pageCount);

            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取指定条件的招聘数据
        /// </summary>
        /// <returns></returns>
        public object GetRecruitData()
        {
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = _if;
            //获取标签属性值
            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            List<stwh_recruit> ListData = new List<stwh_recruit>();
            VTemplate.Engine.Attribute showall = tag.Attributes["showall"];
            if (showall != null && !string.IsNullOrEmpty(showall.Value.GetValue().ToString()) && showall.Value.GetValue().ToString().ToLower().Trim() == "true")
            {
                ListData = new stwh_recruitBLL().GetModelList(whereStr + " order by stwh_rtid desc");
            }
            else
            {
                int ss = 0, cc = 0;
                ListData = new stwh_recruitBLL().GetListByPage<stwh_recruit>("stwh_rtid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

                //设置全局文章相关属性（使用以下属性必须先调用GetRecruitData函数，先调用没有任何效果）
                this.Document.Variables.SetValue("recsumcount", cc);
                this.Document.Variables.SetValue("recpageindex", pageIndex);
                this.Document.Variables.SetValue("recpagecount", pageCount);
            }

            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取指定条件的友情链接数据
        /// </summary>
        /// <returns></returns>
        public object GetBlogrollData()
        {
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = _if;
            //获取标签属性值
            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            List<stwh_blogroll> ListData = new List<stwh_blogroll>();
            VTemplate.Engine.Attribute showall = tag.Attributes["showall"];
            if (showall != null && !string.IsNullOrEmpty(showall.Value.GetValue().ToString()) && showall.Value.GetValue().ToString().ToLower().Trim() == "true")
            {
                ListData = new stwh_blogrollBLL().GetModelList(whereStr + " order by stwh_blorder desc,stwh_blid desc");
            }
            else
            {
                int ss = 0, cc = 0;
                ListData = new stwh_blogrollBLL().GetListByPage<stwh_blogroll>("stwh_blorder desc,stwh_blid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

                //设置全局文章相关属性（使用以下属性必须先调用GetBlogrollData函数，先调用没有任何效果）
                this.Document.Variables.SetValue("blosumcount", cc);
                this.Document.Variables.SetValue("blopageindex", pageIndex);
                this.Document.Variables.SetValue("blopagecount", pageCount);
            }

            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取搜索文章结果
        /// </summary>
        /// <returns></returns>
        public object GetSearchData()
        {
            List<stwh_article> ListData = new List<stwh_article>();
            int ss = 0, cc = 0;
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = "stwh_atissh = 1";
            string key = HttpContext.Current.Request["key"];
            if (!string.IsNullOrEmpty(key) && PageValidate.IsHasCHZNNum(key))
            {
                whereStr = whereStr + " and (" + string.Format("stwh_attitle like '%{0}%' or stwh_atbiaoqian like '%{1}%'", key, key) + ")";

                //获取标签属性值
                VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
                if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();


                VTemplate.Engine.Attribute showall = tag.Attributes["showall"];
                if (showall != null && !string.IsNullOrEmpty(showall.Value.GetValue().ToString()) && showall.Value.GetValue().ToString().ToLower().Trim() == "true")
                {
                    ListData = new stwh_articleBLL().GetModelList(whereStr + " order by stwh_atiszhiding desc,stwh_atorder desc,stwh_atid desc");
                }
                else
                {
                    ListData = new stwh_articleBLL().GetListByPage<stwh_article>("stwh_atiszhiding desc,stwh_atorder desc,stwh_atid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);
                }

                VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
                if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            }
            //设置全局相关属性（使用以下属性必须先调用GetSearchData函数，先调用没有任何效果）
            this.Document.Variables.SetValue("searchsumcount", cc);
            this.Document.Variables.SetValue("searchpageindex", pageIndex);
            this.Document.Variables.SetValue("searchpagecount", pageCount);
            this.Document.Variables.SetValue("searchkey", key);
            return ListData;
        }

        /// <summary>
        /// 获取搜索公告结果
        /// </summary>
        /// <returns></returns>
        public object GetSearchNoticeData()
        {
            List<stwh_notice> ListData = new List<stwh_notice>();
            int ss = 0, cc = 0;
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = "stwh_notid <> 1";
            string key = HttpContext.Current.Request["key"];
            if (!string.IsNullOrEmpty(key) && PageValidate.IsHasCHZNNum(key))
            {
                whereStr = whereStr + " and (" + string.Format("stwh_notitle like '%{0}%' ", key) + ")";

                //获取标签属性值
                VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
                if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();


                VTemplate.Engine.Attribute showall = tag.Attributes["showall"];
                if (showall != null && !string.IsNullOrEmpty(showall.Value.GetValue().ToString()) && showall.Value.GetValue().ToString().ToLower().Trim() == "true")
                {
                    ListData = new stwh_noticeBLL().GetModelList(whereStr + " order by stwh_noorder desc,stwh_noid desc");
                }
                else
                {
                    ListData = new stwh_noticeBLL().GetListByPage<stwh_notice>("stwh_noorder desc,stwh_noid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);
                }

                VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
                if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            }
            //设置全局相关属性（使用以下属性必须先调用GetSearchNoticeData函数，先调用没有任何效果）
            this.Document.Variables.SetValue("searchnoticesumcount", cc);
            this.Document.Variables.SetValue("searchnoticepageindex", pageIndex);
            this.Document.Variables.SetValue("searchnoticepagecount", pageCount);
            this.Document.Variables.SetValue("searchnoticekey", key);
            return ListData;
        }

        /// <summary>
        /// 获取搜索招聘结果
        /// </summary>
        /// <returns></returns>
        public object GetSearchJobData()
        {
            List<stwh_recruit> ListData = new List<stwh_recruit>();
            int ss = 0, cc = 0;
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = _if;
            string key = HttpContext.Current.Request["key"];
            if (!string.IsNullOrEmpty(key) && PageValidate.IsHasCHZNNum(key))
            {
                whereStr = whereStr + " and (" + string.Format("stwh_rtnamejd like '%{0}%' or stwh_rtname like '%{1}%' or stwh_rtplace like'%{2}%'", key, key, key) + ")";

                //获取标签属性值
                VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
                if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();


                VTemplate.Engine.Attribute showall = tag.Attributes["showall"];
                if (showall != null && !string.IsNullOrEmpty(showall.Value.GetValue().ToString()) && showall.Value.GetValue().ToString().ToLower().Trim() == "true")
                {
                    ListData = new stwh_recruitBLL().GetModelList(whereStr + " order by stwh_rtorder desc,stwh_rtid desc");
                }
                else
                {
                    ListData = new stwh_recruitBLL().GetListByPage<stwh_recruit>("stwh_rtorder desc,stwh_rtid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);
                }

                VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
                if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            }
            //设置全局相关属性（使用以下属性必须先调用GetSearchJobData函数，先调用没有任何效果）
            this.Document.Variables.SetValue("searchjobsumcount", cc);
            this.Document.Variables.SetValue("searchjobpageindex", pageIndex);
            this.Document.Variables.SetValue("searchjobpagecount", pageCount);
            this.Document.Variables.SetValue("searchjobkey", key);
            return ListData;
        }

        /// <summary>
        /// 获取搜索商品结果
        /// </summary>
        /// <returns></returns>
        public object GetSearchProductData()
        {
            List<stwh_product> ListData = new List<stwh_product>();
            int ss = 0, cc = 0;
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = HttpContext.Current.Request["page"];
            if (string.IsNullOrEmpty(pageIndex) || !PageValidate.IsNumber(pageIndex)) pageIndex = "0";
            string whereStr = "stwh_pissh = 1";

            string key = HttpContext.Current.Request["key"];
            if (!string.IsNullOrEmpty(key) && PageValidate.IsHasCHZNNum(key))
            {
                whereStr = whereStr + " and (" + string.Format("stwh_ptitlesimple like '%{0}%' or stwh_ptitle like '%{1}%' or stwh_pdescription like '%{2}%' or stwh_pbiaoqian like '%{3}%'", key, key, key, key) + ")";

                //获取标签属性值
                VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
                if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

                VTemplate.Engine.Attribute showall = tag.Attributes["showall"];
                if (showall != null && !string.IsNullOrEmpty(showall.Value.GetValue().ToString()) && showall.Value.GetValue().ToString().ToLower().Trim() == "true")
                {
                    ListData = new stwh_productBLL().GetModelList(whereStr + " order by stwh_piszhiding desc,stwh_porder desc,stwh_pid desc");
                }
                else
                {
                    ListData = new stwh_productBLL().GetListByPage<stwh_product>("stwh_piszhiding desc,stwh_porder desc,stwh_pid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);
                }
                foreach (stwh_product item in ListData)
                {
                    CheckProduct(item);
                }
                VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
                if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            }
            //设置全局相关属性（使用以下属性必须先调用GetSearchProductData函数，先调用没有任何效果）
            this.Document.Variables.SetValue("searchspsumcount", cc);
            this.Document.Variables.SetValue("searchsppageindex", pageIndex);
            this.Document.Variables.SetValue("searchsppagecount", pageCount);
            this.Document.Variables.SetValue("searchspkey", key);

            return ListData;
        }
        #endregion

        #region 获取某个表的所有数据

        #endregion

        #region 获取前几条数据
        /// <summary>
        /// 获取前几条指定条件文章数据（默认10条数据）
        /// </summary>
        /// <returns></returns>
        public object GetArticleDataTop()
        {
            //获取标签的属性
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = "0";
            string whereStr = "stwh_atissh = 1";

            VTemplate.Engine.Attribute pindex = tag.Attributes["pageindex"];
            if (pindex != null && PageValidate.IsNumber(pindex.Value.GetValue())) pageIndex = pindex.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            VTemplate.Engine.Attribute showtype = tag.Attributes["showtype"];
            if (showtype != null)
            {
                List<stwh_artype> AllLisTypetData;
                StringBuilder sb = new StringBuilder();
                string id = "";
                switch (showtype.Value.GetValue().ToString().ToLower().Trim())
                {
                    case "news":
                        //递归获取新闻中心的子栏目id
                        AllLisTypetData = new stwh_artypeBLL().GetModelList("stwh_artid not in (2,3)");
                        stwh_Web.Handler.stwh_admin.BaseHandler.ArticleList(AllLisTypetData, sb, 1, 0);
                        id = "1," + sb.ToString() + "0";
                        break;
                    case "product":
                        //递归获取产品中心的子栏目id
                        AllLisTypetData = new stwh_artypeBLL().GetModelList("stwh_artid not in (1,3)");
                        stwh_Web.Handler.stwh_admin.BaseHandler.ArticleList(AllLisTypetData, sb, 2, 0);
                        id = "2," + sb.ToString() + "0";
                        break;
                    case "project":
                        //递归获取案例中心的子栏目id
                        AllLisTypetData = new stwh_artypeBLL().GetModelList("stwh_artid not in (1,2)");
                        stwh_Web.Handler.stwh_admin.BaseHandler.ArticleList(AllLisTypetData, sb, 3, 0);
                        id = "3," + sb.ToString() + "0";
                        break;
                    default:
                        //递归获取其它栏目的子栏目id
                        List<stwh_artype> idlist = new stwh_artypeBLL().GetModelList(string.Format("stwh_artname like '%{0}%'", showtype.Value.GetValue().ToString().ToLower().Trim()));
                        if (idlist.Count > 0)
                        {
                            AllLisTypetData = new stwh_artypeBLL().GetModelList("");
                            stwh_Web.Handler.stwh_admin.BaseHandler.ArticleList(AllLisTypetData, sb, idlist[0].stwh_artid, 0);
                            id = idlist[0].stwh_artid + "," + sb.ToString() + "0";
                        }
                        break;
                }
                if (!string.IsNullOrEmpty(id)) whereStr = whereStr + " and stwh_artid in (" + id + ")";
            }

            #region 解析带有表达式函数变量
            //获取变量表达式
            VTemplate.Engine.Attribute exp = tag.Attributes["exp"];
            //获取数据库字段名
            VTemplate.Engine.Attribute expname = tag.Attributes["expname"];
            //获取是否模糊查询
            VTemplate.Engine.Attribute expvague = tag.Attributes["expvague"];
            //获取分割符号
            VTemplate.Engine.Attribute expsplit = tag.Attributes["expsplit"];
            if (exp != null && !string.IsNullOrEmpty(exp.Value.GetValue().ToString()) && expname != null && !string.IsNullOrEmpty(expname.Value.GetValue().ToString()))
            {
                string expstr = exp.Value.GetValue().ToString();
                //数据库字段名
                string expitemname = expname.Value.GetValue().ToString();
                object objValue = null;
                //遍历当前模板中的变量
                foreach (Variable item in this.Document.Variables)
                {
                    if (expstr.ToLower().IndexOf(item.Name.ToLower()) > 0)
                    {
                        objValue = item.Value;
                        break;
                    }
                }
                if (objValue != null)
                {
                    string searchValue = "";
                    //获取对象中所有公共属性
                    PropertyInfo[] propertys = objValue.GetType().GetProperties();
                    foreach (PropertyInfo pi in propertys)
                    {
                        if (expstr.ToLower().IndexOf(pi.Name.ToLower()) > 0)
                        {
                            searchValue = pi.GetValue(objValue, null) + "";
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        //判断是否模糊查询
                        if (expvague != null && !string.IsNullOrEmpty(expvague.Value.GetValue().ToString()) && expvague.Value.GetValue().ToString().ToLower() == "true")
                        {
                            char charsplit = ',';
                            if (expsplit != null && !string.IsNullOrEmpty(expsplit.Value.GetValue().ToString())) charsplit = expsplit.Value.GetValue().ToString()[0];
                            string[] parlist = searchValue.Split(charsplit);
                            string lsif = "";
                            for (int i = 0; i < parlist.Length; i++)
                            {
                                if (i == (parlist.Length - 1)) lsif = lsif + expitemname + " like '%" + parlist[i] + "%'";
                                else lsif += expitemname + " like '%" + parlist[i] + "%' or ";
                            }
                            whereStr = whereStr + " and (" + lsif + ")";
                        }
                        else
                        {
                            whereStr = whereStr + " and " + expitemname + " = '" + searchValue + "'";
                        }
                    }
                }
            }
            #endregion

            int ss = 0, cc = 0;
            List<stwh_article> ListData = new stwh_articleBLL().GetListByPage<stwh_article>("stwh_atiszhiding desc,stwh_atorder desc,stwh_atid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);
            foreach (stwh_article item in ListData)
            {
                CheckArticle(item);
            }
            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取前几条指定条件的文件数据（默认10条数据）
        /// </summary>
        /// <returns></returns>
        public object GetFilesDataTop()
        {
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = "0";
            string whereStr = "stwh_flissh = 1";

            //获取标签属性值
            VTemplate.Engine.Attribute pindex = tag.Attributes["pageindex"];
            if (pindex != null && PageValidate.IsNumber(pindex.Value.GetValue())) pageIndex = pindex.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            VTemplate.Engine.Attribute showtype = tag.Attributes["showtype"];
            if (showtype != null)
            {
                string id = "";
                switch (showtype.Value.GetValue().ToString().ToLower().Trim())
                {
                    default:
                        List<stwh_filetype> idlist = new stwh_filetypeBLL().GetModelList(string.Format("stwh_ftname like '%{0}%'", showtype.Value.GetValue().ToString().ToLower().Trim()));
                        if (idlist.Count > 0) id = idlist[0].stwh_ftid + "";
                        break;
                }
                if (!string.IsNullOrEmpty(id)) whereStr = whereStr + " and stwh_ftid in (" + id + ")";
            }

            int ss = 0, cc = 0;
            List<stwh_files> ListData = new stwh_filesBLL().GetListByPage<stwh_files>("stwh_fliszhiding desc,stwh_florder desc,stwh_flid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

            foreach (stwh_files item in ListData)
            {
                CheckFile(item);
            }
            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取前几条指定条件的商品数据（默认10条数据）
        /// </summary>
        /// <returns></returns>
        public object GetProductDataTop()
        {
            //获取当前出现数据标签
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = "0";
            string whereStr = "stwh_pissh = 1";

            //获取标签属性值
            VTemplate.Engine.Attribute pindex = tag.Attributes["pageindex"];
            if (pindex != null && PageValidate.IsNumber(pindex.Value.GetValue())) pageIndex = pindex.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            VTemplate.Engine.Attribute showtype = tag.Attributes["showtype"];
            if (showtype != null)
            {
                StringBuilder sb = new StringBuilder();
                string id = "";
                switch (showtype.Value.GetValue().ToString().ToLower().Trim())
                {
                    default:
                        List<stwh_producttype> idlist = new stwh_producttypeBLL().GetModelList(string.Format("stwh_ptname like '%{0}%'", showtype.Value.GetValue().ToString().ToLower().Trim()));
                        if (idlist.Count > 0)
                        {
                            List<stwh_producttype> AllLisTypetData = new stwh_producttypeBLL().GetModelList("");
                            //查询当前栏目下的子栏目id
                            stwh_Web.Handler.stwh_admin.BaseHandler.ProductTypeList(AllLisTypetData, sb, idlist[0].stwh_ptid, 0);
                            id = idlist[0].stwh_ptid + "," + sb.ToString() + "0";
                            //获取商品分类参数列表
                            this.Document.Variables.SetValue("ptparamlist", new stwh_producttype_pBLL().GetModelList("stwh_ptid = " + idlist[0].stwh_ptid + " order by stwh_ptporder desc", 1));
                        }
                        break;
                }
                if (!string.IsNullOrEmpty(id)) whereStr = whereStr + " and stwh_ptid in (" + id + ")";
            }

            #region 解析带有表达式函数变量
            //获取变量表达式
            VTemplate.Engine.Attribute exp = tag.Attributes["exp"];
            //获取数据库字段名
            VTemplate.Engine.Attribute expname = tag.Attributes["expname"];
            //获取是否模糊查询
            VTemplate.Engine.Attribute expvague = tag.Attributes["expvague"];
            //获取分割符号
            VTemplate.Engine.Attribute expsplit = tag.Attributes["expsplit"];
            if (exp != null && !string.IsNullOrEmpty(exp.Value.GetValue().ToString()) && expname != null && !string.IsNullOrEmpty(expname.Value.GetValue().ToString()))
            {
                string expstr = exp.Value.GetValue().ToString();
                //数据库字段名
                string expitemname = expname.Value.GetValue().ToString();
                object objValue = null;
                //遍历当前模板中的变量
                foreach (Variable item in this.Document.Variables)
                {
                    if (expstr.ToLower().IndexOf(item.Name.ToLower()) > 0)
                    {
                        objValue = item.Value;
                        break;
                    }
                }
                if (objValue != null)
                {
                    string searchValue = "";
                    //获取对象中所有公共属性
                    PropertyInfo[] propertys = objValue.GetType().GetProperties();
                    foreach (PropertyInfo pi in propertys)
                    {
                        if (expstr.ToLower().IndexOf(pi.Name.ToLower()) > 0)
                        {
                            searchValue = pi.GetValue(objValue, null) + "";
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        //判断是否模糊查询
                        if (expvague != null && !string.IsNullOrEmpty(expvague.Value.GetValue().ToString()) && expvague.Value.GetValue().ToString().ToLower() == "true")
                        {
                            char charsplit = ',';
                            if (expsplit != null && !string.IsNullOrEmpty(expsplit.Value.GetValue().ToString())) charsplit = expsplit.Value.GetValue().ToString()[0];
                            string[] parlist = searchValue.Split(charsplit);
                            string lsif = "";
                            for (int i = 0; i < parlist.Length; i++)
                            {
                                if (i == (parlist.Length - 1)) lsif = lsif + expitemname + " like '%" + parlist[i] + "%'";
                                else lsif += expitemname + " like '%" + parlist[i] + "%' or ";
                            }
                            whereStr = whereStr + " and (" + lsif + ")";
                        }
                        else
                        {
                            whereStr = whereStr + " and " + expitemname + " = '" + searchValue + "'";
                        }
                    }
                }
            }
            #endregion

            int ss = 0, cc = 0;
            List<stwh_product> ListData = new stwh_productBLL().GetListByPage<stwh_product>("stwh_piszhiding desc,stwh_porder desc,stwh_pid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

            foreach (stwh_product item in ListData)
            {
                CheckProduct(item);
            }
            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取前几条指定条件友情链接数据（默认10条数据）
        /// </summary>
        /// <returns></returns>
        public object GetBlogrollDataTop()
        {
            //获取标签的属性
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = "0";
            string whereStr = _if;
            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            int ss = 0, cc = 0;
            List<stwh_blogroll> ListData = new stwh_blogrollBLL().GetListByPage<stwh_blogroll>("stwh_blorder desc,stwh_blid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取前几条指定条件公告数据（默认10条数据）
        /// </summary>
        /// <returns></returns>
        public object GetNoticeDataTop()
        {
            //获取标签的属性
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = "0";
            string whereStr = _if;
            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            int ss = 0, cc = 0;
            List<stwh_notice> ListData = new stwh_noticeBLL().GetListByPage<stwh_notice>("stwh_noorder desc,stwh_noid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }

        /// <summary>
        /// 获取前几条指定条件招标数据（默认10条数据）
        /// </summary>
        /// <returns></returns>
        public object GetBiddingDataTop()
        {
            //获取标签的属性
            Tag tag = this.Document.CurrentRenderingTag;
            string pageCount = "10";
            string pageIndex = "0";
            string whereStr = "1 = 1";
            VTemplate.Engine.Attribute pcount = tag.Attributes["pagecount"];
            if (pcount != null && PageValidate.IsNumber(pcount.Value.GetValue())) pageCount = int.Parse(pcount.Value.GetValue().ToString()) <= 0 ? "10" : pcount.Value.GetValue().ToString();

            VTemplate.Engine.Attribute pwhere = tag.Attributes["wherestr"];
            if (pwhere != null && !string.IsNullOrEmpty(pwhere.Value.GetValue().ToString())) whereStr = whereStr + " and " + pwhere.Value.GetValue().ToString();

            int ss = 0, cc = 0;
            List<stwh_bidding> ListData = new stwh_biddingBLL().GetListByPage<stwh_bidding>("stwh_bdorder desc,stwh_bdid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);

            VTemplate.Engine.Attribute toJson = tag.Attributes["toJson"];
            if (toJson != null && toJson.Value.GetValue().ToString().Trim().ToLower() == "true") return JsonConvert.SerializeObject(ListData);
            return ListData;
        }
        #endregion

        #region 根据指定条件获取唯一数据
        public object GetStaticModel()
        {
            //获取标签
            Tag tag = this.Document.CurrentRenderingTag;
            //获取标签属性值
            VTemplate.Engine.Attribute id = tag.Attributes["id"];
            VTemplate.Engine.Attribute title = tag.Attributes["title"];
            if (id != null && !string.IsNullOrEmpty(id.Value.GetValue().ToString()) && PageValidate.IsNumber(id.Value.GetValue().ToString()))
            {
                List<stwh_static> list = new stwh_staticBLL().GetModelList(_if + " and stwh_stid = " + id);
                //设置全局属性
                if (list.Count != 0) return list[0];
            }
            else if (title != null && !string.IsNullOrEmpty(title.Value.GetValue().ToString()))
            {
                List<stwh_static> list = new stwh_staticBLL().GetModelList(_if + " and stwh_sttitle = '" + title.Value.GetValue().ToString() + "'");
                //设置全局属性
                if (list.Count != 0) return list[0];
            }
            return new stwh_static();
        }

        /// <summary>
        /// 获取指定id的招聘数据
        /// </summary>
        public void GetRecruitModel()
        {
            string id = HttpContext.Current.Request["id"];
            if (PageValidate.IsNumber(id))
            {
                List<stwh_recruit> listdata = new stwh_recruitBLL().GetModelList(_if + " and stwh_rtid = " + id);
                if (listdata.Count != 0) this.Document.Variables.SetValue("recruitmodel", listdata[0]);
                else this.Document.Variables.SetValue("recruitmodel", null);
            }
        }

        /// <summary>
        /// 获取指定id的网站公告数据
        /// </summary>
        public void GetNoticeModel()
        {
            string id = HttpContext.Current.Request["id"];
            if (PageValidate.IsNumber(id))
            {
                List<stwh_notice> listdata = new stwh_noticeBLL().GetModelList(_if + " and stwh_noid = " + id);
                if (listdata.Count != 0) this.Document.Variables.SetValue("noticemodel", listdata[0]);
                else this.Document.Variables.SetValue("noticemodel", null);
            }
        }

        /// <summary>
        /// 获取指定id的招标信息数据
        /// </summary>
        public void GetBiddingModel()
        {
            string id = HttpContext.Current.Request["id"];
            if (PageValidate.IsNumber(id))
            {
                //设置全局属性
                this.Document.Variables.SetValue("biddingmodel", new stwh_biddingBLL().GetModel(int.Parse(id)));
            }
        }

        /// <summary>
        /// 获取指定id的文章
        /// </summary>
        public void GetArticleModel()
        {
            stwh_article model = new stwh_article();
            string id = HttpContext.Current.Request["id"];
            if (PageValidate.IsNumber(id))
            {
                int ss = 0, cc = 0;
                List<stwh_article> ListData = new stwh_articleBLL().GetListByPage<stwh_article>("stwh_atiszhiding desc,stwh_atid", "desc", _if + " and stwh_atissh = 1 and stwh_atid = " + id, 1, 0, ref ss, ref cc, 0);
                foreach (stwh_article item in ListData)
                {
                    CheckArticle(item);
                }
                if (ListData.Count != 0) model = ListData[0];
                //设置全局属性
                this.Document.Variables.SetValue("articlemodel", model);
            }
        }

        /// <summary>
        /// 获取指定id的文件
        /// </summary>
        public void GetFileModel()
        {
            stwh_files model = new stwh_files();
            string id = HttpContext.Current.Request["id"];
            if (PageValidate.IsNumber(id))
            {
                int ss = 0, cc = 0;
                List<stwh_files> ListData = new stwh_filesBLL().GetListByPage<stwh_files>("stwh_fliszhiding desc,stwh_florder desc,stwh_flid", "desc", _if + " and stwh_flissh = 1 and stwh_flid = " + id, 1, 0, ref ss, ref cc, 0);
                foreach (stwh_files item in ListData)
                {
                    CheckFile(item);
                }
                if (ListData.Count != 0) model = ListData[0];
                //设置全局属性
                this.Document.Variables.SetValue("filemodel", model);
            }
        }

        /// <summary>
        /// 获取指定id的商品
        /// </summary>
        public void GetProductModel()
        {
            stwh_product model = new stwh_product();
            string id = HttpContext.Current.Request["id"];
            if (PageValidate.IsNumber(id))
            {
                int ss = 0, cc = 0;
                List<stwh_product> ListData = new stwh_productBLL().GetListByPage<stwh_product>("stwh_piszhiding desc,stwh_porder desc,stwh_pid", "desc", _if + " and stwh_pissh = 1 and stwh_pid = " + id, 1, 0, ref ss, ref cc, 0);
                List<stwh_producttype_p> pvaluelist = new stwh_producttype_pBLL().GetModelList(_if + " and stwh_pid = " + id + " order by stwh_ptporder desc", 0);

                foreach (stwh_product item in ListData)
                {
                    CheckProduct(item);
                }
                if (ListData.Count != 0) model = ListData[0];
                //设置全局属性
                this.Document.Variables.SetValue("productmodel", model);
                this.Document.Variables.SetValue("productpvalue", pvaluelist);
            }
        }
        #endregion

        /// <summary>
        /// 输出模板内容
        /// </summary>
        public virtual void OutPutHtml()
        {
            try
            {
                //设置全局变量属性值
                this.Document.Variables.SetValue("webconfig", WebSite.LoadWebSite());
                this.Document.Variables.SetValue("this", this);
                this.Document.Variables.SetValue("domain", WebClient.GetWebSite());

                //输出模板数据
                this.Document.Render(HttpContext.Current.Response.Output);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<!DOCTYPE html>");
                sb.Append("<html lang=\"zh-CN\">");
                sb.Append("<head>");
                sb.Append("<title></title>");
                sb.Append("</head>");
                sb.Append("<body>");
                sb.Append("<div>");
                sb.Append(ex.Message);
                sb.Append("</div>");
                sb.Append("</body>");
                sb.Append("</html>");
                StringWriter sw = new StringWriter(sb);
                this.Document.Render(sw);
            }
        }
    }
}