﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using VTemplate.Engine;
using stwh_Common;
using stwh_Common.HttpProc;
using System.Text;

namespace stwh_Web.stwh_admin.Common
{
    public class PageBaseMobileVT : PageBaseVT
    {
        public PageBaseMobileVT(string PageName)
        {
            string webtemplate = WebSite.LoadWebSite().Webtemplate_m;
            Document = new TemplateDocument(WebClient.GetRootPath() + "/templatemobile/" + webtemplate + "/" + PageName + ".html", Encoding.UTF8);
        }
    }
}