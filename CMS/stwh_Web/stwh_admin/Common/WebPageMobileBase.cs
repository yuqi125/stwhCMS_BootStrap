﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace stwh_Web.stwh_admin.Common
{
    public class WebPageMobileBase : System.Web.UI.Page
    {
        protected override void OnInit(EventArgs e)
        {
            try
            {
                //检查是否开启网站维护
                if (stwh_Web.stwh_admin.Common.WebSite.LoadWebSite().Webmaintain_m == 1)
                {
                    Response.Redirect("/mobile/maintain.html", false);
                    return;
                }
            }
            catch (Exception)
            {
            }
            base.OnInit(e);
        }

        protected override void OnError(EventArgs e)
        {
            Exception exception = Server.GetLastError();
            Response.Clear();
            HttpException httpException = exception as HttpException;
            int errorCode = (httpException == null ? 0 : httpException.GetHttpCode());
            Server.ClearError();
            switch (errorCode)
            {
                case 403:
                    Response.Redirect("/mobile/403.html");
                    break;
                case 404:
                    Response.Redirect("/mobile/404.html");
                    break;
                case 500:
                    stwh_Web.Handler.stwh_admin.BaseHandler.AddLog("系统异常：" + exception.Message + "——详细信息：" + exception.StackTrace);
                    HttpContext.Current.Session["error_mobile_500"] = exception.Message;
                    Response.Redirect("/mobile/500.html");
                    break;
                default:
                    stwh_Web.Handler.stwh_admin.BaseHandler.AddLog("系统异常：" + exception.Message + "——详细信息：" + exception.StackTrace);
                    HttpContext.Current.Session["error_mobile_other"] = exception.Message;
                    Response.Redirect("/mobile/other.html");
                    break;
            }
            base.OnError(e);
        }
    }
}