﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using stwh_Model;
using stwh_BLL;

namespace stwh_Web.stwh_admin.Common
{
    public class WebPageShopBase : WebPageBase
    {
        /// <summary>
        /// 校验会员是否登录
        /// </summary>
        /// <returns></returns>
        public stwh_buyuser CheckLogin()
        {
            stwh_buyuser model = null;
            object qtmodel = Session["shopqtmodel"];
            if (qtmodel != null) model = qtmodel as stwh_buyuser;
            return model;
        }

        protected override void OnInit(EventArgs e)
        {
            //检查是否开启网站维护
            if (stwh_Web.stwh_admin.Common.WebSite.LoadWebSite().Webmaintain_s == 1)
            {
                Response.Redirect("/shop/maintain.html", false);
                return;
            }
            //base.OnInit(e);
        }

        protected override void OnError(EventArgs e)
        {
            Exception exception = Server.GetLastError();
            Response.Clear();
            HttpException httpException = exception as HttpException;
            int errorCode = (httpException == null ? 0 : httpException.GetHttpCode());
            Server.ClearError();
            switch (errorCode)
            {
                case 403:
                    Response.Redirect("/shop/403.html");
                    break;
                case 404:
                    Response.Redirect("/shop/404.html");
                    break;
                case 500:
                    stwh_Web.Handler.stwh_admin.BaseHandler.AddLog("系统异常：" + exception.Message + "——详细信息：" + exception.StackTrace);
                    HttpContext.Current.Session["error_shop_500"] = exception.Message;
                    Response.Redirect("/shop/500.html");
                    break;
                default:
                    stwh_Web.Handler.stwh_admin.BaseHandler.AddLog("系统异常：" + exception.Message + "——详细信息：" + exception.StackTrace);
                    HttpContext.Current.Session["error_shop_other"] = exception.Message;
                    Response.Redirect("/shop/other.html");
                    break;
            }
            base.OnError(e);
        }
    }
}