﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_file
{
    public partial class update : Common.PageBase
    {
        public stwh_files UpdateModel = new stwh_files();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string mid = Request.QueryString["id"];
                if (PageValidate.IsNumber(mid))
                {
					SetMenuSpanText(this.menuSpan, "sys_file", "文章管理");
                    UpdateModel = new stwh_filesBLL().GetModel(int.Parse(mid));
                    List<stwh_filetype> AllListData = new stwh_filetypeBLL().GetModelList("");
                    StringBuilder listSave = new StringBuilder();
                    foreach (stwh_filetype item in AllListData)
                    {
                        listSave.Append("<li><a data-pid=\"" + item.stwh_ftid + "\">" + item.stwh_ftname + "</a></li>");
                    }
                    this.selectShowList.InnerHtml = listSave.ToString();
                }
            }
            catch (Exception)
            {

            }
        }
    }
}