﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_articles
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_articles", "文章管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                int ss = 0, cc = 0;
                List<stwh_article> ListData = new stwh_articleBLL().GetListByPage<stwh_article>("stwh_atid", "desc", "1=1", 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_article item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_atid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_atid + "\" value=\"" + item.stwh_atid + "\" /></td><td>"+item.stwh_atid+"</td><td><b>[" + item.stwh_artname + "]</b> " + item.stwh_attitle + "---[" + (item.stwh_atissh == 0 ? "<span style=\"color:red;\">待审核</span>" : "<span style=\"color:green;\">已审核</span>") + "]</td><td>" + item.stwh_ataddtime.ToString("yyyy-MM-dd HH:mm:ss") + "</td><td>" + (item.stwh_atiszhiding == 0 ? "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_atid + "\"value=\"0\" checked=\"checked\"/>不置顶</label><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_atid + "\"value=\"1\"/>置顶</label></div>" : "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_atid + "\"value=\"0\"/>不置顶</label><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_atid + "\"value=\"1\"/>置顶</label></div>") + "</td><td><input type=\"text\" min=\"0\" value=\"" + item.stwh_atorder + "\" onkeydown=\"return checkNumber(event);\" style=\"width: 50px;\"/></td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"6\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);

                List<stwh_artype> AllListData = new stwh_artypeBLL().GetModelList("");
                StringBuilder sb = new StringBuilder();
                sb.Append("<li><a data-pid=\"0\">请选择栏目</a></li>");
                sb.Append("<li class=\"divider\"></li>");
                for (int i = 0; i < AllListData.Count; i++)
                {
                    sb.Append("<li><a data-pid=\"" + AllListData[i].stwh_artid + "\">" + AllListData[i].stwh_artname + "</a></li>");
                    if (i != AllListData.Count - 1) sb.Append("<li class=\"divider\"></li>");
                }
                this.selectShowList.InnerHtml = sb.ToString();
            }
        }
    }
}