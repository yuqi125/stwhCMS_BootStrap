﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_banners.update" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">文章栏目管理</span></li>
        <li class="active">修改数据</li>
    </ol>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx"
    method="post" role="form">
    <div class="container-fluid">
        <div class="row">
            <input type="hidden" value="<%=UpdateModel.stwh_baid %>" name="stwh_baid" id="stwh_baid" />
            <input type="hidden" value="<%=UpdateModel.stwh_baaddtime %>" name="stwh_baaddtime" id="stwh_baaddtime" />
            <div class="col-sm-1 st-text-right">
                序号：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_baorder" name="stwh_baorder" onkeydown="return checkNumber(event);" value="<%=UpdateModel.stwh_baorder %>" class="st-input-text-300 form-control" />
                
                <span class="text-danger">*越大越考前</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                Banner图片：</div>
            <div class="col-sm-11 form-group">
                <input id="stwh_baurl" name="stwh_baurl" type="text" value="<%=UpdateModel.stwh_baurl %>" class="st-input-text-300 form-control" placeholder="请输入上传图片" style="float: left;" />
                <div style="float: left;width: 100px;">
                    <div id="upwebico">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div id="progressFile" class="progress progress-striped active" style=" display:none;">
                    <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                    </div>
                </div>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                访问路径：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_balink" name="stwh_balink" value="<%=UpdateModel.stwh_balink %>"  class="st-input-text-300 form-control" required="required" />
                
                <span class="text-danger">*点击Banner后跳转的地址，以http://开头</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                是否新窗口：</div>
            <div class="col-sm-11 form-group">
                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-primary btn-sm <%=UpdateModel.stwh_batarget=="_self"?"active":"" %>">
                        <input type="radio" name="stwh_batarget" id="stwh_batarget0" value="_self" <%=UpdateModel.stwh_batarget=="_self"?"checked=\"checked\"":"" %> />
                        否
                    </label>
                    <label class="btn btn-primary btn-sm <%=UpdateModel.stwh_batarget=="_blank"?"active":"" %>">
                        <input type="radio" name="stwh_batarget" id="stwh_batarget1" value="_blank" <%=UpdateModel.stwh_batarget=="_blank"?"checked=\"checked\"":"" %> />
                        是
                    </label>
                </div>
                
                <span class="text-danger">*点击Banner后是否打开新窗口</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                Banner描述：</div>
            <div class="col-sm-11 form-group">
                <textarea id="stwh_bams" name="stwh_bams" class="st-input-text-300 form-control"><%=UpdateModel.stwh_bams%></textarea>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-edit"></i> 修改</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script src="/Plugin/swfobject.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#addMenu").click(function () {
                var stwh_baorder = $("#stwh_baorder").val();
                if (!stwh_baorder) {
                    $.bs.alert("请输入Banner图片序号！", "info");
                    return false;
                }
                var stwh_baurl = $("#stwh_baurl").val();
                if (!stwh_baurl) {
                    $.bs.alert("请上传（输入）Banner图片！", "info");
                    return false;
                }
                else if (stwh_baurl.length > 300) {
                    $.bs.alert("Banner图片路径长度超出服务器限制！", "info");
                    return false;
                }
                var stwh_balink = $("#stwh_balink").val();
                if (!stwh_balink) {
                    $.bs.alert("请输入Banner图片的跳转链接！", "info");
                    return false;
                }
                else if (stwh_balink.length > 300) {
                    $.bs.alert("Banner图片访问路径长度超出服务器限制！", "info");
                    return false;
                }
                var stwh_bams = $("#stwh_bams").val();
                if (stwh_bams) {
                    if (stwh_bams.length > 100) {
                        $.bs.alert("Banner图片描述长度超出服务器限制！", "info");
                        return false;
                    }
                }
                $.post("/Handler/stwh_admin/sys_banners/update.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "-1");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        var auth = "<% = Request.Cookies[FormsAuthentication.FormsCookieName]==null ? string.Empty : Request.Cookies[FormsAuthentication.FormsCookieName].Value %>";
        var ASPSESSID = "<%= Session.SessionID %>";
        var flashvarsVoice = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: "*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG",
            FileType: "image",
            MaxSize: "<%=WebSite.Webthumbnail %>" //500KB 字节为单位
        };
        var params = {
            wmode: "transparent",
            play: true,
            loop: true,
            menu: true,
            devicefont: false,
            scale: "showall",
            quality: "high",
            bgcolor: "#ffffff",
            allowScriptAccess: "sameDomain",
            salign: ""
        };
        swfobject.embedSWF("/Plugin/upload.swf", "upwebico", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsVoice, { btnid: "upwebico", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);

        //设置进度
        function setProgress(info, name, IsSelect, btnid) {
            $("#progressFile").show().children().first().css("width", info + "%");
        }

        //上传完成时调用的方法（该方法被flash所调用）
        //filename:文件安上传成功后返回的文件名，包含扩展名,altString提示信息,msg服务器返回的成功消息
        function EndUpload(filename, altString, msg, btnid) {
            if (msg == "NoFile") {
                $.bs.alert("请选择文件！", "info");
            }
            else if (msg == "NoType") {
                $.bs.alert("请选择指定类型文件！", "info");
            }
            else if (msg == "NoSize") {
                $.bs.alert("文件大小超出限制，请联系管理员！", "info");
            }
            else if (msg == "login") {
                $.bs.alert("请先登录！", "info");
            }
            else if (msg == "Exception") {
                $.bs.alert("请稍后操作！", "info");
            }
            else {
                if ((/image/gi).test(msg)) {
                    if (btnid == "upwebico") {
                        $("#stwh_baurl").val(msg);
                    }
                }
                else {
                    $.bs.alert(msg, "info");
                }
            }
        }

        //上传失败时调用的方法（该方法被flash所调用）
        function ErrorUpload(msg) {
            $.bs.alert(msg, "info");
        }
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>