﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.IO;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using stwh_Common.DEncrypt;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_setting
{
    public partial class index : Common.PageBase
    {
        public string wmm1 = "active", wmm2 = "active", wmm_m1 = "active", wmm_m2 = "active", wmm_s1 = "active", wmm_s2 = "active";
        public stwh_config configmodel = new stwh_config();

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];

            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_setting", "基本信息设置");
                //设置功能按钮
                this.Literal1.Text = GetFunctionForRole(mid);
                stwh_userinfo model = Session["htuser"] as stwh_userinfo;
                #region 控件赋值
                if (WebSite != null)
                {
                    configmodel = stwh_Web.stwh_admin.Common.WebSite.WebSiteToConfig(WebSite);
                    this.stwh_cfid.Value = configmodel.stwh_cfid + "";
                    this.stwh_cfkeytime.Value = configmodel.stwh_cfkeytime;
                    this.stwh_cfkey.Value = configmodel.stwh_cfkey;
                }
                else
                {
                    this.stwh_cfid.Value = "0";
                    this.stwh_cfid.Disabled = true;
                }
                if (configmodel == null) configmodel = new stwh_config();
                this.stwh_cfico.Value = configmodel.stwh_cfico;
                this.stwh_cfico_m.Value = configmodel.stwh_cfico_m;
                this.stwh_cfico_s.Value = configmodel.stwh_cfico_s;
                this.stwh_cfwebname.Value = configmodel.stwh_cfwebname;
                this.stwh_cfwebname_m.Value = configmodel.stwh_cfwebname_m;
                this.stwh_cfwebname_s.Value = configmodel.stwh_cfwebname_s;
                this.stwh_cflogo.Value = configmodel.stwh_cflogo;
                this.stwh_cflogo_m.Value = configmodel.stwh_cflogo_m;
                this.stwh_cflogo_s.Value = configmodel.stwh_cflogo_s;
                this.stwh_cfwebtemplate.Value = configmodel.stwh_cfwebtemplate;
                this.stwh_cfmwebtemplate.Value = configmodel.stwh_cfmwebtemplate;
                this.stwh_cfshoptemplate.Value = configmodel.stwh_cfshoptemplate;

                #region 获取pc模板信息
                string templatename = "template";
                string path = Server.MapPath("/" + templatename + "/");
                string[] ff = Directory.GetDirectories(path);
                string innerhtml = "";
                for (int i = 0; i < ff.Length; i++)
                {
                    string chff = ff[i].Substring(ff[i].LastIndexOf("\\") + 1).ToLower();
                    if (configmodel.stwh_cfwebtemplate.ToLower() == chff) innerhtml += "<div class=\"col-sm-6 col-md-3\"><div class=\"thumbnail activethumbnail\" data-template=\"" + chff + "\"  title=\"" + chff + "\"><img src=\"/" + templatename + "/" + chff + "/index.jpg\" alt=\"\" style=\"height:200px;\"/></div></div>";
                    else innerhtml += "<div class=\"col-sm-6 col-md-3\"><div class=\"thumbnail\" data-template=\"" + chff + "\" title=\"" + chff + "\"><img src=\"/" + templatename + "/" + chff + "/index.jpg\" alt=\"\" style=\"height:200px;\"/></div></div>";
                }
                this.templateDiv.InnerHtml = innerhtml;
                #endregion
                #region 获取mobile模板信息
                templatename = "templatemobile";
                path = Server.MapPath("/" + templatename + "/");
                ff = Directory.GetDirectories(path);
                innerhtml = "";
                for (int i = 0; i < ff.Length; i++)
                {
                    string chff = ff[i].Substring(ff[i].LastIndexOf("\\") + 1).ToLower();
                    if (configmodel.stwh_cfmwebtemplate.ToLower() == chff) innerhtml += "<div class=\"col-sm-6 col-md-3\"><div class=\"thumbnail activethumbnail\" data-template=\"" + chff + "\"  title=\"" + chff + "\"><img src=\"/" + templatename + "/" + chff + "/index.jpg\" alt=\"\" style=\"height:200px;\"/></div></div>";
                    else innerhtml += "<div class=\"col-sm-6 col-md-3\"><div class=\"thumbnail\" data-template=\"" + chff + "\" title=\"" + chff + "\"><img src=\"/" + templatename + "/" + chff + "/index.jpg\" alt=\"\" style=\"height:200px;\"/></div></div>";
                }
                this.templateDiv_m.InnerHtml = innerhtml;
                #endregion
                #region 获取商城模板信息
                templatename = "templateshopping";
                path = Server.MapPath("/" + templatename + "/");
                ff = Directory.GetDirectories(path);
                innerhtml = "";
                for (int i = 0; i < ff.Length; i++)
                {
                    string chff = ff[i].Substring(ff[i].LastIndexOf("\\") + 1).ToLower();
                    if (configmodel.stwh_cfshoptemplate.ToLower() == chff) innerhtml += "<div class=\"col-sm-6 col-md-3\"><div class=\"thumbnail activethumbnail\" data-template=\"" + chff + "\"  title=\"" + chff + "\"><img src=\"/" + templatename + "/" + chff + "/index.jpg\" alt=\"\" style=\"height:200px;\"/></div></div>";
                    else innerhtml += "<div class=\"col-sm-6 col-md-3\"><div class=\"thumbnail\" data-template=\"" + chff + "\" title=\"" + chff + "\"><img src=\"/" + templatename + "/" + chff + "/index.jpg\" alt=\"\" style=\"height:200px;\"/></div></div>";
                }
                this.templateDiv_s.InnerHtml = innerhtml;
                #endregion

                #region 是否开启维护
                if (configmodel.stwh_cfweihu == 0)
                {
                    wmm2 = "";
                    this.stwh_cfweihu0.Attributes.Add("checked", "checked");
                }
                else
                {
                    wmm1 = "";
                    this.stwh_cfweihu1.Attributes.Add("checked", "checked");
                }
                if (configmodel.stwh_cfweihu_m == 0)
                {
                    wmm_m2 = "";
                    this.stwh_cfweihu_m0.Attributes.Add("checked", "checked");
                }
                else
                {
                    wmm_m1 = "";
                    this.stwh_cfweihu_m1.Attributes.Add("checked", "checked");
                }
                if (configmodel.stwh_cfweihu_s == 0)
                {
                    wmm_s2 = "";
                    this.stwh_cfweihu_s0.Attributes.Add("checked", "checked");
                }
                else
                {
                    wmm_s1 = "";
                    this.stwh_cfweihu_s1.Attributes.Add("checked", "checked");
                }
                #endregion

                this.stwh_cfuploadsize.Value = configmodel.stwh_cfuploadsize + "";
                this.stwh_cfthumbnail.Value = configmodel.stwh_cfthumbnail + "";
                this.stwh_cfuploadtype.Value = configmodel.stwh_cfuploadtype;
                this.stwh_cfcopyright.Value = configmodel.stwh_cfcopyright;
                this.stwh_cfcopyright_m.Value = configmodel.stwh_cfcopyright_m;
                this.stwh_cfcopyright_s.Value = configmodel.stwh_cfcopyright_s;
                this.stwh_cfkeywords.Value = configmodel.stwh_cfkeywords;
                this.stwh_cfkeywords_m.Value = configmodel.stwh_cfkeywords_m;
                this.stwh_cfkeywords_s.Value = configmodel.stwh_cfkeywords_s;
                this.stwh_cfdescription.Value = configmodel.stwh_cfdescription;
                this.stwh_cfdescription_m.Value = configmodel.stwh_cfdescription_m;
                this.stwh_cfdescription_s.Value = configmodel.stwh_cfdescription_s;
                this.stwh_cfsmtp.Value = configmodel.stwh_cfsmtp;
                this.stwh_cfport.Value = configmodel.stwh_cfport + "";
                this.stwh_cfsendname.Value = configmodel.stwh_cfsendname;
                this.stwh_cfname.Value = configmodel.stwh_cfname;
                this.stwh_cfuser.Value = configmodel.stwh_cfuser;
                this.stwh_cfpwd.Attributes.Add("placeholder", "请输入邮箱登陆密码");
                this.stwh_cfpwd.Attributes.Add("value", DESEncrypt.Decrypt(configmodel.stwh_cfpwd));
                #endregion
            }
        }
    }
}