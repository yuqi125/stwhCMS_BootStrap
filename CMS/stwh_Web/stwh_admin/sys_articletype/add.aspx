﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_articletype.add" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">文章栏目管理</span></li>
        <li class="active">添加数据</li>
    </ol>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx"
    method="post" role="form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1 st-text-right">
                归属栏目：</div>
            <div class="col-sm-11 form-group">
                <input type="hidden" id="stwh_artparentid" name="stwh_artparentid" value="0" />
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span id="selectShowText">一级栏目</span> <span class="caret"></span>
                </button>
                <ul id="selectShowList" class="dropdown-menu" role="menu" runat="server" style="max-height:300px; overflow:auto; left:auto; top:auto;">
                    <li><a data-pid="0">父级导航</a></li>
                    <li><a data-pid="0">系统设置</a></li>
                    <li><a data-pid="0">系统管理</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="0">内容管理</a></li>
                </ul>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                栏目图片：</div>
            <div class="col-sm-11 form-group">
                <input id="stwh_artimg" name="stwh_artimg" type="text" class="st-input-text-700 form-control" placeholder="请输入上传栏目图片" style="float: left;" />
                <div style="float: left;width: 100px;">
                    <div id="upwebico">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div id="progressFile" class="progress progress-striped active" style=" display:none;">
                    <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                    </div>
                </div>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                序号：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_artorder" name="stwh_artorder" onkeydown="return checkNumber(event);" value="1" class="st-input-text-300 form-control" />
                <span class="text-danger">*越大越考前</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                栏目名称：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_artname" name="stwh_artname" value=""  class="st-input-text-700 form-control" placeholder="请输入栏目名称" />
                
                <span class="text-danger">*栏目名称内容长度不能超过100个字符</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                栏目描述：</div>
            <div class="col-sm-11 form-group">
                <textarea id="stwh_artdescription" name="stwh_artdescription" class="st-input-text-700 form-control" placeholder="请输入栏目描述" ></textarea>
                
                <span class="text-danger">非必填，内容长度不能超过100字符</span>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-plus"></i> 添加</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script src="/Plugin/swfobject.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#selectShowList a").click(function () {
                $("#selectShowText").text($(this).text().Trim().split('.')[1]);
                $("#stwh_artparentid").val($(this).attr("data-pid"));
            });
            $("#addMenu").click(function () {
                var stwh_artimg = $("#stwh_artimg").val();
                if (stwh_artimg.length > 200) {
                    $.bs.alert("图片路径长度超出服务器限制！", "info");
                    return false;
                }
                var stwh_artname = $("#stwh_artname").val();
                if (!stwh_artname) {
                    $.bs.alert("请输入栏目名称！", "info");
                    return false;
                }
                else if (!IsHanZF(1,100,stwh_artname)) {
                    $.bs.alert("栏目名称长度超出服务器限制！", "info");
                    return false;
                }
                var stwh_artdescription = $("#stwh_artdescription").val();
                if (stwh_artdescription) {
                    if (!IsHanZF(1, 100, stwh_artdescription)) {
                        $.bs.alert("描述长度超出服务器限制！", "info");
                        return false;
                    }
                }
                $.post("/Handler/stwh_admin/sys_articletype/add.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        var auth = "<% = Request.Cookies[FormsAuthentication.FormsCookieName]==null ? string.Empty : Request.Cookies[FormsAuthentication.FormsCookieName].Value %>";
        var ASPSESSID = "<%= Session.SessionID %>";
        var flashvarsVoice = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: "*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG",
            FileType: "image",
            MaxSize: "<%=WebSite.Webthumbnail %>" //1000KB 字节为单位
        };
        var params = {
            wmode: "transparent",
            play: true,
            loop: true,
            menu: true,
            devicefont: false,
            scale: "showall",
            quality: "high",
            bgcolor: "#ffffff",
            allowScriptAccess: "sameDomain",
            salign: ""
        };
        swfobject.embedSWF("/Plugin/upload.swf", "upwebico", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsVoice, { btnid: "upwebico", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);

        //设置进度
        function setProgress(info, name, IsSelect, btnid) {
            $("#progressFile").show().children().first().css("width", info + "%");
        }

        //上传完成时调用的方法（该方法被flash所调用）
        //filename:文件安上传成功后返回的文件名，包含扩展名,altString提示信息,msg服务器返回的成功消息
        function EndUpload(filename, altString, msg, btnid) {
            if (msg == "NoFile") {
                $.bs.alert("请选择文件！", "info");
            }
            else if (msg == "NoType") {
                $.bs.alert("请选择指定类型文件！", "info");
            }
            else if (msg == "NoSize") {
                $.bs.alert("文件大小超出限制，请联系管理员！", "info");
            }
            else if (msg == "login") {
                $.bs.alert("请先登录！", "info");
            }
            else if (msg == "Exception") {
                $.bs.alert("请稍后操作！", "info");
            }
            else {
                if ((/image/gi).test(msg)) {
                    if (btnid == "upwebico") {
                        $("#stwh_artimg").val(msg);
                    }
                }
                else {
                    $.bs.alert(msg, "info");
                }
            }
        }

        //上传失败时调用的方法（该方法被flash所调用）
        function ErrorUpload(msg) {
            $.bs.alert(msg, "info");
        }
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>