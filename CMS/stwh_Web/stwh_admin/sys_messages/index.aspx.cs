﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_messages
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_messages", "留言管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);

                int ss = 0, cc = 0;
                List<stwh_leaveMSG> ListData = new stwh_leaveMSGBLL().GetListByPage<stwh_leaveMSG>("stwh_lmid", "desc", "1=1", 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_leaveMSG item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_lmid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_lmid + "\" value=\"" + item.stwh_lmid + "\" /></td><td>" + item.stwh_lmid + "</td><td>" + item.stwh_lmnc + "</td><td>" + item.stwh_lmname + "</td><td>" + item.stwh_lmage + "</td><td>" + (item.stwh_lmsex == 0 ? "男" : "女") + "</td><td>" + item.stwh_lmsj + "</td><td>" + item.stwh_lmemail + "</td><td>" + item.stwh_lmsubject + "</td><td>" + item.stwh_lmlynr + "</td><td>" + item.stwh_lmaddtime.ToString("yyyy-MM-dd hh:mm:ss") + "</td><td>" + (item.stwh_lmissh == 0 ? "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_lmid + "\"value=\"0\" checked=\"checked\"/>取消</label><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_lmid + "\"value=\"1\"/>审核</label></div>" : "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_lmid + "\"value=\"0\"/>取消</label><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_lmid + "\"value=\"1\"/>审核</label></div>") + "</td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"12\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);
            }
        }
    }
}