﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_links
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_links", "友情链接管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);

                int ss = 0, cc = 0;
                List<stwh_blogroll> ListData = new stwh_blogrollBLL().GetListByPage<stwh_blogroll>("stwh_blorder", "desc", "1=1", 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_blogroll item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_blid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_blid + "\" value=\"" + item.stwh_blid + "\" /></td><td>" + item.stwh_blid + "</td><td>" + item.stwh_blname + "</td><td>" + (item.stwh_blimg == "" ? "" : "<img src=\"" + item.stwh_blimg + "\" class=\"img-circle\" alt=\"" + item.stwh_blname + "\" style=\"width:100px; height:100px;\" />") + "</td><td><a href=\"" + item.stwh_blurl + "\" target=\"_blank\">" + item.stwh_blurl + "</a></td><td>" + item.stwh_blremark + "</td><td><input type=\"text\" min=\"0\" value=\"" + item.stwh_blorder + "\" onkeydown=\"return checkNumber(event);\" style=\"width: 50px;\"/></td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"7\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);
            }
        }
    }
}