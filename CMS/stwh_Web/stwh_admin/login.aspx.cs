﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace stwh_Web.stwh_admin
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.WebSite.LoadWebSite().Webcheck == 0) this.codediv.Visible = true;
            else this.codediv.Visible = false;
            this.user.Focus();
        }
    }
}