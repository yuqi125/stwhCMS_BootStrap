﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using stwh_Common.DEncrypt;

namespace stwh_Web.stwh_admin.sys_shopuser
{
    public partial class update : Common.PageBase
    {
        public stwh_buyuser UpdateModel = new stwh_buyuser();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SetMenuSpanText(this.menuSpan, "sys_shopuser", "会员管理");
                string mid = Request.QueryString["id"];
                if (PageValidate.IsNumber(mid))
                {
                    UpdateModel = new stwh_buyuserBLL().GetModel(int.Parse(mid));
                    UpdateModel.stwh_bupwd = DESEncrypt.Decrypt(UpdateModel.stwh_bupwd);
                }
            }
            catch (Exception)
            {

            }
        }
    }
}