﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_users
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_users", "用户管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                int ss = 0, cc = 0;
                List<stwh_userinfo> ListData = new stwh_userinfoBLL().GetListByPage<stwh_userinfo>("stwh_uiid", "desc", "1=1", 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_userinfo item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_uiid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_uiid + "\" value=\"" + item.stwh_uiid + "\" /></td><td>" + item.stwh_uiid + "</td><td>" + (item.stwh_uiportrait == "" ? "" : "<img src=\"" + item.stwh_uiportrait + "\" class=\"img-circle\" alt=\"" + item.stwh_uidescription + "\" style=\"width:50px; height:50px;\" />") + "</td><td>" + item.stwh_uiname + "</td><td>" + item.stwh_uilname + "</td><td>" + item.stwh_rname + "</td><td>" + item.stwh_rctime.ToString("yyyy-MM-dd HH:mm:ss") + "</td><td>" + item.stwh_uidescription + "</td><td>" + (item.stwh_uistatus == 0 ? "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_uiid + "\"value=\"0\" checked=\"checked\"/>启用</label><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_uiid + "\"value=\"1\"/>暂停</label></div>" : "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_uiid + "\"value=\"0\"/>启用</label><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_uiid + "\"value=\"1\"/>暂停</label></div>") + "</td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"9\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);
            }
        }
    }
}