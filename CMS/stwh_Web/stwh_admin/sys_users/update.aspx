﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_users.update" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">用户管理</span></li>
        <li class="active">修改用户</li>
    </ol>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx" method="post" role="form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1 st-text-right">
                所属角色：</div>
            <div class="col-sm-11 form-group">
                <input type="hidden" id="stwh_uiid" name="stwh_uiid" value="<%=UpdateModel.stwh_uiid %>" />
                <input type="hidden" id="stwh_uictime" name="stwh_uictime" value="<%=UpdateModel.stwh_uictime %>" />
                <input type="hidden" id="stwh_rid" name="stwh_rid" value="<%=UpdateModel.stwh_rid %>" />
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span id="selectShowText"><%=UpdateModel.stwh_rname%></span> <span class="caret"></span>
                </button>
                <ul id="selectShowList" class="dropdown-menu" role="menu" runat="server" style="max-height:300px; overflow:auto; left:auto; top:auto;">
                    <li><a data-pid="0">父级导航</a></li>
                    <li><a data-pid="0">系统设置</a></li>
                    <li><a data-pid="0">系统管理</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="0">内容管理</a></li>
                </ul>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                用户头像：</div>
            <div class="col-sm-11 form-group">
                <input id="stwh_uiportrait" name="stwh_uiportrait" type="text" class="st-input-text-300 form-control" placeholder="请上传用户头像" value="<%=UpdateModel.stwh_uiportrait %>" style="float: left;" />
                <div style="float: left;width: 100px;">
                    <div id="upwebico">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div id="progressFile" class="progress progress-striped active" style=" display:none;">
                    <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                    </div>
                </div>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                姓名：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_uiname" name="stwh_uiname" value="<%=UpdateModel.stwh_uiname %>"  class="st-input-text-300 form-control" required="required" />
                
                <span class="text-danger">*内容长度不能超过10个字符</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                登录名：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_uilname" name="stwh_uilname" value="<%=UpdateModel.stwh_uilname %>"  class="st-input-text-300 form-control" required="required" />
                
                <span class="text-danger">*内容长度不能超过10个字符，至少5个字符，并且不能重复</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                密码：</div>
            <div class="col-sm-11 form-group">
                <input type="password" id="stwh_uipwd" name="stwh_uipwd" value="<%=UpdateModel.stwh_uipwd %>"  class="st-input-text-300 form-control" required="required" />
                <span class="text-danger">*必须包含字母、数字、特称字符(+=_!@#&)，至少8个字符，最多12个字符</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                状态：</div>
            <div class="col-sm-11 form-group">
                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-primary btn-sm <%=UpdateModel.stwh_uistatus==0?"active":"" %>">
                        <input type="radio" name="stwh_uistatus" id="stwh_uistatus0" value="0" <%=UpdateModel.stwh_uistatus==0?"checked=\"checked\"":"" %>/>
                        启用
                    </label>
                    <label class="btn btn-primary btn-sm <%=UpdateModel.stwh_uistatus==1?"active":"" %>">
                        <input type="radio" name="stwh_uistatus" id="stwh_uistatus1" value="1" <%=UpdateModel.stwh_uistatus==1?"checked=\"checked\"":"" %> />
                        暂停
                    </label>
                </div>
                
                <span class="text-danger">*暂停后用户无法登陆系统后台</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                用户备注：</div>
            <div class="col-sm-11 form-group">
                <textarea id="stwh_uidescription" name="stwh_uidescription" class="st-input-text-300 form-control"><%=UpdateModel.stwh_uidescription%></textarea>
            </div>
        </div>
    </div>
    </form>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-edit"></i> 修改</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script src="/Plugin/swfobject.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#selectShowList a").click(function () {
                $("#selectShowText").text($(this).text());
                $("#stwh_rid").val($(this).attr("data-pid"));
            });
            $("#addMenu").click(function () {
                var stwh_rid = $("#stwh_rid").val();
                if (stwh_rid == 0) {
                    $.bs.alert("请选择所属角色！", "info");
                    return false;
                }
                var stwh_uiname = $("#stwh_uiname").val();
                if (!stwh_uiname) {
                    $.bs.alert("请输入姓名！", "info");
                    return false;
                }
                else if (!IsHanZF(1, 10, stwh_uiname)) {
                    $.bs.alert("姓名格式错误，不能超过10个字！", "info");
                    return false;
                }
                var stwh_uilname = $("#stwh_uilname").val();
                if (!stwh_uilname) {
                    $.bs.alert("请输入登录名！", "info");
                    return false;
                }
                else if (!IsZMNum(5, 10, stwh_uilname)) {
                    $.bs.alert("登录名格式错误，由数字字母组成，长度在5-10字符以内！", "info");
                    return false;
                }
                var stwh_uipwd = $("#stwh_uipwd").val();
                if (!stwh_uipwd) {
                    $.bs.alert("请输入登录密码！", "info");
                    return false;
                }
                else if (!IsPwdComplex(stwh_uipwd)) {
                    $.bs.alert("登录密码格式错误，必须包含字母、数字、特称字符(+=_!@#&)，至少8个字符，最多12个字符！", "danger");
                    return false;
                }
                var stwh_uidescription = $("#stwh_uidescription").val();
                if (stwh_uidescription) {
                    if (stwh_uidescription.length > 100) {
                        $.bs.alert("用户描述内容长度超出限制！", "info");
                        return false;
                    }
                }
                $.post("/Handler/stwh_admin/sys_users/update.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "-1");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        var auth = "<% = Request.Cookies[FormsAuthentication.FormsCookieName]==null ? string.Empty : Request.Cookies[FormsAuthentication.FormsCookieName].Value %>";
        var ASPSESSID = "<%= Session.SessionID %>";
        var flashvarsVoice = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: "*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG",
            FileType: "image",
            MaxSize: "<%=WebSite.Webthumbnail %>" //500KB 字节为单位
        };
        var params = {
            wmode: "transparent",
            play: true,
            loop: true,
            menu: true,
            devicefont: false,
            scale: "showall",
            quality: "high",
            bgcolor: "#ffffff",
            allowScriptAccess: "sameDomain",
            salign: ""
        };
        swfobject.embedSWF("/Plugin/upload.swf", "upwebico", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsVoice, { btnid: "upwebico", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);

        //设置进度
        function setProgress(info, name, IsSelect, btnid) {
            $("#progressFile").show().children().first().css("width", info + "%");
        }

        //上传完成时调用的方法（该方法被flash所调用）
        //filename:文件安上传成功后返回的文件名，包含扩展名,altString提示信息,msg服务器返回的成功消息
        function EndUpload(filename, altString, msg, btnid) {
            if (msg == "NoFile") {
                $.bs.alert("请选择文件！", "info");
            }
            else if (msg == "NoType") {
                $.bs.alert("请选择指定类型文件！", "info");
            }
            else if (msg == "NoSize") {
                $.bs.alert("文件大小超出限制，请联系管理员！", "info");
            }
            else if (msg == "login") {
                $.bs.alert("请先登录！", "info");
            }
            else if (msg == "Exception") {
                $.bs.alert("请稍后操作！", "info");
            }
            else {
                if ((/image/gi).test(msg)) {
                    if (btnid == "upwebico") {
                        $("#stwh_uiportrait").val(msg);
                    }
                }
                else {
                    $.bs.alert(msg, "info");
                }
            }
        }

        //上传失败时调用的方法（该方法被flash所调用）
        function ErrorUpload(msg) {
            $.bs.alert(msg, "info");
        }
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>