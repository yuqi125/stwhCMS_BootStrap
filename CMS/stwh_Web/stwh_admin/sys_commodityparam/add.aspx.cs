﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_commodityparam
{
    public partial class add : Common.PageBase
    {
        /// <summary>
        /// 获取文章栏目（并判断一级栏目下是否有子栏目）
        /// </summary>
        /// <param name="datasource">数据源</param>
        /// <param name="listSave">储存</param>
        /// <param name="pid">父id</param>
        /// <param name="dj">栏目等级</param>
        private void ArticleList(List<stwh_producttype> datasource, StringBuilder listSave, int pid, int dj)
        {
            //筛选出一级栏目
            List<stwh_producttype> listadd = new List<stwh_producttype>();
            foreach (stwh_producttype item in datasource)
            {
                if (item.stwh_ptparentid == pid) listadd.Add(item);
            }
            if (listadd.Count != 0)
            {
                //循环递归判断当前栏目下是否有子栏目
                foreach (stwh_producttype item in listadd)
                {
                    listSave.Append("<li><a data-pid=\"" + item.stwh_ptid + "\">");
                    for (int i = 0; i < dj; i++) listSave.Append("&nbsp;&nbsp;");
                    listSave.Append("(" + (dj + 1) + ")." + item.stwh_ptname + "</a></li>");
                    ArticleList(datasource, listSave, item.stwh_ptid, dj + 1);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
				SetMenuSpanText(this.menuSpan, "sys_commodityparam", "文章管理");
                List<stwh_producttype> AllListData = new stwh_producttypeBLL().GetModelList("");
                StringBuilder sb = new StringBuilder();
                ArticleList(AllListData, sb, 0, 0);
                this.selectShowList.InnerHtml = sb.ToString();
            }
            catch (Exception)
            {

            }
        }
    }
}