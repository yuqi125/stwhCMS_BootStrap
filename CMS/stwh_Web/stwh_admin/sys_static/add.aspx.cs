﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace stwh_Web.stwh_admin.sys_static
{
    public partial class add : Common.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetMenuSpanText(this.menuSpan, "sys_static", "静态内容管理");
        }
    }
}