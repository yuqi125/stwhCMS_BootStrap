﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_bidding.update" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">文章栏目管理</span></li>
        <li class="active">修改数据</li>
    </ol>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx"
    method="post" role="form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1 st-text-right">
                序号：</div>
            <div class="col-sm-11 form-group">
                <input type="hidden" id="stwh_bdid" name="stwh_bdid" value="<%=UpdateModel.stwh_bdid%>" />
                <input type="hidden" id="stwh_bdaddtime" name="stwh_bdaddtime" value="<%=UpdateModel.stwh_bdaddtime%>" />
                <input type="text" id="stwh_bdorder" name="stwh_bdorder" onkeydown="return checkNumber(event);"
                    value="<%=UpdateModel.stwh_bdorder%>" class="st-input-text-300 form-control" />
                <span class="text-danger">*越大越考前</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                编号：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_bdnumber" name="stwh_bdnumber" onkeydown="return checkNumber(event);" value="<%=UpdateModel.stwh_bdnumber%>" class="st-input-text-300 form-control" />
                <span class="text-danger"></span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                缩略图：</div>
            <div class="col-sm-11 form-group">
                <input id="stwh_bdimg" name="stwh_bdimg" type="text" value="<%=UpdateModel.stwh_bdimg%>" class="st-input-text-700 form-control"
                    placeholder="请上传缩略图" style="float: left;" />
                <div style="float: left; width: 100px;">
                    <div id="upwebico2">
                    </div>
                </div>
                <div class="clearfix">
                </div>
                <div id="progressFile2" class="progress progress-striped active" style="display: none;">
                    <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                    </div>
                </div>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                附件：</div>
            <div class="col-sm-11 form-group">
                <input id="stwh_bdpath" name="stwh_bdpath" type="text" value="<%=UpdateModel.stwh_bdpath%>" class="st-input-text-700 form-control"
                    placeholder="请上传附件或输入文件链接" style="float: left;" />
                <div style="float: left; width: 100px;">
                    <div id="upwebico1">
                    </div>
                </div>
                <div class="clearfix">
                </div>
                <div id="progressFile1" class="progress progress-striped active" style="display: none;">
                    <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                    </div>
                </div>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                招标标题：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_bdtitle" name="stwh_bdtitle" value="<%=UpdateModel.stwh_bdtitle%>" class="st-input-text-700 form-control"
                    required="required" />
                <span class="text-danger"></span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                招标简介：</div>
            <div class="col-sm-11 form-group">
                <textarea id="stwh_bdjianjie" name="stwh_bdjianjie" class="st-input-text-700 form-control" placeholder="请输入招标简介" ><%=UpdateModel.stwh_bdjianjie%></textarea>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                招标公司：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_bdcompany" name="stwh_bdcompany" value="<%=UpdateModel.stwh_bdcompany%>" class="st-input-text-700 form-control"
                    required="required" />
                <span class="text-danger"></span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                联系人：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_bdlxr" name="stwh_bdlxr" value="<%=UpdateModel.stwh_bdlxr%>" class="st-input-text-300 form-control"
                    required="required" />
                <span class="text-danger"></span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                联系电话：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_bdtel" name="stwh_bdtel" value="<%=UpdateModel.stwh_bdtel%>" class="st-input-text-300 form-control"
                    required="required" />
                <span class="text-danger"></span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                通讯地址：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_bdaddress" name="stwh_bdaddress" value="<%=UpdateModel.stwh_bdaddress%>" class="st-input-text-700 form-control"
                    required="required" />
                <span class="text-danger"></span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                邮编：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_bdyoubian" name="stwh_bdyoubian" value="<%=UpdateModel.stwh_bdyoubian%>" class="st-input-text-300 form-control" onkeydown="return checkNumber(event);"
                    required="required" />
                <span class="text-danger"></span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                截止时间：</div>
            <div class="col-sm-11 form-group">
                <div class=" input-group st-input-text-300">
                    <input type="text" name="stwh_bdendtime" id="stwh_bdendtime" placeholder="请输入时间"  class="form-control" value="<%=UpdateModel.stwh_bdendtime.ToString("yyyy-MM-dd HH:mm:ss") %>">
                    <span id="calendar" class=" input-group-addon"  style=" cursor:pointer;"><span class="fa fa-calendar"></span></span>
                </div>
                <span class="text-danger"></span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                招标状态：</div>
            <div class="col-sm-11 form-group">
                <input type="hidden" id="stwh_bdstatus" name="stwh_bdstatus" value="<%=UpdateModel.stwh_bdstatus%>" />
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span id="selectShowText"><%
                                                  if (UpdateModel.stwh_bdstatus==1)
                                                  {
                                                      %>进行中<%
                                                  }
                                                  else if (UpdateModel.stwh_bdstatus == 2)
                                                  {
                                                      %>变更中<%
                                                  }
                                                  else if (UpdateModel.stwh_bdstatus == 3)
                                                  {
                                                      %>结束<%
                                                  }
                                                   %></span> <span class="caret"></span>
                </button>
                <ul id="selectShowList" class="dropdown-menu" role="menu" style="max-height:300px; overflow:auto; left:auto; top:auto;">
                    <li><a data-pid="1">进行中</a></li>
                    <li><a data-pid="2">变更中</a></li>
                    <li><a data-pid="3">结束</a></li>
                </ul>
                <span class="text-danger"></span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                招标内容：</div>
            <div class="col-sm-11 form-group">
                <div id="stwh_bdcontentPanel" style=" display:none;"><%=UpdateModel.stwh_bdcontent%></div>
                <textarea id="stwh_bdcontent" name="stwh_bdcontent"  style="width:700px; height:300px;"></textarea>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-edit"></i> 修改</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <cms:stwhuescript ID="stwhuescript" runat="server" />
    <script type="text/javascript">
        //初始化富文本编辑器
        var qjOptions = {
            // 服务器统一请求接口路径
            serverUrl: "/handler/ueditor/controller.ashx",
            wordCount: true, //是否开启字数统计
            maximumWords: 5000, //允许的最大字符数
            elementPathEnabled: true, //是否启用元素路径，默认是显示
            autoHeightEnabled: false, //是否自动长高,默认true
            zIndex: 900, //编辑器层级的基数,默认是900
            emotionLocalization: true, //是否开启表情本地化，默认关闭。若要开启请确保emotion文件夹下包含官网提供的images表情文件夹
            autoFloatEnabled: false, //当设置为ture时，工具栏会跟随屏幕滚动，并且始终位于编辑区域的最上方
            pasteplain: false, //是否默认为纯文本粘贴。false为不使用纯文本粘贴，true为使用纯文本粘贴
            enableAutoSave: true, //启用自动保存
            saveInterval: 3000, //自动保存间隔时间
            allowDivTransToP: false,
            toolbars: [
                        ['fullscreen', 'source', 'cleardoc', 'undo', 'redo', '|',
                        'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', '|',
                         'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', '|',
                        'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                        'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                        'directionalityltr', 'directionalityrtl', 'indent', '|',
                        'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                        'touppercase', 'tolowercase', 'simpleupload', 'insertimage', '|',
                        'link', 'unlink', '|',
                        'horizontal', 'date', 'time', 'spechars', '|',
                        'inserttable', 'deletetable', 'preview']
                    ]//工具栏
        };
        var qjUEObject = UE.getEditor('stwh_bdcontent', qjOptions);

        setTimeout(function () {
            //qjUEObject.execCommand('drafts'); //载入本地数据（若存在）
            qjUEObject.setContent($("#stwh_bdcontentPanel").html(), false);
            $("#stwh_bdnumber").focus();
            $(window).scrollTop(0);
        }, 500);
    </script>
    <script src="/Plugin/swfobject.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#selectShowList a").click(function () {
                $("#selectShowText").text($(this).text());
                $("#stwh_bdstatus").val($(this).attr("data-pid"));
            });
            $("#addMenu").click(function () {
                var stwh_bdtitle = $("#stwh_bdtitle").val();
                if (!stwh_bdtitle) {
                    $.bs.alert("请输入招标标题！", "info");
                    return false;
                }
                var stwh_atcontent = qjUEObject.getContent();
                if (!stwh_atcontent) {
                    $.bs.alert("请输入招标内容！", "info");
                    return false;
                }
                $.post("/Handler/stwh_admin/sys_bidding/update.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") $.bs.alert(data.msg, "success", "-1");
                    else $.bs.alert(data.msg, "danger");
                }, "json");
                return false;
            });

            $("#calendar").click(function () {
                $('#stwh_bdendtime').click();
            });
            $('#stwh_bdendtime').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: true,
                autoUpdateInput: false,
                startDate: '<%=UpdateModel.stwh_bdendtime.ToString("yyyy-MM-dd HH:mm:ss") %>',
                locale: {
                    format: 'YYYY-MM-DD HH:mm:ss'
                }
            }).on({
                "apply.daterangepicker": function (ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD') + ' ' + new Date().Format("hh:mm:ss"));
                }
            });
        });
    </script>
    <script type="text/javascript">
        var auth = "<% = Request.Cookies[FormsAuthentication.FormsCookieName]==null ? string.Empty : Request.Cookies[FormsAuthentication.FormsCookieName].Value %>";
        var ASPSESSID = "<%= Session.SessionID %>";
        var ttfile = "*." + ("<%=WebSite.Webfiletype %>").replace(/,/ig, ";*.");
        var flashvarsFile = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: ttfile,
            FileType: "image",
            MaxSize: "<%=WebSite.Webfilesize*1024 %>" //500KB 字节为单位
        };
        var flashvarsVoice = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: "*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG",
            FileType: "image",
            MaxSize: "<%=WebSite.Webthumbnail %>" //500KB 字节为单位
        };
        var params = {
            wmode: "transparent",
            play: true,
            loop: true,
            menu: true,
            devicefont: false,
            scale: "showall",
            quality: "high",
            bgcolor: "#ffffff",
            allowScriptAccess: "sameDomain",
            salign: ""
        };
        swfobject.embedSWF("/Plugin/upload.swf", "upwebico1", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsFile, { btnid: "upwebico1", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);
        swfobject.embedSWF("/Plugin/upload.swf", "upwebico2", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsVoice, { btnid: "upwebico2", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);

        //设置进度
        function setProgress(info, name, IsSelect, btnid) {
            if (btnid == "upwebico1") {
                $("#progressFile1").show().children().first().css("width", info + "%");
            }
            else if (btnid == "upwebico2") {
                $("#progressFile2").show().children().first().css("width", info + "%");
            }
        }

        //上传完成时调用的方法（该方法被flash所调用）
        //filename:文件安上传成功后返回的文件名，包含扩展名,altString提示信息,msg服务器返回的成功消息
        function EndUpload(filename, altString, msg, btnid) {
            if (msg == "NoFile") {
                $.bs.alert("请选择文件！", "info");
            }
            else if (msg == "NoType") {
                $.bs.alert("请选择指定类型文件！", "info");
            }
            else if (msg == "NoSize") {
                $.bs.alert("文件大小超出限制，请联系管理员！", "info");
            }
            else if (msg == "login") {
                $.bs.alert("请先登录！", "info");
            }
            else if (msg == "Exception") {
                $.bs.alert("请稍后操作！", "info");
            }
            else {
                if ((/image/gi).test(msg)) {
                    if (btnid == "upwebico1") {
                        $("#stwh_bdpath").val(msg);
                    }
                    else if (btnid == "upwebico2") {
                        $("#stwh_bdimg").val(msg);
                    }
                }
                else {
                    $.bs.alert(msg, "info");
                }
            }
        }

        //上传失败时调用的方法（该方法被flash所调用）
        function ErrorUpload(msg) {
            $.bs.alert(msg, "info");
        }
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>