﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_shoporder
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_shoporder", "订单管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                stwh_userinfo model = Session["htuser"] as stwh_userinfo;
                string ifstr = "1= 1";
                int ss = 0, cc = 0;
                List<stwh_order> ListData = new stwh_orderBLL().GetListByPage<stwh_order>("stwh_orid", "desc", ifstr, 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_order item in ListData)
                {
                    string zffs = "微信", orderstatus = "<span style=\"color:red;\">待支付</span>";
                    if (item.stwh_orpaystyle == 0) zffs = "无";
                    else if (item.stwh_orpaystyle == 1) zffs = "微信";
                    else if (item.stwh_orpaystyle == 2) zffs = "支付宝";
                    else if (item.stwh_orpaystyle == 3) zffs = "银行卡";
                    else zffs = "到付";
                    if (item.stwh_orstatus == 1) orderstatus = "<span style=\"color:green;\">已支付</span>";
                    else if (item.stwh_orstatus == 2) orderstatus = "已取消";
                    else orderstatus = "<span style=\"color:red;\">待支付</span>";
                    strDatas += "<tr data-id=\"" + item.stwh_orid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_orid + "\" value=\"" + item.stwh_orid + "\" /></td><td>" + item.stwh_orid + "</td><td>" + item.stwh_bumobile + "</td><td>" + item.stwh_orddid + "</td><td>" + orderstatus + "</td><td>" + zffs + "</td><td>" + item.stwh_ortime.ToString("yyyy-MM-dd HH:mm:ss") + "</td><td>" + item.stwh_oruser + " " + item.stwh_ortel + " " + item.stwh_oraddress + "</td><td>" + item.stwh_orremark + "</td><td><div class=\"btn btn-default btnshop\" data-id=\"" + item.stwh_orddid + "\">查看商品</div></td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"10\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);
            }
        }
    }
}