﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_commoditytype.update" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">文章栏目管理</span></li>
        <li class="active">修改数据</li>
    </ol>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx"
    method="post" role="form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1 st-text-right">
                归属栏目：</div>
            <div class="col-sm-11 form-group">
                <input type="hidden" id="stwh_ptid" name="stwh_ptid" value="<%=UpdateModel.stwh_ptid %>" />
                <input type="hidden" id="stwh_ptparentid" name="stwh_ptparentid" value="<%=UpdateModel.stwh_ptparentid %>" />
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span id="selectShowText" runat="server">一级栏目</span> <span class="caret"></span>
                </button>
                <ul id="selectShowList" class="dropdown-menu" role="menu" runat="server" style="max-height:300px; overflow:auto; left:auto; top:auto;">
                    <li><a data-pid="0">父级导航</a></li>
                    <li><a data-pid="0">系统设置</a></li>
                    <li><a data-pid="0">系统管理</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="0">内容管理</a></li>
                </ul>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                状态：</div>
            <div class="col-sm-11 form-group">
                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-primary btn-sm <%=UpdateModel.stwh_ptshowmenu==0?"active":"" %>">
                        <input type="radio" name="stwh_ptshowmenu" id="stwh_ptshowmenu0" value="0" <%=UpdateModel.stwh_ptshowmenu==0?"checked=\"checked\"":"" %>/>
                        显示
                    </label>
                    <label class="btn btn-primary btn-sm <%=UpdateModel.stwh_ptshowmenu==1?"active":"" %>">
                        <input type="radio" name="stwh_ptshowmenu" id="stwh_ptshowmenu1" value="1" <%=UpdateModel.stwh_ptshowmenu==1?"checked=\"checked\"":"" %> />
                        隐藏
                    </label>
                </div>
                <span class="text-danger">*是否在菜单中显示该类下的商品</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                栏目图片：</div>
            <div class="col-sm-11 form-group">
                <input id="stwh_ptimg" name="stwh_ptimg" type="text" value="<%=UpdateModel.stwh_ptimg %>" class="st-input-text-700 form-control" placeholder="请输入上传栏目图片" style="float: left;" />
                <div style="float: left;width: 100px;">
                    <div id="upwebico">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div id="progressFile" class="progress progress-striped active" style=" display:none;">
                    <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                    </div>
                </div>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                序号：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_ptorder" name="stwh_ptorder" onkeydown="return checkNumber(event);" value="<%=UpdateModel.stwh_ptorder %>" class="st-input-text-300 form-control" />
                <span class="text-danger">*越大越考前</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                栏目名称：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_ptname" name="stwh_ptname" value="<%=UpdateModel.stwh_ptname %>"  class="st-input-text-700 form-control" placeholder="请输入栏目名称" />
                
                <span class="text-danger">*栏目名称内容长度不能超过100个字符</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                简要描述：</div>
            <div class="col-sm-11 form-group">
                <textarea id="stwh_ptdescription" name="stwh_ptdescription" class="st-input-text-700 form-control" placeholder="请输入栏目描述" ><%=UpdateModel.stwh_ptdescription%></textarea>
                <span class="text-danger">非必填，内容长度不能超过100字符</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                详细描述：</div>
            <div class="col-sm-11 form-group">
                <textarea id="stwh_ptdetails" name="stwh_ptdetails"  style="width:700px; height:300px;"></textarea>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-edit"></i> 修改</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <cms:stwhuescript ID="stwhuescript" runat="server" />
    <script type="text/javascript">
        //初始化富文本编辑器
        var qjOptions = {
            // 服务器统一请求接口路径
            serverUrl: "/handler/ueditor/controller.ashx",
            wordCount: true, //是否开启字数统计
            maximumWords: 5000, //允许的最大字符数
            elementPathEnabled: true, //是否启用元素路径，默认是显示
            autoHeightEnabled: false, //是否自动长高,默认true
            zIndex: 900, //编辑器层级的基数,默认是900
            emotionLocalization: true, //是否开启表情本地化，默认关闭。若要开启请确保emotion文件夹下包含官网提供的images表情文件夹
            autoFloatEnabled: false, //当设置为ture时，工具栏会跟随屏幕滚动，并且始终位于编辑区域的最上方
            pasteplain: false, //是否默认为纯文本粘贴。false为不使用纯文本粘贴，true为使用纯文本粘贴
            enableAutoSave: true, //启用自动保存
            saveInterval: 3000, //自动保存间隔时间
            allowDivTransToP: false,
            toolbars: [
                        ['fullscreen', 'source', 'cleardoc', 'undo', 'redo', '|',
                        'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', '|',
                         'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', '|',
                        'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                        'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                        'directionalityltr', 'directionalityrtl', 'indent', '|',
                        'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                        'touppercase', 'tolowercase', 'simpleupload', 'insertimage', '|',
                        'link', 'unlink', '|',
                        'horizontal', 'date', 'time', 'spechars', '|',
                        'inserttable', 'deletetable', 'preview']
                    ]//工具栏
        };
        var qjUEObject = UE.getEditor('stwh_ptdetails', qjOptions);

        setTimeout(function () {
            //qjUEObject.execCommand('drafts'); //载入本地数据（若存在）
            qjUEObject.setContent('<%=UpdateModel.stwh_ptdetails %>', false);
            $("#stwh_ptname").focus();
            $(window).scrollTop(0);
        }, 500);
    </script>
    <script src="/Plugin/swfobject.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#selectShowList a").click(function () {
                $("#selectShowText").text($(this).text().Trim().split('.')[1]);
                $("#stwh_ptparentid").val($(this).attr("data-pid"));
            });
            $("#addMenu").click(function () {
                var stwh_ptimg = $("#stwh_ptimg").val();
                if (stwh_ptimg.length > 200) {
                    $.bs.alert("图片路径长度超出服务器限制！", "info");
                    return false;
                }
                var stwh_ptname = $("#stwh_ptname").val();
                if (!stwh_ptname) {
                    $.bs.alert("请输入栏目名称！", "info");
                    return false;
                }
                else if (!IsHanZF(1, 100, stwh_ptname)) {
                    $.bs.alert("栏目名称长度超出服务器限制！", "info");
                    return false;
                }
                var stwh_ptdescription = $("#stwh_ptdescription").val();
                if (stwh_ptdescription) {
                    if (!IsHanZF(1, 200, stwh_ptdescription)) {
                        $.bs.alert("简要描述长度超出服务器限制！", "info");
                        return false;
                    }
                }
                var stwh_ptdetails = qjUEObject.getContent();
                if (!stwh_ptdetails) {
                    $.bs.alert("请输入商品分类详细描述！", "info");
                    return false;
                }
                else if (stwh_ptdetails.length > 5000) {
                    $.bs.alert("内容超出限制5000字！", "info");
                    return false;
                }
                $.post("/Handler/stwh_admin/sys_commoditytype/update.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "-1");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        var auth = "<% = Request.Cookies[FormsAuthentication.FormsCookieName]==null ? string.Empty : Request.Cookies[FormsAuthentication.FormsCookieName].Value %>";
        var ASPSESSID = "<%= Session.SessionID %>";
        var flashvarsVoice = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: "*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG",
            FileType: "image",
            MaxSize: "<%=WebSite.Webthumbnail %>" //500KB 字节为单位
        };
        var params = {
            wmode: "transparent",
            play: true,
            loop: true,
            menu: true,
            devicefont: false,
            scale: "showall",
            quality: "high",
            bgcolor: "#ffffff",
            allowScriptAccess: "sameDomain",
            salign: ""
        };
        swfobject.embedSWF("/Plugin/upload.swf", "upwebico", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsVoice, { btnid: "upwebico", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);

        //设置进度
        function setProgress(info, name, IsSelect, btnid) {
            $("#progressFile").show().children().first().css("width", info + "%");
        }

        //上传完成时调用的方法（该方法被flash所调用）
        //filename:文件安上传成功后返回的文件名，包含扩展名,altString提示信息,msg服务器返回的成功消息
        function EndUpload(filename, altString, msg, btnid) {
            if (msg == "NoFile") {
                $.bs.alert("请选择文件！", "info");
            }
            else if (msg == "NoType") {
                $.bs.alert("请选择指定类型文件！", "info");
            }
            else if (msg == "NoSize") {
                $.bs.alert("文件大小超出限制，请联系管理员！", "info");
            }
            else if (msg == "login") {
                $.bs.alert("请先登录！", "info");
            }
            else if (msg == "Exception") {
                $.bs.alert("请稍后操作！", "info");
            }
            else {
                if ((/image/gi).test(msg)) {
                    if (btnid == "upwebico") {
                        $("#stwh_ptimg").val(msg);
                    }
                }
                else {
                    $.bs.alert(msg, "info");
                }
            }
        }

        //上传失败时调用的方法（该方法被flash所调用）
        function ErrorUpload(msg) {
            $.bs.alert(msg, "info");
        }
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>