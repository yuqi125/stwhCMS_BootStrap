﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_QuestionType
{
    public partial class add : Common.PageBase
    {
        /// <summary>
        /// 获取文章栏目（并判断一级栏目下是否有子栏目）
        /// </summary>
        /// <param name="datasource">数据源</param>
        /// <param name="listSave">储存</param>
        /// <param name="pid">父id</param>
        /// <param name="dj">栏目等级</param>
        private void ArticleList(List<stwh_qutype> datasource, StringBuilder listSave, int pid, int dj)
        {
            if (listSave.Length == 0)
            {
                listSave.Append("<li><a data-pid=\"0\">(1).一级栏目</a></li>");
            }
            //筛选出一级栏目
            List<stwh_qutype> listadd = new List<stwh_qutype>();
            foreach (stwh_qutype item in datasource)
            {
                if (item.stwh_qutparentid == pid) listadd.Add(item);
            }
            if (listadd.Count != 0)
            {
                //循环递归判断当前栏目下是否有子栏目
                foreach (stwh_qutype item in listadd)
                {
                    listSave.Append("<li><a data-pid=\"" + item.stwh_qutid + "\">");
                    for (int i = 0; i < dj; i++) listSave.Append("&nbsp;&nbsp;");
                    listSave.Append("(" + (dj + 1) + ")." + item.stwh_qutname + "</a></li>");
                    ArticleList(datasource, listSave, item.stwh_qutid, dj + 1);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SetMenuSpanText(this.menuSpan, "sys_QuestionType", "试题分类管理");
                List<stwh_qutype> AllListData = new stwh_qutypeBLL().GetModelList("");
                StringBuilder sb = new StringBuilder();
                ArticleList(AllListData, sb, 0, 0);
                this.selectShowList.InnerHtml = sb.ToString();
            }
            catch (Exception)
            {

            }
        }
    }
}