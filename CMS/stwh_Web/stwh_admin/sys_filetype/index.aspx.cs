﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_filetype
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_filetype", "文件分类管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);

                int cc = 0;
                List<stwh_filetype> ListData = new stwh_filetypeBLL().GetModelList("");
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";

                StringBuilder sb = new StringBuilder();
                foreach (stwh_filetype item in ListData)
                {
                    sb.Append("<tr data-id=\"" + item.stwh_ftid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_ftid + "\" value=\"" + item.stwh_ftid + "\" /></td><td>" + item.stwh_ftid + "</td><td>" + item.stwh_ftname + "</td><td>" + item.stwh_ftdescription + "</td><td><input type=\"text\" min=\"0\" value=\"" + item.stwh_ftorder + "\" onkeydown=\"return checkNumber(event);\" style=\"width: 50px;\"/></td></tr>");
                }
                if (sb == null || sb.Length == 0) this.ChildDatas.InnerHtml = "<tr><td colspan=\"5\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = sb.ToString();
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);
            }
        }
    }
}