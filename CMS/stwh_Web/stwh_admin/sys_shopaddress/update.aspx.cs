﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_shopaddress
{
    public partial class update : Common.PageBase
    {
        public stwh_buyaddress UpdateModel = new stwh_buyaddress();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SetMenuSpanText(this.menuSpan, "sys_shopaddress", "会员地址管理");
                string mid = Request.QueryString["id"];
                if (PageValidate.IsNumber(mid)) UpdateModel = new stwh_buyaddressBLL().GetModel(int.Parse(mid));
            }
            catch (Exception)
            {

            }
        }
    }
}