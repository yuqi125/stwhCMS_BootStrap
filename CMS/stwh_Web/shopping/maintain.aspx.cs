﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//导入命名空间
using stwh_Web.stwh_admin.Common;

namespace stwh_Web.shopping
{
    public partial class maintain : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PageBaseVT pbvt = new PageBaseShoppingVT("maintain");
            pbvt.OutPutHtml();
        }

        protected override void OnError(EventArgs e)
        {
            Exception exception = Server.GetLastError();
            Response.Clear();
            HttpException httpException = exception as HttpException;
            int errorCode = (httpException == null ? 0 : httpException.GetHttpCode());
            Server.ClearError();
            switch (errorCode)
            {
                case 403:
                    Response.Redirect("/shop/403.html");
                    break;
                case 404:
                    Response.Redirect("/shop/404.html");
                    break;
                case 500:
                    stwh_Web.Handler.stwh_admin.BaseHandler.AddLog("系统异常：" + exception.Message + "——详细信息：" + exception.StackTrace);
                    HttpContext.Current.Session["error_shop_500"] = exception.Message;
                    Response.Redirect("/shop/500.html");
                    break;
                default:
                    stwh_Web.Handler.stwh_admin.BaseHandler.AddLog("系统异常：" + exception.Message + "——详细信息：" + exception.StackTrace);
                    HttpContext.Current.Session["error_shop_other"] = exception.Message;
                    Response.Redirect("/shop/other.html");
                    break;
            }
            base.OnError(e);
        }
    }
}