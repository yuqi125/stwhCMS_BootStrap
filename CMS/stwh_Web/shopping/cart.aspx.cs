﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using stwh_Web.stwh_admin.Common;
using stwh_Model;
using stwh_BLL;

namespace stwh_Web.shopping
{
    public partial class cart : WebPageShopBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //提前判断是否登录
            stwh_buyuser model = CheckLogin();
            if (model == null) Response.Redirect("/shop/ogin.html?reUrl=/shop/art.html");
            else
            {
                PageBaseVT pbvt = new PageBaseShoppingVT("cart");
                pbvt.OutPutHtml();
            }
        }
    }
}