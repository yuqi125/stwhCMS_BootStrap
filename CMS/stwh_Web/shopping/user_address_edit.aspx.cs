﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using stwh_Web.stwh_admin.Common;
using stwh_Model;
using stwh_BLL;
using stwh_Common;

namespace stwh_Web.shopping
{
    /// <summary>
    /// 会员地址编辑页面
    /// </summary>
    public partial class user_address_edit : WebPageShopBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //判断是否登录
            stwh_buyuser model = CheckLogin();
            string id = Request["id"];
            if (model == null)
            {
                Response.Redirect("/shop/login.html?reUrl=/shop/user_address_edit.html");
            }
            else if (!PageValidate.IsNumber(id))
            {
                Response.Redirect("/shop/user_address.html");
            }
            else
            {
                PageBaseVT pbvt = new PageBaseShoppingVT("user_address_edit");
                pbvt.OutPutHtml();
            }
        }
    }
}