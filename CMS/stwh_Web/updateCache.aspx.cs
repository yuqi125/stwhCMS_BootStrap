﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using stwh_Web.stwh_admin.Common;
using stwh_Common;

namespace stwh_Web
{
    public partial class updateCache : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                WebSite.LoadChkURL(true);
                WebSite.LoadWebSite(true);
                this.msg.InnerHtml = "强制更新缓存已完成！！！";
            }
            catch (Exception ex)
            {
                FileHelper.WriteError("强制更新缓存失败！！！详细信息：" + ex.Message + "\r\n" + ex.StackTrace);
                this.msg.InnerHtml = "强制更新缓存失败！！！";
            }
        }
    }
}