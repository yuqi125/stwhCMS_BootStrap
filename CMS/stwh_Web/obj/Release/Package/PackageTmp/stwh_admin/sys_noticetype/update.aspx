﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_noticetype.update" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">公告类型管理</span></li>
        <li class="active">修改数据</li>
    </ol>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx" method="post" role="form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1 st-text-right">
                类型名称：</div>
            <div class="col-sm-11 form-group">
                <input type="hidden" id="stwh_notid" name="stwh_notid" value="<%=UpdateModel.stwh_notid %>" />
                <input type="hidden" id="stwh_notaddtime" name="stwh_notaddtime" value="<%=UpdateModel.stwh_notaddtime %>" />
                <input type="text" id="stwh_nottitle" name="stwh_nottitle" value="<%=UpdateModel.stwh_nottitle %>" class="st-input-text-700 form-control" />
                <span class="text-danger">*字数不能超过100字</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                类型简介：</div>
            <div class="col-sm-11 form-group">
                <textarea id="stwh_notjianjie" name="stwh_notjianjie" class="st-input-text-700 form-control"><%=UpdateModel.stwh_notjianjie%></textarea>
                <span class="text-danger">*简介内容不能超过150字</span>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-edit"></i> 修改</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <cms:stwhuescript ID="stwhuescript" runat="server" />
    <script type="text/javascript">
        $(function () {
            $("#addMenu").click(function () {
                var stwh_nottitle = $("#stwh_nottitle").val();
                if (!IsHanZF(1, 100, stwh_nottitle)) {
                    $.bs.alert("类型名称格式错误，长度控制在100字以内！", "info");
                    return false;
                }
                var stwh_notjianjie = $("#stwh_notjianjie").val();
                if (!IsHanZF(1, 150, stwh_notjianjie)) {
                    $.bs.alert("类型简介格式错误，长度控制在150字以内！", "info");
                    return false;
                }
                $.post("/Handler/stwh_admin/sys_noticetype/update.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "-1");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>