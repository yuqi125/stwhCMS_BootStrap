﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_staff.index" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
    <style type="text/css">
        #searchDiv  .form-group
        {
            margin-bottom:10px;
        }
    </style>
</head>
<body>
    <div class="loading" id="loading">
        <i class="fa fa-spinner fa-spin fa-2x"></i>
    </div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li class="active"><span id="menuSpan" runat="server">员工管理</span></li>
    </ol>
    <div class="m-top-10 p-bottom-15 p-left-15 scrollTop">
        <form class="form-inline">
        <asp:Literal ID="litBtnList" runat="server"></asp:Literal><span id="excelpath" class="text-success"></span>
        <div id="searchDiv" class="collapse">
            <div class="form-group">
                <input type="hidden" id="stwh_dtid" name="stwh_dtid" value="0" />
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span id="selectShowText">请选择部门</span> <span class="caret"></span>
                </button>
                <ul id="selectShowList" class="dropdown-menu" role="menu" runat="server" style="max-height: 300px;
                    overflow: auto; left: auto; top: auto;">
                </ul>
            </div>
            <div class="form-group">
                <input type="hidden" id="stwh_siszz" name="stwh_siszz" value="-1" />
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span id="stwh_siszzText">请选择转正状态</span> <span class="caret"></span>
                </button>
                <ul id="stwh_siszzList" class="dropdown-menu" role="menu" runat="server" style="max-height: 300px;
                    overflow: auto; left: auto; top: auto;">
                    <li><a data-pid="-1">请选择转正状态</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="0">未转正</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="1">已转正</a></li>
                </ul>
            </div>
            <div class="form-group">
                <input type="hidden" id="stwh_sisht" name="stwh_sisht" value="-1" />
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span id="stwh_sishtText">请选择合同状态</span> <span class="caret"></span>
                </button>
                <ul id="stwh_sishtList" class="dropdown-menu" role="menu" runat="server" style="max-height: 300px;
                    overflow: auto; left: auto; top: auto;">
                    <li><a data-pid="-1">请选择合同状态</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="0">未签订</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="1">已签订</a></li>
                </ul>
            </div>
            <div class="form-group">
                <input type="hidden" id="stwh_sislz" name="stwh_sislz" value="-1" />
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span id="stwh_sislzText">请选择离职状态</span> <span class="caret"></span>
                </button>
                <ul id="stwh_sislzList" class="dropdown-menu" role="menu" runat="server" style="max-height: 300px;
                    overflow: auto; left: auto; top: auto;">
                    <li><a data-pid="-1">请选择离职状态</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="0">未离职</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="1">已离职</a></li>
                </ul>
            </div>
            <div class="form-group">
                <input type="hidden" id="month" name="month" value="0" />
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span id="monthText">请选择生日月份</span> <span class="caret"></span>
                </button>
                <ul id="monthList" class="dropdown-menu" role="menu" runat="server" style="max-height: 300px;
                    overflow: auto; left: auto; top: auto;">
                    <li><a data-pid="0">请选择生日月份</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="1">一月份</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="2">二月份</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="3">三月份</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="4">四月份</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="5">五月份</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="6">六月份</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="7">七月份</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="8">八月份</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="9">九月份</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="10">十月份</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="11">十一月份</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="12">十二月份</a></li>
                </ul>
            </div>
            <div class="form-group">
                <div class=" input-group">
                    <input type="text" id="rztime" class="form-control" value="" placeholder="请选择入职时间段">
                    <span id="calendar" class=" input-group-addon"  style=" cursor:pointer;"><span class="glyphicon glyphicon-calendar fa fa-calendar"></span></span>
                </div>
            </div>
            <div class="form-group">
                <div class=" input-group">
                    <input type="text" id="zztime" class="form-control" value="" placeholder="请选择转正时间段">
                    <span id="calendar2" class=" input-group-addon"  style=" cursor:pointer;"><span class="glyphicon glyphicon-calendar fa fa-calendar"></span></span>
                </div>
            </div>
            <div class="form-group">
                <div class=" input-group">
                    <input type="text" id="qdtime" class="form-control" value="" placeholder="请选择签订合同时间段">
                    <span id="calendar3" class=" input-group-addon"  style=" cursor:pointer;"><span class="glyphicon glyphicon-calendar fa fa-calendar"></span></span>
                </div>
            </div>
            <div class="form-group">
                <div class=" input-group">
                    <input type="text" id="lztime" class="form-control" value="" placeholder="请选择离职时间段">
                    <span id="calendar4" class=" input-group-addon"  style=" cursor:pointer;"><span class="glyphicon glyphicon-calendar fa fa-calendar"></span></span>
                </div>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="stwh_sname" id="stwh_sname"
                    placeholder="请输入员工姓名" />
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="stwh_stel" id="stwh_stel"
                    placeholder="请输入联系电话" />
            </div>
            <div class="form-group">
                <div class=" input-group">
                    <input type="text" name="stwh_saddtime" id="stwh_saddtime" placeholder="请输入时间"  class="form-control" value="">
                    <span id="calendar1" class=" input-group-addon"  style=" cursor:pointer;"><span class="fa fa-calendar"></span></span>
                </div>
            </div>
            <div class="form-group">
                &nbsp;&nbsp;<a href="#" class="btn btn-default" id="btnsearchOk"><span class="glyphicon glyphicon-search"></span></a>
            </div>
        </div>
        </form>
    </div>
    <div id="scrollPanel">
        <form id="form1" runat="server" class="form-inline">
        <div class="container-fluid">
            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                    <tr>
                        <th style="width: 50px;">
                            <input type="checkbox" id="allParent" />
                        </th>
                        <th style="width: 50px;">
                            编号
                        </th>
                        <th style="width: 60px;">
                            指纹号
                        </th>
                        <th style="width: 100px;">
                            姓名
                        </th>
                        <th style="width: 60px;">
                            性别
                        </th>
                        <th style="width: 200px;">
                            所属部门
                        </th>
                        <th style="width: 200px;">
                            担任职务
                        </th>
                        <th style="width: 200px;">
                            联系电话
                        </th>
                        <th>
                            状态
                        </th>
                        <th style="width: 200px;">
                            操作
                        </th>
                    </tr>
                </thead>
                <tbody id="ChildDatas" runat="server">
                </tbody>
            </table>
        </div>
        <asp:HiddenField ID="hidListId" runat="server" />
        <asp:HiddenField ID="hidAllData" runat="server" />
        <asp:HiddenField ID="hidTotalSum" runat="server" />
        </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
        <div class="form-group">
            <label>
                每页显示：</label>
            <select id="basic" class="selectpicker show-tick" data-width="55px" data-style="btn-sm btn-default">
                <option>15</option>
                <option>25</option>
                <option>35</option>
                <option>45</option>
                <option>55</option>
            </select>
        </div>
        <div class="form-group">
            <ul id="pageUl" style="margin: 0px auto;">
            </ul>
        </div>
        <div class="form-group">
            共 <span id="SumCountSpan">
                <asp:Literal ID="littotalSum" runat="server"></asp:Literal></span> 条数据
        </div>
        </form>
    </div>
    <div class="modal fade" id="deleteMessageModal" tabindex="-1" role="dialog" aria-labelledby="h2"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="h2">
                        <i class="fa fa-exclamation-circle"></i>系统提示
                    </h4>
                </div>
                <div class="modal-body">
                    <h4>
                        <span class="glyphicon glyphicon-info-sign"></span>你确定要删除吗（删除后不可恢复）？</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        取消
                    </button>
                    <button type="button" class="btn btn-default" id="btnDeleteOk">
                        确定
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <div class="modal fade" id="staffPanel" tabindex="-1" role="dialog" aria-labelledby="stafftitle"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="stafftitle">
                        <i class="fa fa-exclamation-circle"></i>员工详细信息
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <h3>
                            基本信息：</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    归属部门：<span id="dtname" class="text-success"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    员工姓名：<span id="sname" class="text-success"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    指纹号：<span id="snumber" class="text-success"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    员工性别：<span id="ssex" class="text-success"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    担任职务：<span id="szw" class="text-success"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    联系电话：<span id="stel" class="text-success"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    身份证号：<span id="ssfz" class="text-success"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    员工生日：<span id="sbirthday" class="text-success"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    员工学历：<span id="sxueli" class="text-success"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    试用薪资：<span id="syqmoney" class="text-success"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    转正薪资：<span id="szzmoney" class="text-success"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    入职时间：<span id="srztime" class="text-success"></span>
                                </div>
                            </div>
                        </div>
                        <h3>
                            其他信息：</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    转正状态：<span id="siszz" class="text-success"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    转正时间：<span id="szztime" class="text-success"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    合同状态：<span id="sisht" class="text-success"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    签订时间：<span id="shttime" class="text-success"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    离职状态：<span id="sislz" class="text-success"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    离职时间：<span id="slztime" class="text-success"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    备注信息：<span id="sremark" class="text-success"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    录入时间：<span id="saddtime" class="text-success"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        确定
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        var AllData = JSON.parse($("#hidAllData").val());
        var listid = [];
        var pageCount = 15;//每页显示多少条数据
        var ifelse = "";
        function CreateData(DataJson)
        {
            $.each(DataJson, function (index, item) {
                $("#ChildDatas").append("<tr data-id=\"" + item.stwh_sid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_sid + "\" value=\"" + item.stwh_sid + "\" /></td><td>" + item.stwh_sid + "</td><td>" + item.stwh_snumber + "</td><td>" + item.stwh_sname + "</td><td>" + (item.stwh_ssex == 0 ? "男" : "女") + "</td><td>" + item.stwh_dtname + "</td><td>" + item.stwh_szw + "</td><td>" + item.stwh_stel + "</td><td>" + (item.stwh_siszz == 0 ? "<span class=\"text-danger\">[未转正]</span>  " : "<span class=\"text-success\">[已转正]</span> ") + (item.stwh_sisht == 0 ? "<span class=\"text-danger\">[未签订]</span> " : "<span class=\"text-success\">[已签订]</span> ") + (item.stwh_sislz == 0 ? "<span class=\"text-success\">[在职]</span> " : "<span class=\"text-danger\">[离职]</span>") + "</td><td><button class=\"btn btn-default btnxg\" data-id=\"" + item.stwh_sid + "\"><i class=\"fa fa-edit\"></i> 修改</button> | <button class=\"btn btn-default btnLook\" data-id=\"" + item.stwh_sid + "\"><i class=\"fa fa-eye\"></i> 查看</button></td></tr>");
            });
            UpdateScroll();
        }
        //加载数据
        function LoadData(pCount,pIndex,ifstr)
        {
            $.post("/handler/stwh_admin/sys_staff/loaddata.ashx",{
                pageCount:pCount,
                pageIndex:pIndex,
                whereStr:ifstr
            },function(data){
                if (data.msgcode==-1) {
                    $.bs.alert(data.msg, "warning", "");
                }
                else
                {
                    $("#hidListId").val("");
                    AllData = data.msg;
                    $("#ChildDatas tr").remove();
                    $("#SumCountSpan").text(data.sumcount);
                    if (data.msg.length==0) {
                        $("#ChildDatas").append("<tr><td colspan=\"10\" align=\"center\">暂无数据</td></tr>");
                    }
                    else
                    {
                        CreateData(AllData);
                    }
                }
            },"json");
        }
        //重新设置分页
        function SetPaginator(pageCount,totalsum)
        {
            if (totalsum==0) totalsum = 1;
            $("#pageUl").bootstrapPaginator("setOptions",{
                    currentPage: 1, //当前页面
                    totalPages: totalsum / pageCount + ((totalsum % pageCount) == 0 ? 0 : 1), //总页数
                    numberOfPages: 5, //页码数
                    useBootstrapTooltip: true,
                    onPageChanged: function (event, oldPage, newPage) {
                        if (oldPage != newPage) LoadData(pageCount,parseInt(newPage) - 1,ifelse);
                    }
                });
        }
        $(function () {
            $("#selectShowList a").click(function () {
                $("#selectShowText").text($(this).text());
                $("#stwh_dtid").val($(this).attr("data-pid"));
            });
            $("#stwh_siszzList a").click(function () {
                $("#stwh_siszzText").text($(this).text());
                $("#stwh_siszz").val($(this).attr("data-pid"));
            });
            $("#stwh_sishtList a").click(function () {
                $("#stwh_sishtText").text($(this).text());
                $("#stwh_sisht").val($(this).attr("data-pid"));
            });
            $("#stwh_sislzList a").click(function () {
                $("#stwh_sislzText").text($(this).text());
                $("#stwh_sislz").val($(this).attr("data-pid"));
            });
            $("#monthList a").click(function () {
                $("#monthText").text($(this).text());
                $("#month").val($(this).attr("data-pid"));
            });
            $("#pageUl").bootstrapPaginator({
                currentPage: 1, //当前页面
                totalPages: <%=totalPages %>, //总页数
                numberOfPages: 5, //页码数
                useBootstrapTooltip: true,
                onPageChanged: function (event, oldPage, newPage) {
                    if (oldPage != newPage) LoadData(pageCount,parseInt(newPage) - 1,ifelse);
                }
            });
            $("#allParent").click(function () {
                if ($(this).is(":checked")) {
                    $("#ChildDatas :checkbox").prop("checked", $(this).is(":checked"));
                    $("#ChildDatas :checkbox:checked").each(function (i, item) {
                        if (listid.IndexOfArray($(item).val()) == -1) listid.AddArray($(item).val());
                    });
                    $("#hidListId").val(listid.toStringArray());
                }
                else {
                    $("#ChildDatas :checkbox").prop("checked", $(this).is(":checked"));
                    $("#ChildDatas :checkbox:not(:checked)").each(function (i, item) {
                        listid.removeIndexArray(listid.IndexOfArray($(item).val()));
                    });
                    $("#hidListId").val(listid.toStringArray());
                }
            });
            $("#ChildDatas").on("click",":checkbox",function(){
              if ($(this).is(":checked")) {
                    if ($("#ChildDatas :checkbox:checked").length == $("#ChildDatas :checkbox").length) $("#allParent").prop("checked", true);
                    if (listid.IndexOfArray($(this).val()) == -1) listid.AddArray($(this).val());
                    $("#hidListId").val(listid.toStringArray());
                }
                else {
                    if ($("#ChildDatas :checkbox:not(:checked)").length == $("#ChildDatas :checkbox").length) $("#allParent").prop("checked", false);
                    listid.removeIndexArray(listid.IndexOfArray($(this).val()));
                    $("#hidListId").val(listid.toStringArray());
                }
            });
            $("#btnDelete").click(function () {
                $("#deleteMessageModal").modal('show');
            });
            $("#btnDeleteOk").click(function () {
                var ids = $("#hidListId").val();
                if (!ids) {
                    $.bs.alert("请选择数据！", "info");
                    return false;
                }
                else if (!(/^[\d,]*$/).test(ids)) {
                    $.bs.alert("非法数据格式，请刷新页面重试！", "danger");
                    return false;
                }
                $.post(
                "/handler/stwh_admin/sys_staff/delete.ashx",
                {
                    ids: ids
                }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
            });
            $("#btnlizhi").click(function(){
                var ids = $("#hidListId").val();
                if (!ids) {
                    $.bs.alert("请选择数据！", "info");
                    return false;
                }
                else if (!(/^[\d,]*$/).test(ids)) {
                    $.bs.alert("非法数据格式，请刷新页面重试！", "danger");
                    return false;
                }
                $.post(
                "/handler/stwh_admin/sys_staff/examine.ashx",
                {
                    flag:2,
                    state:1,
                    ids: ids
                }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
            $("#btnUpdate").click(function () {
                if ($("#ChildDatas :checkbox:checked").length == 0) {
                    $.bs.alert("请选择数据！", "warning", "");
                    return false;
                }
                $("#ChildDatas :checkbox:checked").each(function (i, item) {
                    if (i == 0) window.location.href = "update.aspx?id=" + $(item).val();
                });
                return false;
            });
            $("#btnSearch").click(function(){
                $('#searchDiv').collapse("toggle");
                return false;
            });
            $("#btnsearchOk").click(function(){
                var stwh_dtid = $("#stwh_dtid").val();
                var stwh_siszz = $("#stwh_siszz").val();
                var stwh_sisht = $("#stwh_sisht").val();
                var stwh_sislz = $("#stwh_sislz").val();
                var month = $("#month").val();
                var rztime = $("#rztime").val();
                var zztime = $("#zztime").val();
                var qdtime = $("#qdtime").val();
                var lztime = $("#lztime").val();
                var stwh_sname = $("#stwh_sname").val();
                if (!IsHanZF(1,50,stwh_sname) && stwh_sname != "") {
                    $.bs.alert("员工姓名格式错误！", "info");
                    return;
                }
                var stwh_saddtime = $("#stwh_saddtime").val();
                var stwh_stel = $("#stwh_stel").val();
                if (!IsMobileTel(stwh_stel) && stwh_stel != "") {
                    $.bs.alert("联系电话格式错误！", "info");
                    return;
                }
                if (stwh_sname=="" && stwh_saddtime == "" && stwh_dtid == "0" && month == "0" && rztime == ""&& zztime == "" && qdtime == "" && lztime == "" && stwh_siszz == "-1"&& stwh_sislz == "-1"&& stwh_sisht == "-1" && stwh_stel == "") {
                    $.bs.alert("至少输入一个查询值！", "info");
                    return;
                }
                ifelse = '[{"name":"stwh_sname","value":"'+stwh_sname+'"},{"name":"stwh_saddtime","value":"'+stwh_saddtime+'"},{"name":"stwh_dtid","value":"'+stwh_dtid+'"},{"name":"stwh_stel","value":"'+stwh_stel+'"},{"name":"month","value":"'+month+'"},{"name":"rztime","value":"'+rztime+'"},{"name":"zztime","value":"'+zztime+'"},{"name":"qdtime","value":"'+qdtime+'"},{"name":"lztime","value":"'+lztime+'"},{"name":"stwh_siszz","value":"'+stwh_siszz+'"},{"name":"stwh_sisht","value":"'+stwh_sisht+'"},{"name":"stwh_sislz","value":"'+stwh_sislz+'"}]';
                $.bs.loading(true);
                $.post("/handler/stwh_admin/sys_staff/loaddata.ashx",{
                    pageCount:pageCount,
                    pageIndex:0,
                    whereStr:ifelse
                },function(data){
                    $.bs.loading(false);
                    $('#searchDiv').collapse("toggle");
                    if (data.msgcode==-1) {
                        $.bs.alert(data.msg, "warning", "");
                    }
                    else
                    {
                        $("#hidListId").val("");
                        AllData = data.msg;
                        $("#ChildDatas tr").remove();
                        $("#SumCountSpan").text(data.sumcount);
                        $("#hidTotalSum").val(data.sumcount);
                        SetPaginator(pageCount,parseInt(data.sumcount));
                        if (data.msg.length==0) $("#ChildDatas").append("<tr><td colspan=\"11\" align=\"center\">暂无数据</td></tr>");
                        else CreateData(AllData);
                    }
                },"json");
                return false;
            });
            $("#btnzzuser,#btnlzuser").click(function(){
                if ($(this).attr("id")=="btnlzuser") {
                    ifelse = '[{"name":"stwh_sislz","value":"1"}]';
                }
                else {
                    ifelse = '[{"name":"stwh_sislz","value":"0"}]';
                }
                $.post("/handler/stwh_admin/sys_staff/loaddata.ashx",{
                    pageCount:pageCount,
                    pageIndex:0,
                    whereStr:ifelse
                },function(data){
                    if (data.msgcode==-1) {
                        $.bs.alert(data.msg, "warning", "");
                    }
                    else
                    {
                        $("#hidListId").val("");
                        AllData = data.msg;
                        $("#ChildDatas tr").remove();
                        $("#SumCountSpan").text(data.sumcount);
                        $("#hidTotalSum").val(data.sumcount);
                        SetPaginator(pageCount,parseInt(data.sumcount));
                        if (data.msg.length==0) 
                            $("#ChildDatas").append("<tr><td colspan=\"11\" align=\"center\">暂无数据</td></tr>");
                        else
                            CreateData(AllData);
                    }
                },"json");
                return false;
            });
            $("#btnyqcontract,#btnlwqcontract").click(function(){
                if ($(this).attr("id")=="btnyqcontract") {
                    ifelse = '[{"name":"stwh_sisht","value":"1"}]';
                }
                else {
                    ifelse = '[{"name":"stwh_sisht","value":"0"}]';
                }
                $.post("/handler/stwh_admin/sys_staff/loaddata.ashx",{
                    pageCount:pageCount,
                    pageIndex:0,
                    whereStr:ifelse
                },function(data){
                    if (data.msgcode==-1) {
                        $.bs.alert(data.msg, "warning", "");
                    }
                    else
                    {
                        $("#hidListId").val("");
                        AllData = data.msg;
                        $("#ChildDatas tr").remove();
                        $("#SumCountSpan").text(data.sumcount);
                        $("#hidTotalSum").val(data.sumcount);
                        SetPaginator(pageCount,parseInt(data.sumcount));
                        if (data.msg.length==0) 
                            $("#ChildDatas").append("<tr><td colspan=\"11\" align=\"center\">暂无数据</td></tr>");
                        else
                            CreateData(AllData);
                    }
                },"json");
                return false;
            });
            $("#btnExcel").click(function(){
                $.post("/handler/stwh_admin/sys_staff/download.ashx",{
                    whereStr:ifelse
                },function(data){
                    if (data.msgcode==-1) {
                        $("#excelpath").html("");
                        $.bs.alert(data.msg, "warning", "");
                    }
                    else
                    {
                        $("#excelpath").html("<a href=\""+data.msg+"\">数据已生成，请点击下载！！！</a>");
                    }
                },"json");
                return false;
            });
            $("#ChildDatas").on("click",".btnxg",function(){
                window.location.href = "update.aspx?id=" + $(this).attr("data-id");
                return false;
            });
            $("#ChildDatas").on("click",".btnLook",function(){
                $("#staffPanel").modal('show');
                var sid = $(this).attr("data-id");
                $.each(AllData,function(index,item){
                    if (item.stwh_sid == sid) {
                        $("#dtname").text(item.stwh_dtname);
                        $("#sname").text(item.stwh_sname);
                        $("#snumber").text(item.stwh_snumber);
                        $("#ssex").text(item.stwh_ssex == 0 ? "男性" : "女性");
                        $("#szw").text(item.stwh_szw);
                        $("#stel").text(item.stwh_stel);
                        $("#sxueli").text(item.stwh_sxueli);
                        $("#ssfz").text(item.stwh_ssfz);
                        $("#syqmoney").text(item.stwh_syqmoney + "元");
                        $("#szzmoney").text(item.stwh_szzmoney + "元");
                        $("#siszz").text(item.stwh_siszz == 0 ? "未转正" : "已转正");
                        $("#sisht").text(item.stwh_sisht == 0 ? "未签订" : "已签订");
                        $("#sislz").text(item.stwh_sislz == 0 ? "未离职" : "已离职");
                        $("#sremark").text(item.stwh_sremark);
                        if (item.stwh_sbirthday) $("#sbirthday").text(item.stwh_sbirthday.split('T')[0]);
                        if (item.stwh_srztime) $("#srztime").text(item.stwh_srztime.replace('T',' ').split('.')[0]);
                        if (item.stwh_szztime) $("#szztime").text(item.stwh_szztime.replace('T',' ').split('.')[0]);
                        if (item.stwh_shttime) $("#shttime").text(item.stwh_shttime.replace('T',' ').split('.')[0]);
                        if (item.stwh_slztime) $("#slztime").text(item.stwh_slztime.replace('T',' ').split('.')[0]);
                        if (item.stwh_saddtime) $("#saddtime").text(item.stwh_saddtime.replace('T',' ').split('.')[0]);
                        return false;
                    }
                });
                return false;
            });
            $("#calendar").click(function () {
                $('#rztime').click();
            });
            $('#rztime').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY-MM-DD',
                    separator: ',',
                    applyLabel: '确定',
                    cancelLabel: '清除'
                }
            }).on({
                "cancel.daterangepicker": function (e) {
                    $(this).val("");
                },
                "apply.daterangepicker": function (ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD'));
                }
            });
            $("#calendar1").click(function () {
                $('#stwh_saddtime').click();
            });
            $('#stwh_saddtime').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: true,
                autoUpdateInput: false
            }).on({
                "apply.daterangepicker": function (ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD'));
                }
            });
            $("#calendar2").click(function () {
                $('#zztime').click();
            });
            $('#zztime').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY-MM-DD',
                    separator: ',',
                    applyLabel: '确定',
                    cancelLabel: '清除'
                }
            }).on({
                "cancel.daterangepicker": function (e) {
                    $(this).val("");
                },
                "apply.daterangepicker": function (ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD'));
                }
            });
            $("#calendar3").click(function () {
                $('#qdtime').click();
            });
            $('#qdtime').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY-MM-DD',
                    separator: ',',
                    applyLabel: '确定',
                    cancelLabel: '清除'
                }
            }).on({
                "cancel.daterangepicker": function (e) {
                    $(this).val("");
                },
                "apply.daterangepicker": function (ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD'));
                }
            });
            $("#calendar4").click(function () {
                $('#lztime').click();
            });
            $('#lztime').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY-MM-DD',
                    separator: ',',
                    applyLabel: '确定',
                    cancelLabel: '清除'
                }
            }).on({
                "cancel.daterangepicker": function (e) {
                    $(this).val("");
                },
                "apply.daterangepicker": function (ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD'));
                }
            });
            $("#basic").change(function(){
                pageCount = $(this).val();
                LoadData(pageCount,0,ifelse);
                var totalsum = parseInt($("#hidTotalSum").val());
                SetPaginator(pageCount,totalsum);
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
