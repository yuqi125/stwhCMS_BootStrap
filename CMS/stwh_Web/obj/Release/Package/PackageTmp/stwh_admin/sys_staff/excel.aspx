﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="excel.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_staff.excel" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading">
        <i class="fa fa-spinner fa-spin fa-2x"></i>
    </div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">员工管理</span></li>
        <li class="active">数据导入</li>
    </ol>
    <div id="scrollPanel">
        <form id="form1" runat="server" class="form-horizontal" role="form">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <h4>
                        Excel数据格式，请参照excel模板中的格式进行设置（选择其中一种即可，性别：0表示男，1表示女，转正、合同、离职：0表示否，1表示是）！</h4>
                    <h5 class="text-danger">注意保持一列单元格数据一致，否则无法导入！</h5>
                    <a href="template1.xls">【点击下载模板一】</a>&nbsp;&nbsp;&nbsp;<a href="template2.xls">【点击下载模板二】</a>
                </div>
                <div class="line10">
                </div>
            </div>
            <div class="row">
                <div class="container-fluid">
                    <table class="table table-striped table-bordered table-hover table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    文件名（点击下载）
                                </th>
                                <th style="width: 250px;">
                                    时间
                                </th>
                                <th style="width: 200px;">
                                    操作
                                </th>
                            </tr>
                        </thead>
                        <tbody id="ChildDatas" runat="server">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <input type="hidden" id="excelpath" name="excelpath" value="" />
        </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
        <div class="col-sm-12">
            <div class="form-group">
                <div class="pull-left">
                    <label class="control-label">
                        请上传Excel数据文件：</label>
                </div>
                <div class="pull-left">
                    <div style="float: left; width: 100px;">
                        <div id="upwebico">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button id="addMenu" class="btn btn-default">
                    <i class="fa fa-plus"></i> 导入</button> &nbsp;&nbsp;<span id="msg"></span>
            </div>
            <div id="progressFile" class="progress progress-striped active" style="display: none; z-index:-1; position:absolute; width:100%; height:39px; left:0px; top:0px;">
                <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                </div>
            </div>
        </div>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script src="/Plugin/swfobject.js" type="text/javascript"></script>
    <script type="text/javascript">
        var currentTabId = "#setpanel0";
        $(function () {
            $(".btnundo").click(function () {
                $("#excelpath").val($(this).attr("data-id"));
                $.post("/Handler/stwh_admin/sys_staff/excel.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $("#msg").addClass("text-success").html(data.msg);
                        $.bs.alert(data.msg, "success");
                    }
                    else {
                        $("#msg").addClass("text-danger").html(data.msg);
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
            $(".btndelete").click(function () {
                $.post("/Handler/stwh_admin/sys_staff/deleteFile.ashx", { data: "[{name:\"excelpath\",value:\"" + $(this).attr("data-id") + "\"}]" }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success","this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
            $("#addMenu").click(function () {
                var excelpath = $("#excelpath").val();
                if (!excelpath) {
                    $.bs.alert("请上传Excel数据文件！", "info");
                    return false;
                }
                $.post("/Handler/stwh_admin/sys_staff/excel.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $("#msg").addClass("text-success").html(data.msg);
                        $.bs.alert(data.msg, "success");
                        $("#progressFile").css("width", "0%");
                        $("#progressFile").hiden().children().first().css("width", "0%");
                    }
                    else {
                        $("#msg").addClass("text-danger").html(data.msg);
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        var auth = "<% = Request.Cookies[FormsAuthentication.FormsCookieName]==null ? string.Empty : Request.Cookies[FormsAuthentication.FormsCookieName].Value %>";
        var ASPSESSID = "<%= Session.SessionID %>";
        var flashvarsVoice = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: '<%="*."+WebSite.Webfiletype.Replace(",",";*.") %>',
            FileType: "image",
            MaxSize: "<%=WebSite.Webfilesize*1024 %>" //500KB 字节为单位
        };
        var params = {
            wmode: "transparent",
            play: true,
            loop: true,
            menu: true,
            devicefont: false,
            scale: "showall",
            quality: "high",
            bgcolor: "#ffffff",
            allowScriptAccess: "sameDomain",
            salign: ""
        };
        swfobject.embedSWF("/Plugin/upload.swf", "upwebico", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsVoice, { btnid: "upwebico", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);

        //设置进度
        function setProgress(info, name, IsSelect, btnid) {
            if (btnid == "upwebico") {
                $("#progressFile").css("width",info+"%");
                $("#progressFile").show().children().first().css("width", info + "%");
            }
        }

        //上传完成时调用的方法（该方法被flash所调用）
        //filename:文件安上传成功后返回的文件名，包含扩展名,altString提示信息,msg服务器返回的成功消息
        function EndUpload(filename, altString, msg, btnid) {
            if (msg == "NoFile") {
                $.bs.alert("请选择文件！", "info");
            }
            else if (msg == "NoType") {
                $.bs.alert("请选择指定类型文件！", "info");
            }
            else if (msg == "NoSize") {
                $.bs.alert("文件大小超出限制，请联系管理员！", "info");
            }
            else if (msg == "login") {
                $.bs.alert("请先登录！", "info");
            }
            else if (msg == "Exception") {
                $.bs.alert("请稍后操作！", "info");
            }
            else {
                if ((/image/gi).test(msg)) {
                    if (btnid == "upwebico") {
                        $("#excelpath").val(msg);
                    }
                }
                else {
                    $.bs.alert(msg, "info");
                }
            }
        }

        //上传失败时调用的方法（该方法被flash所调用）
        function ErrorUpload(msg) {
            $.bs.alert(msg, "info");
        }
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
