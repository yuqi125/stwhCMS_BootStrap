﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="stwhBT_Navbar.ascx.cs"
    Inherits="stwh_Web.UserControls.stwhBT_Navbar" %>
<div class="row navbar_bg">
    <div class="pull-left mainlogo">
        <a id="indexLogo" href="#">
            <div class="pull-left logodiv1">
                <div>
                    <h3>
                        舒 同&nbsp;&nbsp;文 化</h3>
                </div>
                <div>
                    吉 祥&nbsp;&nbsp;&nbsp;象 数&nbsp;&nbsp;&nbsp;设 计</div>
            </div>
            <div class="pull-left logodiv2">
                <h3>企龙管理系统</h3>
                <span><%=WebSite.Webversion%></span>
            </div>
        </a>
    </div>
    <div id="pmenu" class="pull-left" runat="server">
    </div>
    <div class="pull-right">
        <a href="/index.html" target="_blank" title="pc"><span class="fa fa-laptop fa-lg"></span></a>
        <a href="/mobile/index.html" target="_blank" title="移动端"><span class="fa fa-mobile-phone fa-lg"></span></a>
        <a id="webShop" href="/shop/index.html" target="_blank" title="商城网站" runat="server"><span class="fa fa-shopping-cart fa-lg"></span></a>
        <%--<a href="/stwhcms/wechat" title="公众账号管理"><span class="fa fa-wechat"></span></a>--%>
        <a id="updateCache" title="更新系统缓存"><span class="fa fa-refresh"></span></a>
        <a href="#updatePwd" id="pwd" data-toggle="modal" title="修改密码"><span class="fa fa-lock"></span></a>
        <a id="sysSound" data-id="1" title="开启系统提示音" style=" display:none;"><span class="fa fa-volume-up"></span></a>
        <a id="exit" title="安全退出"><span class="fa fa-power-off"></span></a>
    </div>
</div>
