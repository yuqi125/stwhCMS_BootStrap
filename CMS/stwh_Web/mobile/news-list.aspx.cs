﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using stwh_Web.stwh_admin.Common;

namespace stwh_Web.mobile
{
    public partial class news_list : WebPageMobileBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PageBaseVT pbvt = new PageBaseMobileVT("news-list");
            pbvt.OutPutHtml();
        }
    }
}