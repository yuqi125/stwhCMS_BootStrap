﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="installkey.aspx.cs" Inherits="stwh_Web.installkey" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>系统key设置</title>
    <style type="text/css">
        body {
            padding: 0px;
            margin: 0px auto;
        }
        div{
            width:100%;
        }
        .content {
            width: 80%;
            height: 100%;
            margin:0px auto;
            line-height: 40px;
            text-align: center;
        }
        .content input[type="text"] {
            width:500px;
            height:35px;
            line-height:35px;
            border:0px solid #ccc;
            border-bottom:1px solid #ccc;
        }
</style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="content">
            <div style="text-align:center; font-size:30px; font-weight:bold;">
                系统提示：安装好系统key以后，请删除网站更目录下的“installkey.aspx”文件，以防他人恶意篡改系统key，导致系统无法正常运行！
            </div>
            <div>
                系统key：<input type="text" name="key" id="key" value="" runat="server" />
            </div>
            <div>
                <asp:Button ID="setKey" runat="server" Text="设置" OnClick="setKey_Click" />
            </div>
        </div>
    </form>
    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
</body>
</html>
