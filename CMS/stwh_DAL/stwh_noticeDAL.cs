﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_notice
    /// </summary>
    public partial class stwh_noticeDAL : BaseDAL, Istwh_noticeDAL
    {
        public stwh_noticeDAL()
        { }
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_notice");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_notice", "stwh_noid", FieldColumn, FieldOrder, "stwh_noid,stwh_notid,stwh_notitle,stwh_nojianjie,stwh_nocontent,stwh_noaddtime,stwh_nopath,stwh_noorder,stwh_nottitle,stwh_notjianjie,stwh_notaddtime", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_notice where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_notice", "stwh_noid", FieldColumn, FieldOrder, "stwh_noid,stwh_notid,stwh_notitle,stwh_nojianjie,stwh_nocontent,stwh_noaddtime,stwh_nopath,stwh_noorder,stwh_nottitle,stwh_notjianjie,stwh_notaddtime", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_noid", "stwh_notice");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_noid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_notice");
            strSql.Append(" where stwh_noid=@stwh_noid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_noid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_noid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_notice jbmodel = model as stwh_notice;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_notice(");
            strSql.Append("stwh_notid,stwh_notitle,stwh_nojianjie,stwh_nocontent,stwh_nopath,stwh_noorder,stwh_noaddtime)");
            strSql.Append(" values (");
            strSql.Append("@stwh_notid,@stwh_notitle,@stwh_nojianjie,@stwh_nocontent,@stwh_nopath,@stwh_noorder,@stwh_noaddtime)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_notid", SqlDbType.Int,4),
					new SqlParameter("@stwh_notitle", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_nojianjie", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_nocontent", SqlDbType.NText),
                    new SqlParameter("@stwh_nopath", SqlDbType.NVarChar,500),
                    new SqlParameter("@stwh_noorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_noaddtime", SqlDbType.DateTime)};
            parameters[0].Value = jbmodel.stwh_notid;
            parameters[1].Value = jbmodel.stwh_notitle;
            parameters[2].Value = jbmodel.stwh_nojianjie;
            parameters[3].Value = jbmodel.stwh_nocontent;
            parameters[4].Value = jbmodel.stwh_nopath;
            parameters[5].Value = jbmodel.stwh_noorder;
            parameters[6].Value = jbmodel.stwh_noaddtime;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_notice jbmodel = model as stwh_notice;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_notice set ");
            strSql.Append("stwh_notid=@stwh_notid,");
            strSql.Append("stwh_notitle=@stwh_notitle,");
            strSql.Append("stwh_nojianjie=@stwh_nojianjie,");
            strSql.Append("stwh_nocontent=@stwh_nocontent,");
            strSql.Append("stwh_nopath=@stwh_nopath,");
            strSql.Append("stwh_noorder=@stwh_noorder,");
            strSql.Append("stwh_noaddtime=@stwh_noaddtime");
            strSql.Append(" where stwh_noid=@stwh_noid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_notid", SqlDbType.Int,4),
					new SqlParameter("@stwh_notitle", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_nojianjie", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_nocontent", SqlDbType.NText),
                    new SqlParameter("@stwh_nopath", SqlDbType.NVarChar,500),
                    new SqlParameter("@stwh_noorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_noaddtime", SqlDbType.DateTime),
					new SqlParameter("@stwh_noid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_notid;
            parameters[1].Value = jbmodel.stwh_notitle;
            parameters[2].Value = jbmodel.stwh_nojianjie;
            parameters[3].Value = jbmodel.stwh_nocontent;
            parameters[4].Value = jbmodel.stwh_nopath;
            parameters[5].Value = jbmodel.stwh_noorder;
            parameters[6].Value = jbmodel.stwh_noaddtime;
            parameters[7].Value = jbmodel.stwh_noid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_noid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_notice ");
            strSql.Append(" where stwh_noid=@stwh_noid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_noid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_noid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_noidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_notice ");
            strSql.Append(" where stwh_noid in (" + stwh_noidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_noid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_noid,stwh_notid,stwh_notitle,stwh_nojianjie,stwh_nocontent,stwh_noaddtime,stwh_nopath,stwh_noorder,stwh_nottitle,stwh_notjianjie,stwh_notaddtime from view_notice ");
            strSql.Append(" where stwh_noid=@stwh_noid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_noid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_noid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_notice jbmodel = new stwh_notice();
            if (row != null)
            {
                if (row["stwh_noid"] != null)
                {
                    jbmodel.stwh_noid = int.Parse(row["stwh_noid"].ToString());
                }
                if (row["stwh_notid"] != null)
                {
                    jbmodel.stwh_notid = int.Parse(row["stwh_notid"].ToString());
                }
                if (row["stwh_noorder"] != null)
                {
                    jbmodel.stwh_noorder = int.Parse(row["stwh_noorder"].ToString());
                }
                if (row["stwh_notitle"] != null)
                {
                    jbmodel.stwh_notitle = row["stwh_notitle"].ToString();
                }
                if (row["stwh_nopath"] != null)
                {
                    jbmodel.stwh_nopath = row["stwh_nopath"].ToString();
                }
                if (row["stwh_nojianjie"] != null)
                {
                    jbmodel.stwh_nojianjie = row["stwh_nojianjie"].ToString();
                }
                if (row["stwh_nocontent"] != null)
                {
                    jbmodel.stwh_nocontent = row["stwh_nocontent"].ToString();
                }
                if (row["stwh_noaddtime"] != null)
                {
                    jbmodel.stwh_noaddtime = DateTime.Parse(row["stwh_noaddtime"].ToString());
                }
                if (row.ItemArray.Length > 8)
                {
                    if (row["stwh_nottitle"] != null)
                    {
                        jbmodel.stwh_nottitle = row["stwh_nottitle"].ToString();
                    }
                    if (row["stwh_notjianjie"] != null)
                    {
                        jbmodel.stwh_notjianjie = row["stwh_notjianjie"].ToString();
                    }
                    if (row["stwh_notaddtime"] != null)
                    {
                        jbmodel.stwh_notaddtime = DateTime.Parse(row["stwh_notaddtime"].ToString());
                    }
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_noid,stwh_notid,stwh_notitle,stwh_nojianjie,stwh_nocontent,stwh_noaddtime,stwh_nopath,stwh_noorder,stwh_nottitle,stwh_notjianjie,stwh_notaddtime ");
            strSql.Append(" FROM view_notice ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_noid,stwh_notid,stwh_notitle,stwh_nojianjie,stwh_nocontent,stwh_noaddtime,stwh_nopath,stwh_noorder,stwh_nottitle,stwh_notjianjie,stwh_notaddtime ");
            strSql.Append(" FROM view_notice ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_notice ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_noid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_notice T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod

        int IBaseDAL.GetMaxId()
        {
            throw new NotImplementedException();
        }

        bool IBaseDAL.Exists(int id)
        {
            throw new NotImplementedException();
        }

        int IBaseDAL.Add(BaseModel model)
        {
            throw new NotImplementedException();
        }

        bool IBaseDAL.Update(BaseModel model)
        {
            throw new NotImplementedException();
        }

        bool IBaseDAL.Delete(int id)
        {
            throw new NotImplementedException();
        }

        bool IBaseDAL.DeleteList(string idlist)
        {
            throw new NotImplementedException();
        }

        BaseModel IBaseDAL.GetModel(int id)
        {
            throw new NotImplementedException();
        }

        BaseModel IBaseDAL.DataRowToModel(DataRow row)
        {
            throw new NotImplementedException();
        }

        DataSet IBaseDAL.GetList(string where)
        {
            throw new NotImplementedException();
        }

        DataSet IBaseDAL.GetList(int Top, string strWhere, string filedOrder)
        {
            throw new NotImplementedException();
        }

        int IBaseDAL.GetRecordCount(string strWhere)
        {
            throw new NotImplementedException();
        }

        DataSet IBaseDAL.GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            throw new NotImplementedException();
        }

        DataSet IBaseDAL.GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            throw new NotImplementedException();
        }

        DataSet IBaseDAL.GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            throw new NotImplementedException();
        }
    }
}

