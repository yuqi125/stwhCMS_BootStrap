﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_buyaddressDAL
    /// </summary>
    public partial class stwh_buyaddressDAL : BaseDAL, Istwh_buyaddressDAL
    {
        public stwh_buyaddressDAL()
        { }
        #region  Istwh_buyaddressDAL接口实现方法
        /// <summary>
        /// 更新会员地址默认状态
        /// </summary>
        /// <param name="stwh_buid">会员id</param>
        /// <param name="stwh_baid">地址id</param>
        /// <returns></returns>
        public bool Update(int stwh_buid, int stwh_baid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_buyaddress set stwh_baismr=0  where stwh_buid=@stwh_buid;");
            strSql.Append("update stwh_buyaddress set stwh_baismr=1  where stwh_baid=@stwh_baid;");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_buid", SqlDbType.Int,4),
					new SqlParameter("@stwh_baid", SqlDbType.Int,4)};
            parameters[0].Value = stwh_buid;
            parameters[1].Value = stwh_baid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 删除会员地址
        /// </summary>
        /// <param name="stwh_baid">地址编号</param>
        /// <param name="stwh_buid">会员id</param>
        /// <returns></returns>
        public bool Delete(int stwh_baid, int stwh_buid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_buyaddress ");
            strSql.Append(" where stwh_baid=@stwh_baid and stwh_buid = @stwh_buid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_baid", SqlDbType.Int,4),
                    new SqlParameter("@stwh_buid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_baid;
            parameters[1].Value = stwh_buid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 获取会员最新订单商品总价
        /// </summary>
        /// <param name="stwh_buid">会员id</param>
        /// <returns></returns>
        public decimal GetOrderTotalPrice(int stwh_buid, ref string stwh_orddid)
        {
            decimal result = 0.00m;
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@stwh_buid",SqlDbType.Int,4)};
                parameters[0].Value = stwh_buid;
                using (SqlDataReader reader = DbHelperSQL.RunProcedure("ProcSelectOrderNewUser", parameters))
                {

                    if (reader.Read())
                    {
                        result = decimal.Parse(reader["totalprice"].ToString());
                        stwh_orddid = reader["stwh_orddid"].ToString();
                    }
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        /// <summary>
        /// 获取会员默认地址
        /// </summary>
        /// <param name="stwh_buid">会员id</param>
        /// <param name="stwh_baismr">是否默认地址（0不默认，1默认）</param>
        /// <returns></returns>
        public stwh_buyaddress GetModel(int stwh_buid, int stwh_baismr = 1)
        {
            stwh_buyaddress model = null;
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@stwh_buid",SqlDbType.Int,4)};
                parameters[0].Value = stwh_buid;
                using (SqlDataReader reader = DbHelperSQL.RunProcedure("ProcSelectUserAddressDefault", parameters))
                {

                    if (reader.Read())
                    {
                        model = new stwh_buyaddress();
                        model.stwh_bumobile = reader["stwh_bumobile"].ToString();
                        model.stwh_baid = int.Parse(reader["stwh_baid"].ToString());
                        model.stwh_buid = int.Parse(reader["stwh_buid"].ToString());
                        model.stwh_baname = reader["stwh_baname"].ToString();
                        model.stwh_bamobile = reader["stwh_bamobile"].ToString();
                        model.stwh_badiqu = reader["stwh_badiqu"].ToString();
                        model.stwh_baaddress = reader["stwh_baaddress"].ToString();
                        model.stwh_bayoubian = int.Parse(reader["stwh_bayoubian"].ToString());
                        model.stwh_baismr = int.Parse(reader["stwh_baismr"].ToString());
                        model.stwh_baaddtime = DateTime.Parse(reader["stwh_baaddtime"].ToString());
                        model.stwh_bacity = int.Parse(reader["stwh_bacity"].ToString());
                    }
                }
            }
            catch (Exception)
            {

            }
            return model;
        }
        #endregion
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_buyaddress");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_buyaddress", "stwh_baid", FieldColumn, FieldOrder, "stwh_baid,stwh_buid,stwh_baname,stwh_bamobile,stwh_badiqu,stwh_baaddress,stwh_bayoubian,stwh_baismr,stwh_baaddtime,stwh_bumobile,stwh_bacity", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_buyaddress where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_buyaddress", "stwh_baid", FieldColumn, FieldOrder, "stwh_baid,stwh_buid,stwh_baname,stwh_bamobile,stwh_badiqu,stwh_baaddress,stwh_bayoubian,stwh_baismr,stwh_baaddtime,stwh_bumobile,stwh_bacity", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_baid", "stwh_buyaddress");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_baid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_buyaddress");
            strSql.Append(" where stwh_baid=@stwh_baid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_baid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_baid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_buyaddress jbmodel = model as stwh_buyaddress;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_buyaddress(");
            strSql.Append("stwh_buid,stwh_baname,stwh_bamobile,stwh_badiqu,stwh_baaddress,stwh_bayoubian,stwh_baismr,stwh_baaddtime,stwh_bacity)");
            strSql.Append(" values (");
            strSql.Append("@stwh_buid,@stwh_baname,@stwh_bamobile,@stwh_badiqu,@stwh_baaddress,@stwh_bayoubian,@stwh_baismr,@stwh_baaddtime,@stwh_bacity)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_buid", SqlDbType.Int,4),
					new SqlParameter("@stwh_baname", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_bamobile", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_badiqu", SqlDbType.NVarChar,200),
					new SqlParameter("@stwh_baaddress", SqlDbType.NVarChar,200),
					new SqlParameter("@stwh_bayoubian", SqlDbType.Int,4),
					new SqlParameter("@stwh_baismr", SqlDbType.Int,4),
					new SqlParameter("@stwh_baaddtime", SqlDbType.DateTime),
                    new SqlParameter("@stwh_bacity", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_buid;
            parameters[1].Value = jbmodel.stwh_baname;
            parameters[2].Value = jbmodel.stwh_bamobile;
            parameters[3].Value = jbmodel.stwh_badiqu;
            parameters[4].Value = jbmodel.stwh_baaddress;
            parameters[5].Value = jbmodel.stwh_bayoubian;
            parameters[6].Value = jbmodel.stwh_baismr;
            parameters[7].Value = jbmodel.stwh_baaddtime;
            parameters[8].Value = jbmodel.stwh_bacity;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null) return 0;
            else return Convert.ToInt32(obj);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_buyaddress jbmodel = model as stwh_buyaddress;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_buyaddress set ");
            strSql.Append("stwh_buid=@stwh_buid,");
            strSql.Append("stwh_baname=@stwh_baname,");
            strSql.Append("stwh_bamobile=@stwh_bamobile,");
            strSql.Append("stwh_badiqu=@stwh_badiqu,");
            strSql.Append("stwh_baaddress=@stwh_baaddress,");
            strSql.Append("stwh_bayoubian=@stwh_bayoubian,");
            strSql.Append("stwh_baismr=@stwh_baismr,");
            strSql.Append("stwh_baaddtime=@stwh_baaddtime,");
            strSql.Append("stwh_bacity=@stwh_bacity");
            strSql.Append(" where stwh_baid=@stwh_baid and stwh_buid=@stwh_buid1");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_buid", SqlDbType.Int,4),
					new SqlParameter("@stwh_baname", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_bamobile", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_badiqu", SqlDbType.NVarChar,200),
					new SqlParameter("@stwh_baaddress", SqlDbType.NVarChar,200),
					new SqlParameter("@stwh_bayoubian", SqlDbType.Int,4),
					new SqlParameter("@stwh_baismr", SqlDbType.Int,4),
					new SqlParameter("@stwh_baaddtime", SqlDbType.DateTime),
					new SqlParameter("@stwh_baid", SqlDbType.Int,4),
                    new SqlParameter("@stwh_bacity", SqlDbType.Int,4),
                    new SqlParameter("@stwh_buid1", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_buid;
            parameters[1].Value = jbmodel.stwh_baname;
            parameters[2].Value = jbmodel.stwh_bamobile;
            parameters[3].Value = jbmodel.stwh_badiqu;
            parameters[4].Value = jbmodel.stwh_baaddress;
            parameters[5].Value = jbmodel.stwh_bayoubian;
            parameters[6].Value = jbmodel.stwh_baismr;
            parameters[7].Value = jbmodel.stwh_baaddtime;
            parameters[8].Value = jbmodel.stwh_baid;
            parameters[9].Value = jbmodel.stwh_bacity;
            parameters[10].Value = jbmodel.stwh_buid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_baid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_buyaddress ");
            strSql.Append(" where stwh_baid=@stwh_baid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_baid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_baid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_baidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_buyaddress ");
            strSql.Append(" where stwh_baid in (" + stwh_baidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_baid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_baid,stwh_buid,stwh_baname,stwh_bamobile,stwh_badiqu,stwh_baaddress,stwh_bayoubian,stwh_baismr,stwh_baaddtime,stwh_bumobile,stwh_bacity from view_buyaddress ");
            strSql.Append(" where stwh_baid=@stwh_baid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_baid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_baid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0) return DataRowToModel(ds.Tables[0].Rows[0]);
            else return null;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_buyaddress model = new stwh_buyaddress();
            if (row != null)
            {
                if (row["stwh_baid"] != null)
                {
                    model.stwh_baid = int.Parse(row["stwh_baid"].ToString());
                }
                if (row["stwh_bacity"] != null)
                {
                    model.stwh_bacity = int.Parse(row["stwh_bacity"].ToString());
                }
                if (row["stwh_buid"] != null)
                {
                    model.stwh_buid = int.Parse(row["stwh_buid"].ToString());
                }
                if (row["stwh_baname"] != null)
                {
                    model.stwh_baname = row["stwh_baname"].ToString();
                }
                if (row["stwh_bamobile"] != null)
                {
                    model.stwh_bamobile = row["stwh_bamobile"].ToString();
                }
                if (row["stwh_badiqu"] != null)
                {
                    model.stwh_badiqu = row["stwh_badiqu"].ToString();
                }
                if (row["stwh_baaddress"] != null)
                {
                    model.stwh_baaddress = row["stwh_baaddress"].ToString();
                }
                if (row["stwh_bayoubian"] != null)
                {
                    model.stwh_bayoubian = int.Parse(row["stwh_bayoubian"].ToString());
                }
                if (row["stwh_baismr"] != null)
                {
                    model.stwh_baismr = int.Parse(row["stwh_baismr"].ToString());
                }
                if (row["stwh_baaddtime"] != null)
                {
                    model.stwh_baaddtime = DateTime.Parse(row["stwh_baaddtime"].ToString());
                }
                if (row.ItemArray.Length > 10)
                {
                    if (row["stwh_bumobile"] != null)
                    {
                        model.stwh_bumobile = row["stwh_bumobile"].ToString();
                    }
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_baid,stwh_buid,stwh_baname,stwh_bamobile,stwh_badiqu,stwh_baaddress,stwh_bayoubian,stwh_baismr,stwh_baaddtime,stwh_bumobile,stwh_bacity ");
            strSql.Append(" FROM view_buyaddress ");
            if (strWhere.Trim() != "") strSql.Append(" where " + strWhere);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_baid,stwh_buid,stwh_baname,stwh_bamobile,stwh_badiqu,stwh_baaddress,stwh_bayoubian,stwh_baismr,stwh_baaddtime,stwh_bumobile,stwh_bacity ");
            strSql.Append(" FROM view_buyaddress ");
            if (strWhere.Trim() != "") strSql.Append(" where " + strWhere);
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM view_buyaddress ");
            if (strWhere.Trim() != "") strSql.Append(" where " + strWhere);
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null) return 0;
            else return Convert.ToInt32(obj);
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim())) strSql.Append("order by T." + orderby);
            else strSql.Append("order by T.stwh_baid desc");
            strSql.Append(")AS Row, T.*  from view_buyaddress T ");
            if (!string.IsNullOrEmpty(strWhere.Trim())) strSql.Append(" WHERE " + strWhere);
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

