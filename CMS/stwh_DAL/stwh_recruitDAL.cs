﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_recruit
    /// </summary>
    public partial class stwh_recruitDAL : BaseDAL, Istwh_recruitDAL
    {
        public stwh_recruitDAL()
        { }
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_recruit");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_recruit", "stwh_rtid", FieldColumn, FieldOrder, "stwh_rtid,stwh_rtnamejd,stwh_rtname,stwh_rtzhize,stwh_rtyaoqiu,stwh_rtfuli,stwh_rtmoney,stwh_rtxueli,stwh_rtjobtime,stwh_rtpeople,stwh_rtplace,stwh_rtaddtime,stwh_rtcname,stwh_rtchangye,stwh_rtcxingzhi,stwh_rtcguimo,stwh_rtorder,stwh_rtimg", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_recruit where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_recruit", "stwh_rtid", FieldColumn, FieldOrder, "stwh_rtid,stwh_rtnamejd,stwh_rtname,stwh_rtzhize,stwh_rtyaoqiu,stwh_rtfuli,stwh_rtmoney,stwh_rtxueli,stwh_rtjobtime,stwh_rtpeople,stwh_rtplace,stwh_rtaddtime,stwh_rtcname,stwh_rtchangye,stwh_rtcxingzhi,stwh_rtcguimo,stwh_rtorder,stwh_rtimg", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_rtid", "stwh_recruit");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_rtid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_recruit");
            strSql.Append(" where stwh_rtid=@stwh_rtid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_rtid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_rtid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_recruit jbmodel = model as stwh_recruit;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_recruit(");
            strSql.Append("stwh_rtnamejd,stwh_rtname,stwh_rtzhize,stwh_rtyaoqiu,stwh_rtfuli,stwh_rtmoney,stwh_rtxueli,stwh_rtjobtime,stwh_rtpeople,stwh_rtplace,stwh_rtaddtime,stwh_rtcname,stwh_rtchangye,stwh_rtcxingzhi,stwh_rtcguimo,stwh_rtorder,stwh_rtimg)");
            strSql.Append(" values (");
            strSql.Append("@stwh_rtnamejd,@stwh_rtname,@stwh_rtzhize,@stwh_rtyaoqiu,@stwh_rtfuli,@stwh_rtmoney,@stwh_rtxueli,@stwh_rtjobtime,@stwh_rtpeople,@stwh_rtplace,@stwh_rtaddtime,@stwh_rtcname,@stwh_rtchangye,@stwh_rtcxingzhi,@stwh_rtcguimo,@stwh_rtorder,@stwh_rtimg)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_rtnamejd", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtzhize", SqlDbType.NText),
					new SqlParameter("@stwh_rtyaoqiu", SqlDbType.NText),
					new SqlParameter("@stwh_rtfuli", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtmoney", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtxueli", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtjobtime", SqlDbType.Int,4),
					new SqlParameter("@stwh_rtpeople", SqlDbType.Int,4),
					new SqlParameter("@stwh_rtplace", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtaddtime", SqlDbType.DateTime),
					new SqlParameter("@stwh_rtcname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtchangye", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtcxingzhi", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtcguimo", SqlDbType.NVarChar,300),
                    new SqlParameter("@stwh_rtorder", SqlDbType.Int,4),
                    new SqlParameter("@stwh_rtimg", SqlDbType.NVarChar,500)};
            parameters[0].Value = jbmodel.stwh_rtnamejd;
            parameters[1].Value = jbmodel.stwh_rtname;
            parameters[2].Value = jbmodel.stwh_rtzhize;
            parameters[3].Value = jbmodel.stwh_rtyaoqiu;
            parameters[4].Value = jbmodel.stwh_rtfuli;
            parameters[5].Value = jbmodel.stwh_rtmoney;
            parameters[6].Value = jbmodel.stwh_rtxueli;
            parameters[7].Value = jbmodel.stwh_rtjobtime;
            parameters[8].Value = jbmodel.stwh_rtpeople;
            parameters[9].Value = jbmodel.stwh_rtplace;
            parameters[10].Value = jbmodel.stwh_rtaddtime;
            parameters[11].Value = jbmodel.stwh_rtcname;
            parameters[12].Value = jbmodel.stwh_rtchangye;
            parameters[13].Value = jbmodel.stwh_rtcxingzhi;
            parameters[14].Value = jbmodel.stwh_rtcguimo;
            parameters[15].Value = jbmodel.stwh_rtorder;
            parameters[16].Value = jbmodel.stwh_rtimg;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_recruit jbmodel = model as stwh_recruit;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_recruit set ");
            strSql.Append("stwh_rtnamejd=@stwh_rtnamejd,");
            strSql.Append("stwh_rtname=@stwh_rtname,");
            strSql.Append("stwh_rtzhize=@stwh_rtzhize,");
            strSql.Append("stwh_rtyaoqiu=@stwh_rtyaoqiu,");
            strSql.Append("stwh_rtfuli=@stwh_rtfuli,");
            strSql.Append("stwh_rtmoney=@stwh_rtmoney,");
            strSql.Append("stwh_rtxueli=@stwh_rtxueli,");
            strSql.Append("stwh_rtjobtime=@stwh_rtjobtime,");
            strSql.Append("stwh_rtpeople=@stwh_rtpeople,");
            strSql.Append("stwh_rtplace=@stwh_rtplace,");
            strSql.Append("stwh_rtaddtime=@stwh_rtaddtime,");
            strSql.Append("stwh_rtcname=@stwh_rtcname,");
            strSql.Append("stwh_rtchangye=@stwh_rtchangye,");
            strSql.Append("stwh_rtcxingzhi=@stwh_rtcxingzhi,");
            strSql.Append("stwh_rtcguimo=@stwh_rtcguimo,");
            strSql.Append("stwh_rtorder=@stwh_rtorder,");
            strSql.Append("stwh_rtimg=@stwh_rtimg");
            strSql.Append(" where stwh_rtid=@stwh_rtid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_rtnamejd", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtzhize", SqlDbType.NText),
					new SqlParameter("@stwh_rtyaoqiu", SqlDbType.NText),
					new SqlParameter("@stwh_rtfuli", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtmoney", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtxueli", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtjobtime", SqlDbType.Int,4),
					new SqlParameter("@stwh_rtpeople", SqlDbType.Int,4),
					new SqlParameter("@stwh_rtplace", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtaddtime", SqlDbType.DateTime),
					new SqlParameter("@stwh_rtcname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtchangye", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtcxingzhi", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_rtcguimo", SqlDbType.NVarChar,300),
                    new SqlParameter("@stwh_rtorder", SqlDbType.Int,4),
                    new SqlParameter("@stwh_rtimg", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_rtid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_rtnamejd;
            parameters[1].Value = jbmodel.stwh_rtname;
            parameters[2].Value = jbmodel.stwh_rtzhize;
            parameters[3].Value = jbmodel.stwh_rtyaoqiu;
            parameters[4].Value = jbmodel.stwh_rtfuli;
            parameters[5].Value = jbmodel.stwh_rtmoney;
            parameters[6].Value = jbmodel.stwh_rtxueli;
            parameters[7].Value = jbmodel.stwh_rtjobtime;
            parameters[8].Value = jbmodel.stwh_rtpeople;
            parameters[9].Value = jbmodel.stwh_rtplace;
            parameters[10].Value = jbmodel.stwh_rtaddtime;
            parameters[11].Value = jbmodel.stwh_rtcname;
            parameters[12].Value = jbmodel.stwh_rtchangye;
            parameters[13].Value = jbmodel.stwh_rtcxingzhi;
            parameters[14].Value = jbmodel.stwh_rtcguimo;
            parameters[15].Value = jbmodel.stwh_rtorder;
            parameters[16].Value = jbmodel.stwh_rtimg;
            parameters[17].Value = jbmodel.stwh_rtid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_rtid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_recruit ");
            strSql.Append(" where stwh_rtid=@stwh_rtid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_rtid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_rtid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_rtidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_recruit ");
            strSql.Append(" where stwh_rtid in (" + stwh_rtidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_rtid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_rtid,stwh_rtnamejd,stwh_rtname,stwh_rtzhize,stwh_rtyaoqiu,stwh_rtfuli,stwh_rtmoney,stwh_rtxueli,stwh_rtjobtime,stwh_rtpeople,stwh_rtplace,stwh_rtaddtime,stwh_rtcname,stwh_rtchangye,stwh_rtcxingzhi,stwh_rtcguimo,stwh_rtorder,stwh_rtimg from stwh_recruit ");
            strSql.Append(" where stwh_rtid=@stwh_rtid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_rtid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_rtid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_recruit jbmodel = new stwh_recruit();
            if (row != null)
            {
                if (row["stwh_rtid"] != null)
                {
                    jbmodel.stwh_rtid = int.Parse(row["stwh_rtid"].ToString());
                }
                if (row["stwh_rtimg"] != null)
                {
                    jbmodel.stwh_rtimg = row["stwh_rtimg"].ToString();
                }
                if (row["stwh_rtnamejd"] != null)
                {
                    jbmodel.stwh_rtnamejd = row["stwh_rtnamejd"].ToString();
                }
                if (row["stwh_rtname"] != null)
                {
                    jbmodel.stwh_rtname = row["stwh_rtname"].ToString();
                }
                if (row["stwh_rtzhize"] != null)
                {
                    jbmodel.stwh_rtzhize = row["stwh_rtzhize"].ToString();
                }
                if (row["stwh_rtyaoqiu"] != null)
                {
                    jbmodel.stwh_rtyaoqiu = row["stwh_rtyaoqiu"].ToString();
                }
                if (row["stwh_rtfuli"] != null)
                {
                    jbmodel.stwh_rtfuli = row["stwh_rtfuli"].ToString();
                }
                if (row["stwh_rtmoney"] != null)
                {
                    jbmodel.stwh_rtmoney = row["stwh_rtmoney"].ToString();
                }
                if (row["stwh_rtxueli"] != null)
                {
                    jbmodel.stwh_rtxueli = row["stwh_rtxueli"].ToString();
                }
                if (row["stwh_rtjobtime"] != null)
                {
                    jbmodel.stwh_rtjobtime = int.Parse(row["stwh_rtjobtime"].ToString());
                }
                if (row["stwh_rtpeople"] != null)
                {
                    jbmodel.stwh_rtpeople = int.Parse(row["stwh_rtpeople"].ToString());
                }
                if (row["stwh_rtplace"] != null)
                {
                    jbmodel.stwh_rtplace = row["stwh_rtplace"].ToString();
                }
                if (row["stwh_rtaddtime"] != null)
                {
                    jbmodel.stwh_rtaddtime = DateTime.Parse(row["stwh_rtaddtime"].ToString());
                }
                if (row["stwh_rtcname"] != null)
                {
                    jbmodel.stwh_rtcname = row["stwh_rtcname"].ToString();
                }
                if (row["stwh_rtchangye"] != null)
                {
                    jbmodel.stwh_rtchangye = row["stwh_rtchangye"].ToString();
                }
                if (row["stwh_rtcxingzhi"] != null)
                {
                    jbmodel.stwh_rtcxingzhi = row["stwh_rtcxingzhi"].ToString();
                }
                if (row["stwh_rtcguimo"] != null)
                {
                    jbmodel.stwh_rtcguimo = row["stwh_rtcguimo"].ToString();
                }
                if (row["stwh_rtorder"] != null)
                {
                    jbmodel.stwh_rtorder = int.Parse(row["stwh_rtorder"].ToString());
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_rtid,stwh_rtnamejd,stwh_rtname,stwh_rtzhize,stwh_rtyaoqiu,stwh_rtfuli,stwh_rtmoney,stwh_rtxueli,stwh_rtjobtime,stwh_rtpeople,stwh_rtplace,stwh_rtaddtime,stwh_rtcname,stwh_rtchangye,stwh_rtcxingzhi,stwh_rtcguimo,stwh_rtorder,stwh_rtimg ");
            strSql.Append(" FROM stwh_recruit ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_rtid,stwh_rtnamejd,stwh_rtname,stwh_rtzhize,stwh_rtyaoqiu,stwh_rtfuli,stwh_rtmoney,stwh_rtxueli,stwh_rtjobtime,stwh_rtpeople,stwh_rtplace,stwh_rtaddtime,stwh_rtcname,stwh_rtchangye,stwh_rtcxingzhi,stwh_rtcguimo,stwh_rtorder,stwh_rtimg ");
            strSql.Append(" FROM stwh_recruit ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_recruit ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_rtid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_recruit T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

