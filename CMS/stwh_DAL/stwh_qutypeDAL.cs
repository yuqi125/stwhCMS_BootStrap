﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_qutype
    /// </summary>
    public partial class stwh_qutypeDAL : BaseDAL, Istwh_qutypeDAL
    {
        public stwh_qutypeDAL()
        { }
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_qutype");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_qutype", "stwh_qutid", FieldColumn, FieldOrder, "stwh_qutid,stwh_qutname,stwh_qutdescription,stwh_qutparentid,stwh_qutorder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_qutype where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_qutype", "stwh_qutid", FieldColumn, FieldOrder, "stwh_qutid,stwh_qutname,stwh_qutdescription,stwh_qutparentid,stwh_qutorder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_qutid", "stwh_qutype");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_qutid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_qutype");
            strSql.Append(" where stwh_qutid=@stwh_qutid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_qutid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_qutid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_qutype jbmodel = model as stwh_qutype;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("if (select count(1) from stwh_qutype where stwh_qutname = '" + jbmodel.stwh_qutname + "') = 0 begin ");
            strSql.Append("insert into stwh_qutype(");
            strSql.Append("stwh_qutname,stwh_qutdescription,stwh_qutparentid,stwh_qutorder)");
            strSql.Append(" values (");
            strSql.Append("@stwh_qutname,@stwh_qutdescription,@stwh_qutparentid,@stwh_qutorder)");
            strSql.Append(";select @@IDENTITY");
            strSql.Append(" end");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_qutname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_qutdescription", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_qutparentid", SqlDbType.Int,4),
                    new SqlParameter("@stwh_qutorder", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_qutname;
            parameters[1].Value = jbmodel.stwh_qutdescription;
            parameters[2].Value = jbmodel.stwh_qutparentid;
            parameters[3].Value = jbmodel.stwh_qutorder;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_qutype jbmodel = model as stwh_qutype;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("if (select count(1) from stwh_qutype where stwh_qutname = '" + jbmodel.stwh_qutname + "' and stwh_qutid <> " + jbmodel.stwh_qutid + ") = 0 begin ");
            strSql.Append("update stwh_qutype set ");
            strSql.Append("stwh_qutname=@stwh_qutname,");
            strSql.Append("stwh_qutdescription=@stwh_qutdescription,");
            strSql.Append("stwh_qutparentid=@stwh_qutparentid,");
            strSql.Append("stwh_qutorder=@stwh_qutorder");
            strSql.Append(" where stwh_qutid=@stwh_qutid");
            strSql.Append(" end");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_qutname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_qutdescription", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_qutparentid", SqlDbType.Int,4),
                    new SqlParameter("@stwh_qutorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_qutid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_qutname;
            parameters[1].Value = jbmodel.stwh_qutdescription;
            parameters[2].Value = jbmodel.stwh_qutparentid;
            parameters[3].Value = jbmodel.stwh_qutorder;
            parameters[4].Value = jbmodel.stwh_qutid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_qutid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_qutype ");
            strSql.Append(" where stwh_qutid=@stwh_qutid or stwh_qutparentid = " + stwh_qutid);
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_qutid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_qutid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_qutidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_questions ");
            strSql.Append(" where stwh_qutid in (" + stwh_qutidlist + ");");
            strSql.Append("delete from stwh_qutype ");
            strSql.Append(" where stwh_qutid in (" + stwh_qutidlist + ") or stwh_qutparentid in (" + stwh_qutidlist + ");");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_qutid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_qutid,stwh_qutname,stwh_qutdescription,stwh_qutparentid,stwh_qutorder from stwh_qutype ");
            strSql.Append(" where stwh_qutid=@stwh_qutid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_qutid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_qutid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_qutype jbmodel = new stwh_qutype();
            if (row != null)
            {
                if (row["stwh_qutid"] != null)
                {
                    jbmodel.stwh_qutid = int.Parse(row["stwh_qutid"].ToString());
                }
                if (row["stwh_qutname"] != null)
                {
                    jbmodel.stwh_qutname = row["stwh_qutname"].ToString();
                }
                if (row["stwh_qutdescription"] != null)
                {
                    jbmodel.stwh_qutdescription = row["stwh_qutdescription"].ToString();
                }
                if (row["stwh_qutparentid"] != null)
                {
                    jbmodel.stwh_qutparentid = int.Parse(row["stwh_qutparentid"].ToString());
                }
                if (row["stwh_qutorder"] != null)
                {
                    jbmodel.stwh_qutorder = int.Parse(row["stwh_qutorder"].ToString());
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_qutid,stwh_qutname,stwh_qutdescription,stwh_qutparentid,stwh_qutorder ");
            strSql.Append(" FROM stwh_qutype ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_qutid,stwh_qutname,stwh_qutdescription,stwh_qutparentid,stwh_qutorder ");
            strSql.Append(" FROM stwh_qutype ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_qutype ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_qutid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_qutype T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

