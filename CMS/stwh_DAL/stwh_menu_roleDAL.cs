﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_menu_role
    /// </summary>
    public partial class stwh_menu_roleDAL : BaseDAL, Istwh_menu_roleDAL
    {
        public stwh_menu_roleDAL()
        { }
        #region Istwh_menu_roleDAL接口实现方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(stwh_menu_role jbmodel)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_menu_role(");
            strSql.Append("stwh_menuid,stwh_rid)");
            strSql.Append(" values (");
            strSql.Append("@stwh_menuid,@stwh_rid)");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_menuid", SqlDbType.Int,4),
					new SqlParameter("@stwh_rid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_menuid;
            parameters[1].Value = jbmodel.stwh_rid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 添加角色菜单信息，去除重复
        /// </summary>
        /// <param name="stwh_rid">角色id</param>
        /// <param name="stwh_menuid">所属菜单id（多个菜单id以','分割）</param>
        /// <returns></returns>
        public int Add(int stwh_rid, string stwh_menuid)
        {
            int result = 0;
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@stwh_rid",SqlDbType.Int,4),
                    new SqlParameter("@stwh_menuid",SqlDbType.NVarChar,100),
                    new SqlParameter("@result",SqlDbType.Int,4)};
                parameters[0].Value = stwh_rid;
                parameters[1].Value = stwh_menuid;
                parameters[2].Direction = ParameterDirection.Output;
                DbHelperSQL.RunProcedure("ProcMenuRoleAdd", parameters);
                result = int.Parse(parameters[2].Value.ToString());
            }
            catch (Exception)
            {
            }
            return result;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_menuid, int stwh_rid)
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_menu_role ");
            strSql.Append(" where ");
            strSql.Append("stwh_menuid=@stwh_menuid and ");
            strSql.Append("stwh_rid=@stwh_rid");
            SqlParameter[] parameters = {
			        new SqlParameter("@stwh_menuid", SqlDbType.Int,4),
					new SqlParameter("@stwh_rid", SqlDbType.Int,4)};

            parameters[0].Value = stwh_menuid;
            parameters[1].Value = stwh_rid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere, int flag)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_menuid,stwh_rid ");
            strSql.Append(" FROM stwh_menu_role ");
            if (strWhere.Trim() != "") strSql.Append(" where " + strWhere);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public stwh_menu_role GetModel()
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_menuid,stwh_menuname,stwh_menunameUS,stwh_menuICO_url,stwh_menuURL,stwh_menuparentID,stwh_menustatus,stwh_menudescription,stwh_menuorder,stwh_rid,stwh_rname,stwh_rdescription,stwh_rctime,stwh_rstate,stwh_rdelstate from view_role_menu ");
            strSql.Append(" where ");
            SqlParameter[] parameters = {
			};

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]) as stwh_menu_role;
            }
            else
            {
                return null;
            }
        }
        #endregion
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_role_menu");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_role_menu", "stwh_menuid", FieldColumn, FieldOrder, "stwh_rctime,stwh_rdelstate,stwh_rdescription,stwh_rid,stwh_rname,stwh_rstate,stwh_menudescription,stwh_menuICO_url,stwh_menuid,stwh_menuname,stwh_menunameUS,stwh_menuparentID,stwh_menustatus,stwh_menuURL,stwh_menuorder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <param name="flag">标识</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from view_role_menu");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("view_role_menu", "stwh_menuid", FieldColumn, FieldOrder, "stwh_rctime,stwh_rdelstate,stwh_rdescription,stwh_rid,stwh_rname,stwh_rstate,stwh_menudescription,stwh_menuICO_url,stwh_menuid,stwh_menuname,stwh_menunameUS,stwh_menuparentID,stwh_menustatus,stwh_menuURL,stwh_menuorder", If, pageSize, pageNumber, ref selectCount);
        }

        public int GetMaxId()
        {
            throw new NotImplementedException();
        }

        public bool Exists(int id)
        {
            throw new NotImplementedException();
        }

        public int Add(BaseModel model)
        {
            throw new NotImplementedException();
        }

        public bool DeleteList(string idlist)
        {
            throw new NotImplementedException();
        }

        public BaseModel GetModel(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_menu_role jbmodel = model as stwh_menu_role;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_menu_role set ");
            strSql.Append("stwh_menuid=@stwh_menuid,");
            strSql.Append("stwh_rid=@stwh_rid");
            strSql.Append(" where ");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_menuid", SqlDbType.Int,4),
					new SqlParameter("@stwh_rid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_menuid;
            parameters[1].Value = jbmodel.stwh_rid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除指定角色菜单数据
        /// </summary>
        /// <param name="stwh_rid">角色id</param>
        /// <returns></returns>
        public bool Delete(int stwh_rid)
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_menu_role");
            strSql.Append(" where stwh_rid = @stwh_rid");
            SqlParameter[] parameters = {
                new SqlParameter("@stwh_rid",SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_rid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_menu_role jbmodel = new stwh_menu_role();
            if (row != null)
            {
                if (row["stwh_menuid"] != null)
                {
                    jbmodel.stwh_menuid = int.Parse(row["stwh_menuid"].ToString());
                }
                if (row["stwh_rid"] != null)
                {
                    jbmodel.stwh_rid = int.Parse(row["stwh_rid"].ToString());
                }
                if (row.ItemArray.Length > 2)
                {
                    if (row["stwh_menuname"] != null)
                    {
                        jbmodel.stwh_menuname = row["stwh_menuname"].ToString();
                    }
                    if (row["stwh_menunameUS"] != null)
                    {
                        jbmodel.stwh_menunameUS = row["stwh_menunameUS"].ToString();
                    }
                    if (row["stwh_menuICO_url"] != null)
                    {
                        jbmodel.stwh_menuICO_url = row["stwh_menuICO_url"].ToString();
                    }
                    if (row["stwh_menuURL"] != null)
                    {
                        jbmodel.stwh_menuURL = row["stwh_menuURL"].ToString();
                    }
                    if (row["stwh_menuparentID"] != null)
                    {
                        jbmodel.stwh_menuparentID = int.Parse(row["stwh_menuparentID"].ToString());
                    }
                    if (row["stwh_menustatus"] != null)
                    {
                        jbmodel.stwh_menustatus = int.Parse(row["stwh_menustatus"].ToString());
                    }
                    if (row["stwh_menuorder"] != null)
                    {
                        jbmodel.stwh_menuorder = int.Parse(row["stwh_menuorder"].ToString());
                    }
                    if (row["stwh_menudescription"] != null)
                    {
                        jbmodel.stwh_menudescription = row["stwh_menudescription"].ToString();
                    }
                    if (row["stwh_rname"] != null)
                    {
                        jbmodel.stwh_rname = row["stwh_rname"].ToString();
                    }
                    if (row["stwh_rdescription"] != null)
                    {
                        jbmodel.stwh_rdescription = row["stwh_rdescription"].ToString();
                    }
                    if (row["stwh_rctime"] != null)
                    {
                        jbmodel.stwh_rctime = DateTime.Parse(row["stwh_rctime"].ToString());
                    }
                    if (row["stwh_rstate"] != null)
                    {
                        jbmodel.stwh_rstate = int.Parse(row["stwh_rstate"].ToString());
                    }
                    if (row["stwh_rdelstate"] != null)
                    {
                        jbmodel.stwh_rdelstate = int.Parse(row["stwh_rdelstate"].ToString());
                    }
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_menuid,stwh_menuname,stwh_menunameUS,stwh_menuICO_url,stwh_menuURL,stwh_menuparentID,stwh_menustatus,stwh_menudescription,stwh_menuorder,stwh_rid,stwh_rname,stwh_rdescription,stwh_rctime,stwh_rstate,stwh_rdelstate ");
            strSql.Append(" FROM view_role_menu ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_menuid,stwh_menuname,stwh_menunameUS,stwh_menuICO_url,stwh_menuURL,stwh_menuparentID,stwh_menustatus,stwh_menudescription,stwh_menuorder,stwh_rid,stwh_rname,stwh_rdescription,stwh_rctime,stwh_rstate,stwh_rdelstate ");
            strSql.Append(" FROM view_role_menu ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM view_role_menu ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_loid desc");
            }
            strSql.Append(")AS Row, T.*  from view_role_menu T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

