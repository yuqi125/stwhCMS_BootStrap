﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_bidding
    /// </summary>
    public partial class stwh_biddingDAL : BaseDAL, Istwh_biddingDAL
    {
        public stwh_biddingDAL()
        { }
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_bidding");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_bidding", "stwh_bdid", FieldColumn, FieldOrder, "stwh_bdid,stwh_bdimg,stwh_bdnumber,stwh_bdtitle,stwh_bdjianjie,stwh_bdcontent,stwh_bdcompany,stwh_bdlxr,stwh_bdtel,stwh_bdaddress,stwh_bdyoubian,stwh_bdendtime,stwh_bdstatus,stwh_bdpath,stwh_bdorder,stwh_bdaddtime", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_bidding where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_bidding", "stwh_bdid", FieldColumn, FieldOrder, "stwh_bdid,stwh_bdimg,stwh_bdnumber,stwh_bdtitle,stwh_bdjianjie,stwh_bdcontent,stwh_bdcompany,stwh_bdlxr,stwh_bdtel,stwh_bdaddress,stwh_bdyoubian,stwh_bdendtime,stwh_bdstatus,stwh_bdpath,stwh_bdorder,stwh_bdaddtime", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_bdid", "stwh_bidding");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_bdid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_bidding");
            strSql.Append(" where stwh_bdid=@stwh_bdid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_bdid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_bdid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_bidding jbmodel = model as stwh_bidding;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_bidding(");
            strSql.Append("stwh_bdimg,stwh_bdnumber,stwh_bdtitle,stwh_bdjianjie,stwh_bdcontent,stwh_bdcompany,stwh_bdlxr,stwh_bdtel,stwh_bdaddress,stwh_bdyoubian,stwh_bdendtime,stwh_bdstatus,stwh_bdpath,stwh_bdorder,stwh_bdaddtime)");
            strSql.Append(" values (");
            strSql.Append("@stwh_bdimg,@stwh_bdnumber,@stwh_bdtitle,@stwh_bdjianjie,@stwh_bdcontent,@stwh_bdcompany,@stwh_bdlxr,@stwh_bdtel,@stwh_bdaddress,@stwh_bdyoubian,@stwh_bdendtime,@stwh_bdstatus,@stwh_bdpath,@stwh_bdorder,@stwh_bdaddtime)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_bdimg", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdnumber", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdtitle", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdjianjie", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdcontent", SqlDbType.NText),
					new SqlParameter("@stwh_bdcompany", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdlxr", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdtel", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdaddress", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdyoubian", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdendtime", SqlDbType.DateTime),
					new SqlParameter("@stwh_bdstatus", SqlDbType.Int,4),
					new SqlParameter("@stwh_bdpath", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_bdaddtime", SqlDbType.DateTime)};
            parameters[0].Value = jbmodel.stwh_bdimg;
            parameters[1].Value = jbmodel.stwh_bdnumber;
            parameters[2].Value = jbmodel.stwh_bdtitle;
            parameters[3].Value = jbmodel.stwh_bdjianjie;
            parameters[4].Value = jbmodel.stwh_bdcontent;
            parameters[5].Value = jbmodel.stwh_bdcompany;
            parameters[6].Value = jbmodel.stwh_bdlxr;
            parameters[7].Value = jbmodel.stwh_bdtel;
            parameters[8].Value = jbmodel.stwh_bdaddress;
            parameters[9].Value = jbmodel.stwh_bdyoubian;
            parameters[10].Value = jbmodel.stwh_bdendtime;
            parameters[11].Value = jbmodel.stwh_bdstatus;
            parameters[12].Value = jbmodel.stwh_bdpath;
            parameters[13].Value = jbmodel.stwh_bdorder;
            parameters[14].Value = jbmodel.stwh_bdaddtime;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_bidding jbmodel = model as stwh_bidding;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_bidding set ");
            strSql.Append("stwh_bdimg=@stwh_bdimg,");
            strSql.Append("stwh_bdnumber=@stwh_bdnumber,");
            strSql.Append("stwh_bdtitle=@stwh_bdtitle,");
            strSql.Append("stwh_bdjianjie=@stwh_bdjianjie,");
            strSql.Append("stwh_bdcontent=@stwh_bdcontent,");
            strSql.Append("stwh_bdcompany=@stwh_bdcompany,");
            strSql.Append("stwh_bdlxr=@stwh_bdlxr,");
            strSql.Append("stwh_bdtel=@stwh_bdtel,");
            strSql.Append("stwh_bdaddress=@stwh_bdaddress,");
            strSql.Append("stwh_bdyoubian=@stwh_bdyoubian,");
            strSql.Append("stwh_bdendtime=@stwh_bdendtime,");
            strSql.Append("stwh_bdstatus=@stwh_bdstatus,");
            strSql.Append("stwh_bdpath=@stwh_bdpath,");
            strSql.Append("stwh_bdorder=@stwh_bdorder,");
            strSql.Append("stwh_bdaddtime=@stwh_bdaddtime");
            strSql.Append(" where stwh_bdid=@stwh_bdid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_bdimg", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdnumber", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdtitle", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdjianjie", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdcontent", SqlDbType.NText),
					new SqlParameter("@stwh_bdcompany", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdlxr", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdtel", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdaddress", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdyoubian", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdendtime", SqlDbType.DateTime),
					new SqlParameter("@stwh_bdstatus", SqlDbType.Int,4),
					new SqlParameter("@stwh_bdpath", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bdorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_bdaddtime", SqlDbType.DateTime),
					new SqlParameter("@stwh_bdid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_bdimg;
            parameters[1].Value = jbmodel.stwh_bdnumber;
            parameters[2].Value = jbmodel.stwh_bdtitle;
            parameters[3].Value = jbmodel.stwh_bdjianjie;
            parameters[4].Value = jbmodel.stwh_bdcontent;
            parameters[5].Value = jbmodel.stwh_bdcompany;
            parameters[6].Value = jbmodel.stwh_bdlxr;
            parameters[7].Value = jbmodel.stwh_bdtel;
            parameters[8].Value = jbmodel.stwh_bdaddress;
            parameters[9].Value = jbmodel.stwh_bdyoubian;
            parameters[10].Value = jbmodel.stwh_bdendtime;
            parameters[11].Value = jbmodel.stwh_bdstatus;
            parameters[12].Value = jbmodel.stwh_bdpath;
            parameters[13].Value = jbmodel.stwh_bdorder;
            parameters[14].Value = jbmodel.stwh_bdaddtime;
            parameters[15].Value = jbmodel.stwh_bdid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_bdid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_bidding ");
            strSql.Append(" where stwh_bdid=@stwh_bdid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_bdid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_bdid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_bdidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_bidding ");
            strSql.Append(" where stwh_bdid in (" + stwh_bdidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_bdid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_bdid,stwh_bdimg,stwh_bdnumber,stwh_bdtitle,stwh_bdjianjie,stwh_bdcontent,stwh_bdcompany,stwh_bdlxr,stwh_bdtel,stwh_bdaddress,stwh_bdyoubian,stwh_bdendtime,stwh_bdstatus,stwh_bdpath,stwh_bdorder,stwh_bdaddtime from stwh_bidding ");
            strSql.Append(" where stwh_bdid=@stwh_bdid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_bdid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_bdid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_bidding model = new stwh_bidding();
            if (row != null)
            {
                if (row["stwh_bdid"] != null)
                {
                    model.stwh_bdid = int.Parse(row["stwh_bdid"].ToString());
                }
                if (row["stwh_bdimg"] != null)
                {
                    model.stwh_bdimg = row["stwh_bdimg"].ToString();
                }
                if (row["stwh_bdnumber"] != null)
                {
                    model.stwh_bdnumber = row["stwh_bdnumber"].ToString();
                }
                if (row["stwh_bdtitle"] != null)
                {
                    model.stwh_bdtitle = row["stwh_bdtitle"].ToString();
                }
                if (row["stwh_bdjianjie"] != null)
                {
                    model.stwh_bdjianjie = row["stwh_bdjianjie"].ToString();
                }
                if (row["stwh_bdcontent"] != null)
                {
                    model.stwh_bdcontent = row["stwh_bdcontent"].ToString();
                }
                if (row["stwh_bdcompany"] != null)
                {
                    model.stwh_bdcompany = row["stwh_bdcompany"].ToString();
                }
                if (row["stwh_bdlxr"] != null)
                {
                    model.stwh_bdlxr = row["stwh_bdlxr"].ToString();
                }
                if (row["stwh_bdtel"] != null)
                {
                    model.stwh_bdtel = row["stwh_bdtel"].ToString();
                }
                if (row["stwh_bdaddress"] != null)
                {
                    model.stwh_bdaddress = row["stwh_bdaddress"].ToString();
                }
                if (row["stwh_bdyoubian"] != null)
                {
                    model.stwh_bdyoubian = row["stwh_bdyoubian"].ToString();
                }
                if (row["stwh_bdendtime"] != null)
                {
                    model.stwh_bdendtime = DateTime.Parse(row["stwh_bdendtime"].ToString());
                }
                if (row["stwh_bdstatus"] != null && row["stwh_bdstatus"].ToString() != "")
                {
                    model.stwh_bdstatus = int.Parse(row["stwh_bdstatus"].ToString());
                }
                if (row["stwh_bdpath"] != null)
                {
                    model.stwh_bdpath = row["stwh_bdpath"].ToString();
                }
                if (row["stwh_bdorder"] != null)
                {
                    model.stwh_bdorder = int.Parse(row["stwh_bdorder"].ToString());
                }
                if (row["stwh_bdaddtime"] != null)
                {
                    model.stwh_bdaddtime = DateTime.Parse(row["stwh_bdaddtime"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_bdid,stwh_bdimg,stwh_bdnumber,stwh_bdtitle,stwh_bdjianjie,stwh_bdcontent,stwh_bdcompany,stwh_bdlxr,stwh_bdtel,stwh_bdaddress,stwh_bdyoubian,stwh_bdendtime,stwh_bdstatus,stwh_bdpath,stwh_bdorder,stwh_bdaddtime ");
            strSql.Append(" FROM stwh_bidding ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_bdid,stwh_bdimg,stwh_bdnumber,stwh_bdtitle,stwh_bdjianjie,stwh_bdcontent,stwh_bdcompany,stwh_bdlxr,stwh_bdtel,stwh_bdaddress,stwh_bdyoubian,stwh_bdendtime,stwh_bdstatus,stwh_bdpath,stwh_bdorder,stwh_bdaddtime ");
            strSql.Append(" FROM stwh_bidding ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_bidding ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_bdid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_bidding T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}

