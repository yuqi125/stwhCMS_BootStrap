﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_cjtel:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_cjtel : BaseModel
	{
		public stwh_cjtel()
		{}
		#region Model
		private int _stwh_cjid;
		private string _stwh_cjname;
		private string _stwh_cjmobile;
		private DateTime _stwh_cjaddtime;
		/// <summary>
		/// 
		/// </summary>
		public int stwh_cjid
		{
			set{ _stwh_cjid=value;}
			get{return _stwh_cjid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_cjname
		{
			set{ _stwh_cjname=value;}
			get{return _stwh_cjname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_cjmobile
		{
			set{ _stwh_cjmobile=value;}
			get{return _stwh_cjmobile;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_cjaddtime
		{
			set{ _stwh_cjaddtime=value;}
			get{return _stwh_cjaddtime;}
		}
		#endregion Model
	}
}

