﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_menuinfo:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_menuinfo : BaseModel
	{
		public stwh_menuinfo()
		{}
		#region Model
		private int _stwh_menuid;
		private string _stwh_menuname;
		private string _stwh_menunameus;
		private string _stwh_menuico_url;
		private string _stwh_menuurl;
		private int _stwh_menuparentid=0;
		private int _stwh_menustatus=0;
		private string _stwh_menudescription="暂无描述";
        private int _stwh_menuorder;

        public int stwh_menuorder
        {
            get { return _stwh_menuorder; }
            set { _stwh_menuorder = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int stwh_menuid
		{
			set{ _stwh_menuid=value;}
			get{return _stwh_menuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_menuname
		{
			set{ _stwh_menuname=value;}
			get{return _stwh_menuname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_menunameUS
		{
			set{ _stwh_menunameus=value;}
			get{return _stwh_menunameus;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_menuICO_url
		{
			set{ _stwh_menuico_url=value;}
			get{return _stwh_menuico_url;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_menuURL
		{
			set{ _stwh_menuurl=value;}
			get{return _stwh_menuurl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_menuparentID
		{
			set{ _stwh_menuparentid=value;}
			get{return _stwh_menuparentid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_menustatus
		{
			set{ _stwh_menustatus=value;}
			get{return _stwh_menustatus;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_menudescription
		{
			set{ _stwh_menudescription=value;}
			get{return _stwh_menudescription;}
		}
		#endregion Model
	}
}

