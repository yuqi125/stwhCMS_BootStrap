﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_buyaddress:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_buyaddress : BaseModel
	{
		public stwh_buyaddress()
		{}
		#region Model
		private int _stwh_baid;
		private int _stwh_buid;
		private string _stwh_baname;
		private string _stwh_bamobile;
		private string _stwh_badiqu;
		private string _stwh_baaddress;
		private int _stwh_bayoubian;
		private int _stwh_baismr=0;
		private DateTime _stwh_baaddtime= DateTime.Now;
        private string _stwh_bumobile;
        private string _stwh_bumobile_h;
        private int _stwh_bacity;

        /// <summary>
        /// 所属城市（直辖市、省、自治区、特别行政区）
        /// 所属城市代码对照表：
        /// 北京市	0
        /// 天津市	1
        /// 上海市	2
        /// 重庆市	3
        /// 广西壮族自治区	4
        /// 内蒙古自治区	5
        /// 西藏自治区	6
        /// 宁夏回族自治区	7
        /// 新疆维吾尔自治区	8
        /// 香港特别行政区	9
        /// 澳门特别行政区	10
        /// 辽宁省	11
        /// 吉林省	12
        /// 黑龙江省	13
        /// 河北省	14
        /// 山西省	15
        /// 江苏省	16
        /// 浙江省	17
        /// 安徽省	18
        /// 福建省	19
        /// 江西省	20
        /// 山东省	21
        /// 河南省	22
        /// 湖北省	23
        /// 湖南省	24
        /// 广东省	25
        /// 海南省	26
        /// 四川省	27
        /// 贵州省	28
        /// 云南省	29
        /// 陕西省	30
        /// 甘肃省	31
        /// 青海省	32
        /// 台湾省	33
        /// </summary>
        public int stwh_bacity
        {
            get { return _stwh_bacity; }
            set { _stwh_bacity = value; }
        }

        /// <summary>
        /// 非数据库字段，该字段用于隐藏手机号码中间的字符
        /// </summary>
        public string stwh_bumobile_h
        {
            get { return _stwh_bumobile_h; }
            set { _stwh_bumobile_h = value; }
        }

        public string stwh_bumobile
        {
            get { return _stwh_bumobile; }
            set { _stwh_bumobile = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int stwh_baid
		{
			set{ _stwh_baid=value;}
			get{return _stwh_baid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_buid
		{
			set{ _stwh_buid=value;}
			get{return _stwh_buid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_baname
		{
			set{ _stwh_baname=value;}
			get{return _stwh_baname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bamobile
		{
			set{ _stwh_bamobile=value;}
			get{return _stwh_bamobile;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_badiqu
		{
			set{ _stwh_badiqu=value;}
			get{return _stwh_badiqu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_baaddress
		{
			set{ _stwh_baaddress=value;}
			get{return _stwh_baaddress;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_bayoubian
		{
			set{ _stwh_bayoubian=value;}
			get{return _stwh_bayoubian;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_baismr
		{
			set{ _stwh_baismr=value;}
			get{return _stwh_baismr;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_baaddtime
		{
			set{ _stwh_baaddtime=value;}
			get{return _stwh_baaddtime;}
		}
		#endregion Model
	}
}

