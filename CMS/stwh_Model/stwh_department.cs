﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_department:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_department : BaseModel
	{
		public stwh_department()
		{}
		#region Model
		private int _stwh_dtid;
		private string _stwh_dtname;
		private string _stwh_dtdescription;
		private string _stwh_dtimg;
		private int _stwh_dtorder=0;
		private int _stwh_dtparentid;
		/// <summary>
		/// 
		/// </summary>
		public int stwh_dtid
		{
			set{ _stwh_dtid=value;}
			get{return _stwh_dtid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_dtname
		{
			set{ _stwh_dtname=value;}
			get{return _stwh_dtname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_dtdescription
		{
			set{ _stwh_dtdescription=value;}
			get{return _stwh_dtdescription;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_dtimg
		{
			set{ _stwh_dtimg=value;}
			get{return _stwh_dtimg;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_dtorder
		{
			set{ _stwh_dtorder=value;}
			get{return _stwh_dtorder;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_dtparentid
		{
			set{ _stwh_dtparentid=value;}
			get{return _stwh_dtparentid;}
		}
		#endregion Model
	}
}

