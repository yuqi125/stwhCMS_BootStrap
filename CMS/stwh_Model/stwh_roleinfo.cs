﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_roleinfo:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_roleinfo : BaseModel
	{
		public stwh_roleinfo()
		{}
		#region Model
		private int _stwh_rid;
		private string _stwh_rname;
		private string _stwh_rdescription="暂无描述";
		private DateTime _stwh_rctime= DateTime.Now;
		private int _stwh_rstate=0;
		private int _stwh_rdelstate=0;
		/// <summary>
		/// 
		/// </summary>
		public int stwh_rid
		{
			set{ _stwh_rid=value;}
			get{return _stwh_rid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_rname
		{
			set{ _stwh_rname=value;}
			get{return _stwh_rname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_rdescription
		{
			set{ _stwh_rdescription=value;}
			get{return _stwh_rdescription;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_rctime
		{
			set{ _stwh_rctime=value;}
			get{return _stwh_rctime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_rstate
		{
			set{ _stwh_rstate=value;}
			get{return _stwh_rstate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_rdelstate
		{
			set{ _stwh_rdelstate=value;}
			get{return _stwh_rdelstate;}
		}
		#endregion Model
	}
}

