﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_article:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_article : BaseModel
	{
		public stwh_article()
		{}
		#region Model
		private int _stwh_atid;
		private int _stwh_artid;
		private string _stwh_attitle="内容标题";
        private string _stwh_attitlesimple = "";
        private string _stwh_atvideourl;
        public string stwh_atvideourl
        {
            get { return _stwh_atvideourl; }
            set { _stwh_atvideourl = value; }
        }
        public string stwh_attitlesimple
        {
            get { return _stwh_attitlesimple; }
            set { _stwh_attitlesimple = value; }
        }
		private string _stwh_atimage="/images/Account_03ww.jpg";
        private string _stwh_atimglist = "";
        public string stwh_atimglist
        {
            get { return _stwh_atimglist; }
            set { _stwh_atimglist = value; }
        }
		private string _stwh_atauthor="未知";
		private string _stwh_atsource="本站原创";
        private string _stwh_atsourceurl;
        public string stwh_atsourceurl
        {
            get { return _stwh_atsourceurl; }
            set { _stwh_atsourceurl = value; }
        }
		private string _stwh_atjianjie;
		private string _stwh_atcontent;
		private string _stwh_atbiaoqian;
		private int _stwh_atissh=0;
		private int _stwh_atiszhiding=0;
		private int _stwh_attuijian=0;
		private int _stwh_attoutiao=0;
		private int _stwh_atgundong=0;
		private string _stwh_atseotitle;
		private string _stwh_atseokeywords;
		private string _stwh_atseodescription;
		private DateTime _stwh_ataddtime= DateTime.Now;
        private int _stwh_atorder = 1;
        private string _stwh_atstyle = "";

        public string stwh_atstyle
        {
            get { return _stwh_atstyle; }
            set { _stwh_atstyle = value; }
        }

        public int stwh_atorder
        {
            get { return _stwh_atorder; }
            set { _stwh_atorder = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int stwh_atid
		{
			set{ _stwh_atid=value;}
			get{return _stwh_atid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_artid
		{
			set{ _stwh_artid=value;}
			get{return _stwh_artid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_attitle
		{
			set{ _stwh_attitle=value;}
			get{return _stwh_attitle;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_atimage
		{
			set{ _stwh_atimage=value;}
			get{return _stwh_atimage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_atauthor
		{
			set{ _stwh_atauthor=value;}
			get{return _stwh_atauthor;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_atsource
		{
			set{ _stwh_atsource=value;}
			get{return _stwh_atsource;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_atjianjie
		{
			set{ _stwh_atjianjie=value;}
			get{return _stwh_atjianjie;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_atcontent
		{
			set{ _stwh_atcontent=value;}
			get{return _stwh_atcontent;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_atbiaoqian
		{
			set{ _stwh_atbiaoqian=value;}
			get{return _stwh_atbiaoqian;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_atissh
		{
			set{ _stwh_atissh=value;}
			get{return _stwh_atissh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_atiszhiding
		{
			set{ _stwh_atiszhiding=value;}
			get{return _stwh_atiszhiding;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_attuijian
		{
			set{ _stwh_attuijian=value;}
			get{return _stwh_attuijian;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_attoutiao
		{
			set{ _stwh_attoutiao=value;}
			get{return _stwh_attoutiao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_atgundong
		{
			set{ _stwh_atgundong=value;}
			get{return _stwh_atgundong;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_atseotitle
		{
			set{ _stwh_atseotitle=value;}
			get{return _stwh_atseotitle;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_atseokeywords
		{
			set{ _stwh_atseokeywords=value;}
			get{return _stwh_atseokeywords;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_atseodescription
		{
			set{ _stwh_atseodescription=value;}
			get{return _stwh_atseodescription;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_ataddtime
		{
			set{ _stwh_ataddtime=value;}
			get{return _stwh_ataddtime;}
		}
		#endregion Model
        #region Model
        private string _stwh_artname;
        private string _stwh_artdescription;
        private string _stwh_artimg;
        private int _stwh_artparentid;
        private int _stwh_artorder = 0;

        /// <summary>
        /// 
        /// </summary>
        public int stwh_artorder
        {
            get { return _stwh_artorder; }
            set { _stwh_artorder = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_artname
        {
            set { _stwh_artname = value; }
            get { return _stwh_artname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_artdescription
        {
            set { _stwh_artdescription = value; }
            get { return _stwh_artdescription; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_artimg
        {
            set { _stwh_artimg = value; }
            get { return _stwh_artimg; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_artparentid
        {
            set { _stwh_artparentid = value; }
            get { return _stwh_artparentid; }
        }
        #endregion Model
	}
}

