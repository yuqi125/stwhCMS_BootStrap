﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_files:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_files : BaseModel
	{
		public stwh_files()
		{}
		#region Model
		private int _stwh_flid;
		private int _stwh_ftid;
		private int _stwh_florder=1;
		private string _stwh_fltitlesimple;
		private string _stwh_fltitle="内容标题";
		private string _stwh_flimage="/stwhup/image/1.jpg";
		private string _stwh_flpath="/stwhup/image/1.jpg";
		private string _stwh_flauthor="未知";
		private string _stwh_flsource="本站原创";
		private int _stwh_flfilesize=1;
		private string _stwh_fldanwei="MB";
		private string _stwh_fljianjie;
		private string _stwh_flcontent;
		private string _stwh_flbiaoqian;
		private int _stwh_flissh=0;
		private int _stwh_fliszhiding=0;
		private int _stwh_fltuijian=0;
		private int _stwh_fltoutiao=0;
		private int _stwh_flgundong=0;
		private string _stwh_flseotitle;
		private string _stwh_flsetokeywords;
		private string _stwh_flsetodescription;
		private DateTime _stwh_fladdtime= DateTime.Now;
		/// <summary>
		/// 
		/// </summary>
		public int stwh_flid
		{
			set{ _stwh_flid=value;}
			get{return _stwh_flid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_ftid
		{
			set{ _stwh_ftid=value;}
			get{return _stwh_ftid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_florder
		{
			set{ _stwh_florder=value;}
			get{return _stwh_florder;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_fltitlesimple
		{
			set{ _stwh_fltitlesimple=value;}
			get{return _stwh_fltitlesimple;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_fltitle
		{
			set{ _stwh_fltitle=value;}
			get{return _stwh_fltitle;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_flimage
		{
			set{ _stwh_flimage=value;}
			get{return _stwh_flimage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_flpath
		{
			set{ _stwh_flpath=value;}
			get{return _stwh_flpath;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_flauthor
		{
			set{ _stwh_flauthor=value;}
			get{return _stwh_flauthor;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_flsource
		{
			set{ _stwh_flsource=value;}
			get{return _stwh_flsource;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_flfilesize
		{
			set{ _stwh_flfilesize=value;}
			get{return _stwh_flfilesize;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_fldanwei
		{
			set{ _stwh_fldanwei=value;}
			get{return _stwh_fldanwei;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_fljianjie
		{
			set{ _stwh_fljianjie=value;}
			get{return _stwh_fljianjie;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_flcontent
		{
			set{ _stwh_flcontent=value;}
			get{return _stwh_flcontent;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_flbiaoqian
		{
			set{ _stwh_flbiaoqian=value;}
			get{return _stwh_flbiaoqian;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_flissh
		{
			set{ _stwh_flissh=value;}
			get{return _stwh_flissh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_fliszhiding
		{
			set{ _stwh_fliszhiding=value;}
			get{return _stwh_fliszhiding;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_fltuijian
		{
			set{ _stwh_fltuijian=value;}
			get{return _stwh_fltuijian;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_fltoutiao
		{
			set{ _stwh_fltoutiao=value;}
			get{return _stwh_fltoutiao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_flgundong
		{
			set{ _stwh_flgundong=value;}
			get{return _stwh_flgundong;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_flseotitle
		{
			set{ _stwh_flseotitle=value;}
			get{return _stwh_flseotitle;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_flsetokeywords
		{
			set{ _stwh_flsetokeywords=value;}
			get{return _stwh_flsetokeywords;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_flsetodescription
		{
			set{ _stwh_flsetodescription=value;}
			get{return _stwh_flsetodescription;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_fladdtime
		{
			set{ _stwh_fladdtime=value;}
			get{return _stwh_fladdtime;}
		}
		#endregion Model
        #region Model
        private string _stwh_ftname;
        private string _stwh_ftdescription;
        private string _stwh_ftimg;
        private int _stwh_ftorder = 0;

        /// <summary>
        /// 
        /// </summary>
        public int stwh_ftorder
        {
            get { return _stwh_ftorder; }
            set { _stwh_ftorder = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_ftname
        {
            set { _stwh_ftname = value; }
            get { return _stwh_ftname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_ftdescription
        {
            set { _stwh_ftdescription = value; }
            get { return _stwh_ftdescription; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_ftimg
        {
            set { _stwh_ftimg = value; }
            get { return _stwh_ftimg; }
        }
        #endregion Model
	}
}

