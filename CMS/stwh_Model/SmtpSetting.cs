﻿using System;

namespace stwh_Model
{
    /// <summary>
    /// SMTP 属性类
    /// </summary>
    [Serializable]
    public class SmtpSetting : BaseModel
    {
        private string _server;
        /// <summary>
        /// SMTP 服务器
        /// </summary>
        public string Server
        {
            get { return _server; }
            set { _server = value; }
        }
        private int _port = 25;
        /// <summary>
        /// SMTP服务器端口号
        /// </summary>
        public int Port
        {
            get { return _port; }
            set { _port = value; }
        }
        private bool _authentication = true;
        /// <summary>
        /// 是否验证发件人身份凭据
        /// </summary>
        public bool Authentication
        {
            get { return _authentication; }
            set { _authentication = value; }
        }
        private string _user;
        /// <summary>
        /// SMTP服务器登录名
        /// </summary>
        public string User
        {
            get { return _user; }
            set { _user = value; }
        }
        private string _sender;
        /// <summary>
        /// 发件人邮箱
        /// </summary>
        public string Sender
        {
            get { return _sender; }
            set { _sender = value; }
        }
        private string _SenderName = "上海舒同文化传播有限公司";
        /// <summary>
        /// 发件人显示名称
        /// </summary>
        public string SenderName
        {
            get { return _SenderName; }
            set { _SenderName = value; }
        }
        private string _password;
        /// <summary>
        /// 登陆密码
        /// </summary>
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
    }
}
