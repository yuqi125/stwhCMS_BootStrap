﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_staff:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_staff : BaseModel
	{
		public stwh_staff()
		{}
		#region Model
		private int _stwh_sid;
		private int _stwh_dtid;
		private int _stwh_snumber;
		private string _stwh_sname;
		private int _stwh_ssex=0;
		private DateTime? _stwh_srztime = null;
		private string _stwh_szw;
		private string _stwh_stel;
		private DateTime? _stwh_sbirthday = null;
		private string _stwh_sxueli="小学";
		private string _stwh_ssfz;
		private int _stwh_syqmoney;
		private int _stwh_szzmoney;
		private int _stwh_siszz=0;
		private DateTime? _stwh_szztime = null;
		private int _stwh_sisht=0;
		private DateTime? _stwh_shttime = null;
		private int _stwh_sislz=0;
		private DateTime? _stwh_slztime = null;
		private string _stwh_sremark;
		private DateTime _stwh_saddtime= DateTime.Now;
        private string _stwh_dtname;

        public string stwh_dtname
        {
            get { return _stwh_dtname; }
            set { _stwh_dtname = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int stwh_sid
		{
			set{ _stwh_sid=value;}
			get{return _stwh_sid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_dtid
		{
			set{ _stwh_dtid=value;}
			get{return _stwh_dtid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_snumber
		{
			set{ _stwh_snumber=value;}
			get{return _stwh_snumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_sname
		{
			set{ _stwh_sname=value;}
			get{return _stwh_sname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_ssex
		{
			set{ _stwh_ssex=value;}
			get{return _stwh_ssex;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? stwh_srztime
		{
			set{ _stwh_srztime=value;}
			get{return _stwh_srztime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_szw
		{
			set{ _stwh_szw=value;}
			get{return _stwh_szw;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_stel
		{
			set{ _stwh_stel=value;}
			get{return _stwh_stel;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? stwh_sbirthday
		{
			set{ _stwh_sbirthday=value;}
			get{return _stwh_sbirthday;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_sxueli
		{
			set{ _stwh_sxueli=value;}
			get{return _stwh_sxueli;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_ssfz
		{
			set{ _stwh_ssfz=value;}
			get{return _stwh_ssfz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_syqmoney
		{
			set{ _stwh_syqmoney=value;}
			get{return _stwh_syqmoney;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_szzmoney
		{
			set{ _stwh_szzmoney=value;}
			get{return _stwh_szzmoney;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_siszz
		{
			set{ _stwh_siszz=value;}
			get{return _stwh_siszz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? stwh_szztime
		{
			set{ _stwh_szztime=value;}
			get{return _stwh_szztime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_sisht
		{
			set{ _stwh_sisht=value;}
			get{return _stwh_sisht;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? stwh_shttime
		{
			set{ _stwh_shttime=value;}
			get{return _stwh_shttime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_sislz
		{
			set{ _stwh_sislz=value;}
			get{return _stwh_sislz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? stwh_slztime
		{
			set{ _stwh_slztime=value;}
			get{return _stwh_slztime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_sremark
		{
			set{ _stwh_sremark=value;}
			get{return _stwh_sremark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_saddtime
		{
			set{ _stwh_saddtime=value;}
			get{return _stwh_saddtime;}
		}
		#endregion Model
	}
}

