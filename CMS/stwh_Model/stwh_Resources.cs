﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_Resources:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_Resources : BaseModel
	{
		public stwh_Resources()
		{}
		#region Model
		private int _stwh_reid;
		private string _stwh_rename;
		private string _stwh_repath;
		private string _stwh_retype="未知";
		private string _stwh_retime;
		private string _stwh_resize;
		private string _stwh_reuname;
		private DateTime _stwh_readdtime;
		/// <summary>
		/// 
		/// </summary>
		public int stwh_reid
		{
			set{ _stwh_reid=value;}
			get{return _stwh_reid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_rename
		{
			set{ _stwh_rename=value;}
			get{return _stwh_rename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_repath
		{
			set{ _stwh_repath=value;}
			get{return _stwh_repath;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_retype
		{
			set{ _stwh_retype=value;}
			get{return _stwh_retype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_retime
		{
			set{ _stwh_retime=value;}
			get{return _stwh_retime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_resize
		{
			set{ _stwh_resize=value;}
			get{return _stwh_resize;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_reuname
		{
			set{ _stwh_reuname=value;}
			get{return _stwh_reuname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_readdtime
		{
			set{ _stwh_readdtime=value;}
			get{return _stwh_readdtime;}
		}
		#endregion Model
	}
}

