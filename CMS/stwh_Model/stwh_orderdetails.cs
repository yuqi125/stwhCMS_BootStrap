﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_orderdetails:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_orderdetails : BaseModel
	{
		public stwh_orderdetails()
		{}
		#region Model
		private int _stwh_orsid;
		private string _stwh_orddid;
		private int _stwh_pid;
		private string _stwh_orspname;
        private decimal _stwh_orsprice;
		private int _stwh_orscount;
        private string _stwh_orspimage;

        public string stwh_orspimage
        {
            get { return _stwh_orspimage; }
            set { _stwh_orspimage = value; }
        }

		/// <summary>
		/// 
		/// </summary>
		public int stwh_orsid
		{
			set{ _stwh_orsid=value;}
			get{return _stwh_orsid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_orddid
		{
			set{ _stwh_orddid=value.Trim();}
			get{return _stwh_orddid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_pid
		{
			set{ _stwh_pid=value;}
			get{return _stwh_pid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_orspname
		{
			set{ _stwh_orspname=value;}
			get{return _stwh_orspname;}
		}
		/// <summary>
		/// 
		/// </summary>
        public decimal stwh_orsprice
		{
			set{ _stwh_orsprice=value;}
			get{return _stwh_orsprice;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_orscount
		{
			set{ _stwh_orscount=value;}
			get{return _stwh_orscount;}
		}
		#endregion Model
	}
}

