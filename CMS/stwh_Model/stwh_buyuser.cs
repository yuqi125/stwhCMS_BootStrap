﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_buyuser:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_buyuser : BaseModel
	{
		public stwh_buyuser()
		{}
		#region Model
		private int _stwh_buid;
		private string _stwh_buimg="/stwhup/image/1.jpg";
		private string _stwh_bunicheng;
		private string _stwh_buname;
		private int _stwh_busex=0;
		private string _stwh_buemail;
		private string _stwh_bumobile;
		private DateTime _stwh_bushengri = DateTime.Now;
		private string _stwh_bupwd;
		private string _stwh_buopenid;
		private string _stwh_buqqid;
		private DateTime _stwh_buaddtime = DateTime.Now;
        private string _stwh_buwbo;
        private int _stwh_bustatus = 0;

        public int stwh_bustatus
        {
            get { return _stwh_bustatus; }
            set { _stwh_bustatus = value; }
        }

        public string stwh_buwbo
        {
            get { return _stwh_buwbo; }
            set { _stwh_buwbo = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int stwh_buid
		{
			set{ _stwh_buid=value;}
			get{return _stwh_buid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_buimg
		{
			set{ _stwh_buimg=value;}
			get{return _stwh_buimg;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bunicheng
		{
			set{ _stwh_bunicheng=value;}
			get{return _stwh_bunicheng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_buname
		{
			set{ _stwh_buname=value;}
			get{return _stwh_buname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_busex
		{
			set{ _stwh_busex=value;}
			get{return _stwh_busex;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_buemail
		{
			set{ _stwh_buemail=value;}
			get{return _stwh_buemail;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bumobile
		{
			set{ _stwh_bumobile=value;}
			get{return _stwh_bumobile;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_bushengri
		{
			set{ _stwh_bushengri=value;}
			get{return _stwh_bushengri;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bupwd
		{
			set{ _stwh_bupwd=value;}
			get{return _stwh_bupwd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_buopenid
		{
			set{ _stwh_buopenid=value;}
			get{return _stwh_buopenid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_buqqid
		{
			set{ _stwh_buqqid=value;}
			get{return _stwh_buqqid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_buaddtime
		{
			set{ _stwh_buaddtime=value;}
			get{return _stwh_buaddtime;}
		}
		#endregion Model
	}
}

