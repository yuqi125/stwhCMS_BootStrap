﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_shopcart:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_shopcart : BaseModel
	{
		public stwh_shopcart()
		{}
		#region Model
		private int _stwh_scid;
		private int _stwh_buid;
		private int _stwh_pid;
        private string _stwh_ptitle;
        private string _stwh_pdescription;
        private float _stwh_pprice;
        private int _stwh_sccount;
        private int _stwh_psumcount;
        private string _stwh_ptitlesimple;
        private string _stwh_pimage;
        private string _stwh_pimagelist;

        public string stwh_pimagelist
        {
            get { return _stwh_pimagelist; }
            set { _stwh_pimagelist = value; }
        }

        public string stwh_pimage
        {
            get { return _stwh_pimage; }
            set { _stwh_pimage = value; }
        }

        public string stwh_ptitlesimple
        {
            get { return _stwh_ptitlesimple; }
            set { _stwh_ptitlesimple = value; }
        }

        public int stwh_psumcount
        {
            get { return _stwh_psumcount; }
            set { _stwh_psumcount = value; }
        }

        public int stwh_sccount
        {
            get { return _stwh_sccount; }
            set { _stwh_sccount = value; }
        }

        public float stwh_pprice
        {
            get { return _stwh_pprice; }
            set { _stwh_pprice = value; }
        }

        public string stwh_pdescription
        {
            get { return _stwh_pdescription; }
            set { _stwh_pdescription = value; }
        }

        public string stwh_ptitle
        {
            get { return _stwh_ptitle; }
            set { _stwh_ptitle = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int stwh_scid
		{
			set{ _stwh_scid=value;}
			get{return _stwh_scid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_buid
		{
			set{ _stwh_buid=value;}
			get{return _stwh_buid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_pid
		{
			set{ _stwh_pid=value;}
			get{return _stwh_pid;}
		}
		#endregion Model
	}
}

