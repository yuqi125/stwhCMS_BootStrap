﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_questions:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_questions : BaseModel
	{
		public stwh_questions()
		{}
		#region Model
		private int _stwh_quid;
		private int _stwh_qutid;
		private int _stwh_quorder=1;
		private string _stwh_qumiaoshu;
		private int _stwh_qutype=1;
		private int _stwh_qufenshu=1;
		private string _stwh_qudaan;
		private DateTime _stwh_quaddtime= DateTime.Now;
        private string _stwh_qutname;
        private string _stwh_qutitle;
        private int _stwh_uiid;

        public int stwh_uiid
        {
            get { return _stwh_uiid; }
            set { _stwh_uiid = value; }
        }

        public string stwh_qutitle
        {
            get { return _stwh_qutitle; }
            set { _stwh_qutitle = value; }
        }

        public string stwh_qutname
        {
            get { return _stwh_qutname; }
            set { _stwh_qutname = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int stwh_quid
		{
			set{ _stwh_quid=value;}
			get{return _stwh_quid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_qutid
		{
			set{ _stwh_qutid=value;}
			get{return _stwh_qutid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_quorder
		{
			set{ _stwh_quorder=value;}
			get{return _stwh_quorder;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_qumiaoshu
		{
			set{ _stwh_qumiaoshu=value;}
			get{return _stwh_qumiaoshu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_qutype
		{
			set{ _stwh_qutype=value;}
			get{return _stwh_qutype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_qufenshu
		{
			set{ _stwh_qufenshu=value;}
			get{return _stwh_qufenshu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_qudaan
		{
			set{ _stwh_qudaan=value;}
			get{return _stwh_qudaan;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_quaddtime
		{
			set{ _stwh_quaddtime=value;}
			get{return _stwh_quaddtime;}
		}
		#endregion Model
	}
}

