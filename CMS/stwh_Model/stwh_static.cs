﻿using System;
namespace stwh_Model
{
	/// <summary>
    /// stwh_static:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_static : BaseModel
	{
        public stwh_static()
		{}
		#region Model
		private int _stwh_stid;
		private string _stwh_sttitle;
		private string _stwh_stremark;
		private string _stwh_stcontent;
		private DateTime _stwh_staddtime;

		/// <summary>
		/// 
		/// </summary>
        public int stwh_stid
		{
			set{ _stwh_stid=value;}
			get{return _stwh_stid;}
		}
		/// <summary>
		/// 
		/// </summary>
        public string stwh_sttitle
		{
			set{ _stwh_sttitle=value;}
			get{return _stwh_sttitle;}
		}
		/// <summary>
		/// 
		/// </summary>
        public string stwh_stremark
		{
			set{ _stwh_stremark=value;}
			get{return _stwh_stremark;}
		}
		/// <summary>
		/// 
		/// </summary>
        public string stwh_stcontent
		{
			set{ _stwh_stcontent=value;}
			get{return _stwh_stcontent;}
		}
		/// <summary>
		/// 
		/// </summary>
        public DateTime stwh_staddtime
		{
			set{ _stwh_staddtime=value;}
			get{return _stwh_staddtime;}
		}
		#endregion Model
	}
}

