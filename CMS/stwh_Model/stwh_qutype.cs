﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_qutype:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_qutype : BaseModel
	{
		public stwh_qutype()
		{}
		#region Model
		private int _stwh_qutid;
		private string _stwh_qutname;
		private string _stwh_qutdescription;
		private int _stwh_qutparentid;
        private int _stwh_qutorder = 0;
        private int _stwh_uiid;

        public int stwh_uiid
        {
            get { return _stwh_uiid; }
            set { _stwh_uiid = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int stwh_qutorder
        {
            get { return _stwh_qutorder; }
            set { _stwh_qutorder = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int stwh_qutid
		{
			set{ _stwh_qutid=value;}
			get{return _stwh_qutid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_qutname
		{
			set{ _stwh_qutname=value;}
			get{return _stwh_qutname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_qutdescription
		{
			set{ _stwh_qutdescription=value;}
			get{return _stwh_qutdescription;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_qutparentid
		{
			set{ _stwh_qutparentid=value;}
			get{return _stwh_qutparentid;}
		}
		#endregion Model
	}
}

